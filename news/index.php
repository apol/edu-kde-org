<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );
  $site_root = "../";
  $page_title = i18n_var( "News from our Blogs" );

  include("header.inc");

  // show_news() makes all the html...
  include "../site_includes/show_news.inc";
?>

<p><?php i18n( 'You can read all news from our blogs as <a href="rss.php" title="RSS formatted KDE-Edu News Feed"><img src="../images/web/rss.png" class="inline" width="22" height="22" alt="RSS"/> RSS feed&nbsp;</a>.' ); ?><br />
<?php i18n( 'Have a look at the KDE feed reader <a href="http://kde.org/applications/internet/akregator/" title="aKregator RSS/Atom reader">aKregator</a>.' ); ?></p>
 
<?php

  show_news( 20 , true );
  
  include("footer.inc");
?>
