# -*- coding: utf-8 -*-
#############################################################################
# Copyright (C) 2009 Daniel Laidig <d.laidig@gmx.de>
#
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; see the file COPYING.LIB.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#############################################################################

from scmdownloader import ScmDownloader
import os

class Downloader:
    iconSizes = [22, 48]
    allIconSizes = [256, 128, 64, 48, 32, 22, 16]
    iconPath = "../../images/icons/"

    def __init__(self):
        self.apps = []
        self.downloadDesktopFiles = False
        self.downloadCodeFiles = False
        self.downloadScVersions = None
        self.downloadIcons = False

        self.scmDownloader = ScmDownloader()

    def setApplicationList(self, apps):
        self.apps = apps

    def setDownloadDesktopFiles(self, download):
        self.downloadDesktopFiles = download

    def setDownloadCodeFiles(self, download):
        self.downloadCodeFiles = download

    def setDownloadScVersions(self, versions):
        self.downloadScVersions = versions

    def setDownloadIcons(self, download):
        self.downloadIcons = download

    def isNoOp(self):
        return (self.downloadDesktopFiles == False and self.downloadCodeFiles == False and self.downloadScVersions == None and self.downloadIcons == False)

    def run(self):
        print "DOWNLOADING FILES FOR", len(self.apps), "APPLICATIONS"
        for app in self.apps:
            self.download(app)

    def download(self, app):
        print "  Downloading files for application", app.name

        # Download desktop and code files
        desktopFile = None
        codeFile = None
        if self.downloadDesktopFiles:
            desktopFile = open(app.desktopFileName(), "w")
        if self.downloadCodeFiles:
            codeFile = open(app.codeFileName(), "w")
        if codeFile != None or desktopFile != None:
            if app.info["repository"][0] != "svn":
                print "    Skipping, not svn"
                return
            self.recursiveDownloadSources(app.name, app.info["repository"][1], codeFile, desktopFile) #TODO: git
        if desktopFile:
            desktopFile.close()
        if codeFile:
            codeFile.close()

        # Download icons
        if self.downloadIcons:
            iconFiles = {}
            for size in self.iconSizes:
                iconFiles[size] = self.iconPath+app.name+"_"+str(size)+".png"
            self.downloadIconFiles(app, iconFiles)

        #self.downloadScVersionFiles(app)

    def recursiveDownloadSources(self, applicationName, path, codeFile, desktopFile, lastPathSegment=None):
        entries = self.scmDownloader.svnList(path)
        if lastPathSegment == None:
            lastPathSegment = path.split("/")[-2]
        for entry in entries:
            if entry[-1:] == "/":
                if entry in (applicationName+"/", "src/", "data/", "shell/", "misc/", "part/", "app/", "support/", "desktop/", "editor/", "program/", "gui/", "irkick/", "knemod/", "korgac/", "other/", lastPathSegment+"/"):
                    self.recursiveDownloadSources(applicationName, path+entry, codeFile, desktopFile, lastPathSegment)
            else:
                if ((entry.lower() in (applicationName+".cpp", lastPathSegment+".cpp", "about.cpp", "aboutdata.h", "version.h", "daboutdata.h")) or ("main.c" in entry.lower())) and codeFile != None:
                    print "    Downloading", path+entry
                    self.scmDownloader.svnDownload(path+entry, codeFile)
                if entry.lower() in (applicationName+".desktop", lastPathSegment+".desktop") and desktopFile != None:
                    print "    Downloading", path+entry
                    self.scmDownloader.svnDownload(path+entry, desktopFile)

    def downloadCategoryIcons(self):
        # categories = ("Development", "Education", "Games", "Graphics", "Internet", "Multimedia", "Settings", "Office", "System", "Utilities")
        categories = ("Education-Preschool", "Education-School", "Education-University", "Education-Language", "Education-Mathematics", "Education-Miscellaneous", "Education-Science")
        print "Downloading category icons"
        for category in categories:
            print "  Category", category
            for size in self.iconSizes:
                path = "/trunk/kdesupport/oxygen-icons/{size}x{size}/categories/applications-"+category.lower()+".png"
                if category == "Settings":
                    path = "/trunk/kdesupport/oxygen-icons/{size}x{size}/categories/preferences-system.png"
                outputFilename = self.iconPath+"/categories/"+category.lower()+"_"+str(size)+".png"
                self.downloadIcon(path, size, outputFilename)

    def downloadIconFiles(self, app, iconFiles):
        for size in iconFiles:
            path = None
            if app.info["icon"][0] == "own":
                path = app.info["repository"][1]
                path += app.info["icon"][1]
            elif app.info["icon"][0] == "oxygen":
                path = "/trunk/kdesupport/oxygen-icons/{size}x{size}/"+app.info["icon"][1]
            else:
                print "    Error: Icon type", app.info["icon"][0], "not yet supported"

            if path != None:
                self.downloadIcon(path, size, iconFiles[size])
                

    def downloadIcon(self, path, size, outputFilename):
        try:
            pathWithSize = path.format(size=size)
            print "    Downloading icon", pathWithSize
            out = open(outputFilename, "wb")
            self.scmDownloader.svnDownload(pathWithSize, out)
            out.close()
        except Exception, e:
            print "    Exception:", e
            print "    Finding alternative icon sizes"
            for altSize in self.allIconSizes:
                pathWithSize = path.format(size=altSize)
                try:
                    out = open(outputFilename, "wb")
                    self.scmDownloader.svnDownload(pathWithSize, out) #TODO: resize
                    out.close()
                    print "    Size", altSize, "FOUND"
                except:
                    print "    Size", altSize, "not found"
                    continue
                os.system("convert +set date:modify +set date:create -resize {size}x{size} '{filename}' '{filename}'".format(size=size, filename=outputFilename))
                break

    def downloadScVersionFiles(self, app):
        if self.downloadScVersions == None:
            return
        if app.info["repository"][0] != "svn" or not app.info["repository"][1].startswith("trunk/KDE/"):
            print "    "+app.name+" is not in KDE SC, skip downloading version history."
            return
        for version in self.downloadScVersions:
            rewrittenPath = "tags/KDE/"+version+app.info["repository"][1][9:]
            print rewrittenPath
            codeFile = open(app.codeFileName()+"_"+version, "w")
            self.recursiveDownloadSources(app.name, rewrittenPath, codeFile, None)
            codeFile.close()
