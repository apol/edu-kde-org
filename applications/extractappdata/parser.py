# -*- coding: utf-8 -*-
#############################################################################
# Copyright (C) 2009 Daniel Laidig <d.laidig@gmx.de>
#
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; see the file COPYING.LIB.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#############################################################################

from codeparser import CodeParser
import sys

categoryFallbacks = {
    "kdevelop": "Development",
    "ktts": "Utilities",
    "kfind": "Utilities",
    "konqueror": "Internet",
    "kremotecontrol":"Utilities",
    "printerapplet": "System",
    "kommander": "Development",
    "amarok": "Multimedia",
    "konversation": "Internet",
    "kappfinder": "System",
}

genericNameFallbacks = {
    "kdevelop": "Integrated Development Environment",
    "killbots": "",
    "kappfinder": "Menu Updating Tool",
    "kfind": "Find Files/Folders",
    "kremotecontrol": "Remote Controls",
    "kommander": "Dynamic Dialog Editor",
    "amarok": "Audio Player",
    "konversation": "IRC Client",
    "kioskadmintool": "Kiosk Framework Administration"
}

class Parser:
    def __init__(self):
        self.apps = []
        self.parseDesktopFiles = False
        self.parseCodeFiles = False
        self.parseScVersions = None

        self.codeParser = CodeParser()

    def setApplicationList(self, apps):
        self.apps = apps

    def setParseDesktopFiles(self, parse):
        self.parseDesktopFiles = parse

    def setParseCodeFiles(self, parse):
        self.parseCodeFiles = parse

    def setParseScVersions(self, versions):
        self.parseScVersions = versions

    def isNoOp(self):
        return (self.parseDesktopFiles == False and self.parseCodeFiles == False and self.parseScVersions == None)

    def run(self):
        print "PARSING FILES FOR", len(self.apps), "APPLICATIONS"
        for app in self.apps:
            self.parse(app)

    def parse(self, app):
        print "  Parsing files for application", app.name
        if self.parseDesktopFiles:
            desktopFile = open(app.desktopFileName(), "r")
            genericName = self.codeParser.extractGenericName(desktopFile)
            desktopFile.seek(0)
            category = self.codeParser.extractCategory(desktopFile)
            if app.name in genericNameFallbacks:
                genericName = genericNameFallbacks[app.name]
            if app.name in categoryFallbacks:
                category = categoryFallbacks[app.name]
            if genericName == None:
                print "    Failed to extract generic name for", app.name
                sys.exit(1)
            if category == None:
                print "    Failed to extract category for", app.name
                sys.exit(1)
            app.generatedInfo["generic name"] = genericName
            app.generatedInfo["category"] = category
            print "    "+app.name+": found generic name \""+genericName+"\" and category \""+category+"\""
        if self.parseCodeFiles:
            codeFile = open(app.codeFileName(), "r")
            result = self.codeParser.extractAuthors(codeFile)
            codeFile.close()
            (authors, credits, version, license) = (None, None, None, None)
            if result:
                (authors, credits, version, license) = result
                print "    "+app.name+": found", len(authors), "authors and", len(credits), "credits, license:", license
                app.generatedInfo["authors"] = authors
                app.generatedInfo["credits"] = credits
                if license:
                    app.generatedInfo["license"] = license
            else:
                print "    "+app.name+": could not extract author information"
