<?php
  $site_root = "../";
  $page_title = 'Marble - Current Release';

  include ( "header.inc" );

  $submenu->show();
?>
<h3><a name="description">Visual ChangeLog: Marble 1.0 ("Magrat&eacute;e")</a></h3>

<p>
Marble 1.0 has been released on January 26th, 2011. After almost 5 years 
of open community-driven development the Marble Team decided that the time is ripe for 
"1.0": Marble has developed into a mature impressive virtual globe with a big developer and 
user community behind. We'd like to thank all the people who have supported us during all those years. And we'd
like to encourage everyone interested to <a href="http://edu.kde.org/marble/getinvolved.php">join us</a>: 
The next release is already <a href="http://www.kdedevelopers.org/node/4373">in the works</a>.

<p>To celebrate this release we've compiled some <i>fact sheets</i> for the Marble Virtual Globe:   
<ul>
<li> <a href="http://developer.kde.org/~tackat/marble_1_0.pdf">Marble Virtual Globe 1.0 fact sheet</a> for <i>users</i> 
<li> <a href="http://developer.kde.org/~tackat/libmarble_0_11_0.pdf">(lib)Marble Library fact sheet</a> for <i>developers</i>
</ul> 
<p>Marble 1.0 is part of the KDE 4.6 Software Compilation. In the good tradition of 
recent years, we have collected those changes directly visible to the user. 
Please enjoy looking over the new and noteworthy:
</p>

<h4>Bookmark Support</h4>

<dl> <dt> <a href="./screenshots/1.0/marble-add-bookmark.png"><img border="0"
width="400" height="300" src="./screenshots/1.0/marble-add-bookmark_thumb.png"
alt="Adding a Bookmark in Marble"></a> </dt> <dd><i>Adding a Bookmark in
Marble</i></dd>
</dl>

<p>
Marble always supported one bookmark, the home location. Since one bookmark may
not be enough for everyone, Gaurav Gupta added support for an arbitrary number
of bookmarks during his Google Summer of Code project. Bookmarks can be added
at any point on the map. Suggestions for bookmark names are automatically
created using reverse geocoding.
</p>
<p>
Bookmarks are arranged in folders. They can be accessed from the Bookmarks main
menu. Additionally they appear as possible routing targets for quick planning
of commonly used routes. The mobile version of Marble supports Bookmarks as
well.
</p>

<dl> <dt> <a href="./screenshots/1.0/marble-maemo-bookmarks.png"><img border="0"
width="400" height="240"
src="./screenshots/1.0/marble-maemo-bookmarks_thumb.png"
alt="Bookmarks as Route Targets on the Nokia N900"></a> </dt>
<dd><i>Bookmarks as Route Targets on the Nokia N900</i></dd>
</dl>

<p>
You can learn more about Bookmarks in 
<a href="http://userbase.kde.org/Special:MyLanguage/Marble/Bookmarks">KDE
Userbase</a>. A Bookmark Manager (editing, moving and deleting existing
bookmarks, KML import and export) is planned for a future Marble version.
</p>

<h4>GPS Tracking</h4>

<p>
The existing GPS tracking support has been extended. Besides showing your current
location on the map, Marble can now automatically follow you as you move.
Furthermore it's possible to have the zoom value adjusted automatically according
to your speed.
</p>

<p>
The mobile version on the Nokia N900 let's you open and save track logs in .kml
format. This feature is not yet available on the Desktop, but will follow shortly
in Marble 1.1. More details on GPS tracking are described in <a href="http://userbase.kde.org/Special:MyLanguage/Marble/Tracking">KDE Userbase</a>.
</p>

<dl> <dt> <a href="./screenshots/1.0/marble-gps-tracking-1.png"><img border="0"
width="400" height="300" src="./screenshots/1.0/marble-gps-tracking-1_thumb.png"
alt="GPS Tracking in Marble"></a>
</dt> <dd><i>GPS Tracking in Marble</i></dd>
</dl>

<h4>Routing</h4>

<p>
Support for routing was introduced in Marble 0.10 using OpenRouteService as
the backend. Since the requirement of an Internet connection and the limitation
to routing in Europe does not cover all use cases we want to support, we
extended Marble's routing feature to support several different backends. Four
new backends join the existing OpenRouteService one: <i>Yours</i> provides
worldwide online routing while <i>Gosmore</i>, <i>Monav</i> and <i>Routino</i> 
are offline routing backends. The different backends can be configured in 
routing profiles. Four profiles are setup as default: <i>Car (fastest)</i>, 
<i>Car (shortest)</i>, <i>Bicycle</i> and <i>Pedestrian</i>.
</p>

<p>
Offline routers require additional maps to be installed locally. For your
convenience we host maps for all worldwide countries for the Monav backend. They
can be downloaded directly from within Marble. Further details can be found in
<a href="http://userbase.kde.org/Special:MyLanguage/Marble/Maemo/OfflineRouting">
KDE Userbase</a>.
</p>

<dl> <dt> <a href="./screenshots/1.0/marble-routing-offline-routing.png"><img border="0"
width="400" height="300" src="./screenshots/1.0/marble-routing-offline-routing_thumb.png"
alt="Download of Offline Routing Maps in Marble"></a>
</dt> <dd><i>Download of Offline Routing Maps in Marble</i></dd>
</dl>

<p>
Sometimes there are several possible ways to reach a destination. They take a
similar amount of time but use quite different roads. In such a case Marble
chooses the best route (shown in blue in the map), but also presents the
alternative ones to you (shown in gray). If you prefer one of the alternative
routes, just click on it in the map to switch to it.
</p>

<dl> <dt> <a href="./screenshots/1.0/marble-routing-alternative-routes.png"><img border="0"
width="400" height="300" src="./screenshots/1.0/marble-routing-alternative-routes_thumb.png"
alt="Alternative Routes between Source and Destination"></a>
</dt> <dd><i>Alternative Routes between Source and Destination</i></dd>
</dl>

Marble provides further assistance functions tailored to the mobile use case.
Once you planned a route, switch Marble into route guidance mode to have
driving instructions shown in front of turn points. In case you deviate from
the route, Marble automatically calculates an appropriate new route.

We created a Tutorial in <a
href="http://userbase.kde.org/Special:MyLanguage/Marble/Maemo/GuidanceMode">KDE
Userbase</a> that introduces the route guidance mode.

<dl> <dt> <a href="./screenshots/1.0/marble-maemo-guidance-mode.png"><img
border="0"
width="400" height="240"
src="./screenshots/1.0/marble-maemo-guidance-mode_thumb.png"
alt="Route Guidance Mode on the Nokia N900"></a>
</dt> <dd><i>Route Guidance Mode on the Nokia N900</i></dd>
</dl>

<h4>Web Map Service (WMS)</h4>

<p>The <a href="http://en.wikipedia.org/wiki/Web_Map_Service">Web Map Service 
(WMS)</a> support that has been introduced with Marble 0.10
has been further improved. For Marble 1.0 WMS only gets used internally for 
ready-made map themes (which can be created and edited via XML files). 
In our future Marble release there will be a 
<a href="http://userbase.kde.org/Marble/WizardMap#Web_Map_Service">Map Creation Wizard</a> 
which will make this feature more accessible to the user. 

<h4>Osmarender Map Theme</h4>

<p>
Osmarender is a popular OpenStreetMap base layer rendered by the tiles@home
project. It can now also be used in Marble as an alternative to the
classical OpenStreetMap Marble map (Mapnik layer). You can install it via
Marble's 'Get New Stuff' feature (File => Download Maps).
</p>

<p>
The Osmarender map theme does also include an optional hillshading layer created
by Colin Marquardt. You can enable it in Marble's Legend tab on the left side of
the screen.
</p>

<dl> <dt> <a href="./screenshots/1.0/marble-osmarender.png"><img border="0"
width="400" height="300" src="./screenshots/1.0/marble-osmarender_thumb.png"
alt="The Osmarender Map Theme with Hillshading"></a>
</dt> <dd><i>The Osmarender Map Theme with Hillshading</i></dd>
</dl>

<h4>Experimental Qt Quick (QtDeclarative/QML) Support</h4>

<p>Qt Quick is a new technology introduced in Qt 4.7 which makes it easier to
create animated, touch enabled user interfaces. In preparation of more
extensive use in future Marble versions we now ship an experimental
QtDeclarative plugin. The plugin exposes the Marble widget and some other
components to QML. Three <a
href="https://projects.kde.org/projects/kde/kde-edu/marble/repository/revisions/master/show/examples/qml">
example QML applications</a> are included as well. You can start the examples
as shown below (paths may be different on your system): <pre>
qmlviewer -I /usr/lib/kde4/plugins/marble/ /usr/share/marble/examples/qml/google-search/google-search.qml</pre>

<dl> <dt> <a href="./screenshots/1.0/marble-qml-google.png"><img border="0"
width="400" height="400" src="./screenshots/1.0/marble-qml-google_thumb.png"
alt="A sample Marble QML application mimicking a popular search engine"></a>
</dt> <dd><i>A sample Marble QML application mimicking a popular search
engine</i></dd>
</dl>

<h4>Performance Improvements</h4>
<p>Marble is known to run smooth even on older hardware. Still we found some ways
to squeeze more speed out of it: Caching pixmaps, optimizing hot spots and
improving projections, Marble now renders faster than ever. Although these
optimizations were primarily developed to accelerate our 
<a href="http://marble.garage.maemo.org/">mobile version</a>, most
of them apply equally well to Marble on the Desktop.

<h4>And more ...</h4>
<p>There is a special version of Marble that doesn't require the KDE framework
(it technically only depends on the Qt library). This version is commonly 
used for our Windows, Mac and Nokia N900 packages.
<p>Marble 1.0 will be the first release where this special Qt-Only version
will ship with a user interface translated into more than 50 languages - just
like the KDE version.
<p>Also the Qt-Only version will have a nice feature for OSM Mappers: 
The user can easily switch from the OpenStreetMap view in Marble to 
OSM editing tools such as JOSM, Merkaator and Potlatch: The tool will
automatically open up the same area seen in Marble.

<p>
New features in earlier versions are described in prior Visual
Changelogs: Marble <a href="http://edu.kde.org/marble/current_0.6.php">0.6</a>,
<a href="http://edu.kde.org/marble/current_0.7.php">0.7</a>,
<a href="http://edu.kde.org/marble/current_0.8.php">0.8</a>,
<a href="http://edu.kde.org/marble/current_0.9.php">0.9</a> and 
<a href="http://edu.kde.org/marble/current_0.10.php">0.10</a>.
</p>

<p><a href="http://opendesktop.org/groups/?id=439"><img src="http://static.opendesktop.org/img/headers/header1_10_1.jpg" class="showonplanet" /></a>
<br>
<p><!-- Facebook Badge START --><a href="http://www.facebook.com/marbleglobe" target="_TOP" title="Marble (Virtual Globe)"><img src="http://badge.facebook.com/badge/112069922186463.730.115653362.png" width="120" height="177" style="border: 0px;" /></a><br/><!-- Facebook Badge END -->

<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
   include "footer.inc";
?>
