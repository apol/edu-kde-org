
<?php
  $site_root = "../";
  $page_title = 'Marble - Current Release';

  include ( "header.inc" );

  $submenu->show();
?>
<h3><a name="description">Visual ChangeLog: Marble 1.1</a></h3>

<p>
Marble 1.1 was released on April 15th, 2011. This release is special: With many new features being developed during Google Code-in (GCI) we decided to make an  early release between the usual ones synchronized with the other KDE applications. The Marble library released alongside is binary compatible with the one shipped with Marble 1.0.
</p>
<p>To celebrate this release we've compiled some <i>fact sheets</i> for the Marble Virtual Globe:   
<ul>
<li> <a href="http://developer.kde.org/~tackat/marble_1_1.pdf">Marble Virtual Globe 1.1 fact sheet</a> for <i>users</i> 
<li> <a href="http://developer.kde.org/~tackat/libmarble_0_11_0.pdf">(lib)Marble Library fact sheet</a> for <i>developers</i>
</ul> 

<p>
In the good tradition of  recent releases, we have collected those changes 
directly visible to the user. Please enjoy looking over the new and noteworthy:
</p>

<h4>Map Creation Wizard and Map Sharing</h4>

<p>
Marble provides a variety of map themes out of the box: Choose between topographic maps, satellite maps, street maps, educational maps, historical maps and more. Even maps of other planets are provided. For space reasons some of them are provided via the 'Get New Maps' feature that provides a convenient installation with just a few clicks.
</p>

<p>
Not all possible maps can be provided, however. Custom map themes can be created by users in Marble's own .dgml file format. Getting started with .dgml file creation was not too easy until now. That's a gap Marble's new map creation wizard fills out: It guides you through the process of creating a new map theme, queries needed information and sets up a fully functional map theme for you that instantly works in Marble. You can even share your map theme with other users with the built-in upload feature.
</p>

<p>
The map creation wizard supports three different kinds of map themes: Those consisting of one large source image showing the world, those accessible from tile servers (e.g. OpenStreetMap, Google Maps, Ovi Maps) and those accessible from <a href="http://en.wikipedia.org/wiki/Web_Map_Service">WMS servers</a>. Further information is available in a <a href="http://userbase.kde.org/Marble/WizardMap">KDE Userbase tutorial</a>.
</p>

<p>
We ship the map creation wizard as a technical preview. The map creation wizard in Marble 1.2 provides improved usability and additional features that could not be introduced in Marble 1.1 while keeping the library binary compatible at the same time.
</p>

<p>
We'd like to thank the <a href="http://opendesktop.org/">openDesktop.org</a> team for providing storage space for uploaded maps.
</p>

<dl> <dt> <a href="./screenshots/1.1/marble-map-wizard.png"><img border="0"
width="400" height="292" src="./screenshots/1.1/marble-map-wizard_thumb.png"
alt="Marble's new map wizard in action"></a> </dt> <dd><i>The groundwater map shown in the screenshot has been created with the help of Marble's new map wizard (foreground). Map data source: WHYMAP, (C) BGR Hannover and UNESCO Paris.</i></dd>
</dl>

<h4>Earthquakes Online Service</h4>

<dl> <dt> <a href="./screenshots/1.1/marble-earthquakes.png"><img border="0"
width="400" height="289" src="./screenshots/1.1/marble-earthquakes_thumb.png"
alt="Recent earthquakes being displayed in Marble"></a> </dt> <dd><i>Recent earthquakes being displayed in Marble</i></dd>
</dl>

<p>
The earthquakes plugin joins Marble's set of online services. It displays earthquakes which occurred in a given time span. Different colors indicate the magnitude of earthquakes. Please note that only historic earthquakes are shown; currently there are no plans to integrate real-time data from earthquake warning systems.
</p>
<p>
This feature has been completed in January already, so its appearance in this release is not related to the recent tragic incidents in Japan. 
</p>

<h4>Open Desktop Online Service</h4>

<dl> <dt> <a href="./screenshots/1.1/marble-opendesktop.png"><img border="0"
width="400" height="289" src="./screenshots/1.1/marble-opendesktop_thumb.png"
alt="The nearby Open Desktop community shows up in Marble"></a> </dt> <dd><i>The nearby Open Desktop community shows up in Marble</i></dd>
</dl>

<p>
OpenDesktop.org is one of the biggest online communities, social networks and portals for the free desktop movement in the world where developers, artists and users can share applications, tools, wallpapers, sounds, icons, themes and other artwork and stuff for the open desktop. Would you like to get to know your local OpenDesktop.org community? Just activate the Open Desktop online service in Marble and people around you will show up on the map.
</p>

<h4>Extended Plugin Configuration</h4>

<p>
Many plugins now let you change their behavior and appearance: Choose a different crosshairs icon, show the ratio scale in the scalebar, change the compass or show a GPS trail. Each info box can now be hidden or locked with a context menu. These changes are especially useful for users of applications embedding Marble.
</p>

<dl> <dt> <a href="./screenshots/1.1/marble-configuration-dialogs.png"><img border="0"
width="267" height="240" src="./screenshots/1.1/marble-configuration-dialogs_thumb.png"
alt="Extended Plugin Configuration"></a> </dt> <dd><i>Most plugins in Marble now allow advanced users to tweak the plugin as they prefer.</i></dd>
</dl>

<h4>Map Editing</h4>

<dl> <dt> <a href="./screenshots/1.1/marble-external-editor.png"><img border="0"
width="400" height="288" src="./screenshots/1.1/marble-external-editor_thumb.png"
alt="External OpenStreetMap editor selection"></a> </dt> <dd><i>External OpenStreetMap editor selection</i></dd>
</dl>

<p>
Did you spot a wrong or missing detail in OpenStreetMap? The wikipedia-like approach of editing the OpenStreetMap by community members makes you the best one to fix it instantly. Marble's KDE version simplifies this process now just like the Qt version did in Marble 1.0: Just use the Edit Map button to open the currently visible map region in your favorite external editor. Supported are Potlach (a web-browser based editor), Merkaartor and JOSM.
</p>

<h4>Voice Navigation</h4>

<dl> <dt> <a href="./screenshots/1.1/marble-speaker-configuration.png"><img border="0"
width="400" height="240" src="./screenshots/1.1/marble-speaker-configuration_thumb.png"
alt="Voice navigation configuration on the Nokia N900"></a> </dt> <dd><i>Voice navigation configuration on the Nokia N900</i></dd>
</dl>

<p>
Marble's mobile version running on the Nokia N900 now sports voice navigation in
a preview version. You can choose between sound output (turn points are announced
with a sound) and speakers. We don't ship any speaker with our packages, but you
can use TomTom voices: Download one of the free ones (some websites offer them 
for personal use), convert it with our web frontend and copy it to your N900. 
A <a href="http://userbase.kde.org/Marble/CustomSpeakers">KDE Userbase tutorial</a>
has all the details you need.
</p>

<h4>And more ...</h4>
<p>There is a special version of Marble that doesn't require the KDE framework
(it technically only depends on the Qt library). This version is commonly 
used for our Windows, Mac and Nokia N900 (Maemo and MeeGo) packages.
</p>

<p>
Marble on the Nokia N900 now uses stackable windows for an improved user
interface. Other parts of the user interface have been polished as well.
</p>

<p>
New features in earlier versions are described in prior Visual
Changelogs: Marble <a href="http://edu.kde.org/marble/current_0.6.php">0.6</a>,
<a href="http://edu.kde.org/marble/current_0.7.php">0.7</a>,
<a href="http://edu.kde.org/marble/current_0.8.php">0.8</a>,
<a href="http://edu.kde.org/marble/current_0.9.php">0.9</a>,
<a href="http://edu.kde.org/marble/current_0.10.php">0.10</a> and
<a href="http://edu.kde.org/marble/current_1.0.php">1.0</a>.
</p>

<p><a href="http://opendesktop.org/groups/?id=439"><img src="http://static.opendesktop.org/img/headers/header1_10_1.jpg" class="showonplanet" /></a>
<br>
<p><!-- Facebook Badge START --><a href="http://www.facebook.com/marbleglobe" target="_TOP" title="Marble (Virtual Globe)"><img src="http://badge.facebook.com/badge/112069922186463.730.115653362.png" width="120" height="177" style="border: 0px;" /></a><br/><!-- Facebook Badge END -->

<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
   include "footer.inc";
?>
