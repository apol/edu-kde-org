<?php
  $site_root = "../";
  $page_title = "Commercial Support";

  include ( "header.inc" );

  $submenu->show();

?>

<p>
<dl> <dt> <a href="./poster/marble_worlddomination.jpg"><img border="0" height="263" src="./poster/marble_worlddomination_thumb.jpg" alt="World domination -- one step at a time."></a> </dt></dl>
</p>

<p>Marble has got an outstanding community: on our mailing lists and IRC channels we
try to answer all questions and try to help everybody voluntarily.</p>
<p>However there might be critical projects and situations where you might want to rely on
commercial support:</p>

<h3><a name="Reviews">Commercial Support for Marble</a></h3>

<ul>
<li>You'd like to have Marble extended with some custom feature? And you need it quickly in time?
<li>You want to develop a custom app based on Marble and you need development support?
<li>You'd like to get Qt and Marble programming training?
</ul>

<p>There are several companies who provide commercial support for Marble. Here's a list of companies which provide commercial support for Marble and which have a <i>track-record for Marble development</i>:

<p>
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
    <tr>
        <th valign="middle">Company logo</a></th> <!--logo-->
        <th valign="middle">Name/Website</th> <!--company name-->
        <th valign="middle">Contact</th> <!--contact-->
        <th valign="middle" >Support options</th> <!--support options-->
    </tr>
    <tr>
        <td valign="middle"><a href="http://www.basyskom.com"><img border="0" src="./support/basyskom.gif"></a></td> <!--logo-->
        <td valign="middle"><a href="http://www.basyskom.de/index.pl/basysEmbedded">basysKom GmbH</a></td> <!--company name-->
        <td valign="middle"><a href="mailto:info@basyskom.de">info@basyskom.de</a></td> <!--contact-->
        <td valign="middle" >Custom Marble solutions, training</td> <!--support options-->
    </tr>
    <tr>
        <td valign="middle"><a href="http://www.c-xx.com"><img border="0" src="./support/c-xx.com-logo.png"></a></td> <!--logo-->
        <td valign="middle"><a href="http://www.c-xx.com">c-xx.com / Hoffmann Information Services GmbH</a></td> <!--company name-->
        <td valign="middle"><a href="mailto:mail@c-xx.com">mail@c-xx.com</a></td> <!--contact-->
        <td valign="middle" >Custom Marble solutions, training</td> <!--support options-->
    </tr>
</table>

<p>Companies who are providing support for Marble are encouraged to <i>contribute back</i> to the Marble Project if possible. This helps Marble to stay a competitive solution. And it helps to build further projects on top.
<p>If your company offers commercial support for Marble and if you'd like your company name added to this page please contact <a href="mailto:marble-devel@kde.org">marble-devel@kde.org</a>.

<br><br>

<?php require 'footer.inc'; ?>
