<?php

  // error_reporting(E_ALL);
  // ini_set("display_errors", 1); 
  $translation_file = "edu-kde-org";
  require_once "functions.inc";
  $site_root = "../";
  $page_title = i18n_noop( "Marble" );

  include ( "header.inc" );

  $appinfo = new AppInfo( "Marble" );
  $appinfo->setIcon( "./logo/marble-logo-72dpi.png", "328", "92" );
  $appinfo->setVersion( "1.4.3" );
  $appinfo->setCopyright( "2005", "The Marble Project" );
  $appinfo->setLicense("lgpl");
  $appinfo->show();

  $submenu->show();

?>

<p>The Marble website has moved to <a href="http://marble.kde.org">marble.kde.org</a></p>
<p>We are redirecting content as needed. Please update your bookmarks and enjoy our new website at <a href="http://marble.kde.org">marble.kde.org</a>!</p>

<p>
<?php i18n( "Last update:" ); ?> <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
