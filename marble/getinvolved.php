<?php 
  $translation_file = "edu-kde-org";
  require_once "functions.inc";
  $site_root = "../";
  $page_title = i18n_noop("Get Involved with Marble");

  include ( "header.inc" );
  
  $submenu->show();

?>

<p>
<dl> <dt> <a href="./poster/marble_history.jpg"><img border="0" height="263" src="./poster/marble_history_thumb.jpg" alt="We're making history together."></a> </dt></dl>
</p>

<p><?php i18n( "Welcome to Marble. By joining this project, you will be part of an international team working on a virtual globe and world atlas. There are many different ways you can help to improve Marble." ); ?></p>
<br/>

<h3><a name="maps"><?php i18n( "Contact: Mailing lists and IRC" ); ?></a></h3>

 <p>
    <?php i18n( "The KDE Education team is very open to new people: Join us and have a chat with us on IRC" ); ?>

  <ul>
    <li> <?php i18n( "<b>#kde-edu</b>, server: irc.freenode.net" ); ?>
    <li> <?php i18n( "<b>#marble</b>, server: irc.freenode.net" ); ?>
  </ul>

 <p>
    <?php i18n( "Also you can reach us via our mailing lists:" ); ?>

  <ul>
    <li> <?php i18n( "<b><a href=\"https://mail.kde.org/mailman/listinfo/kde-edu\">kde-edu@kde.org</a></b> for all educational topics that are related to Marble" ); ?>
    <li> <?php i18n( "<b><a href=\"https://mail.kde.org/mailman/listinfo/marble\">marble@kde.org</a></b> for general discussions about Marble" ); ?>
  </ul>
 </p>.
 <p><?php i18n( "For Marble development it's a good idea to join our development mailing lists:" ); ?>
  <ul>
    <li> <?php i18n( "<b><a href=\"https://mail.kde.org/mailman/listinfo/marble-devel\">marble-devel@kde.org</a></b> for development discussions about Marble" ); ?>
    <li> <?php i18n( "<b><a href=\"https://mail.kde.org/mailman/listinfo/marble-commits\">marble-commits@kde.org</a></b> if you want to follow the progress of Marble's source code." ); ?>
    <li> <?php i18n( "<b><a href=\"https://mail.kde.org/mailman/listinfo/marble-bugs\">marble-bugs@kde.org</a></b> for all people who want to fix Marble bugs." ); ?>
  </ul>
 </p>.

 <p><?php i18n( "To see how exciting Marble development is, see the <a href=\"http://devel-home.kde.org/~tackat/marble/marble-code_swarm-1600.avi\">Marble Code Swarm</a> video (created by Jens-Michael Hoffmann using \"code_swarm\") ..." ); ?></p>
<p>
    <a href="http://devel-home.kde.org/~tackat/marble/marble-code_swarm-1600.avi"><img border="0" width="329" height="233" src="./codeswarm.jpg"></a>
</p>

<h3><a name="maps"><?php i18n( "Maps" ); ?></a></h3>
 <p>
  <?php i18n( "Please help by contributing maps! It's important to us that the maps are licensed in the spirit of Free Software. For maps that are intended to get shipped together with Marble it's important that the license is compatible with the Debian Free Software Guidelines. Often content on the internet is provided under the requirement of \"non-commercial usage\". Note that \"non-commercial usage\" is not enough and is not compatible with our goals (as it would basically forbid inclusion in all Linux distributions)." ); ?>
 <p>
  <?php i18n( "There are many ways to contribute maps:" ); ?>
  <ul>
    <li> <?php i18n( "Join the OpenStreetMap Project!" ); ?>
    <li> <?php i18n( "Check Wikipedia or other sources for historic maps. Especially here it's important that the license is clear and is compatible. Adjust those maps " ); ?>
    <li> <?php i18n( "Find maps which might be interesting for Marble. For maps that are intended to get shipped together with Marble it's important that the topic covered is of general interest or has an important educational aspect covered. All other maps can get shipped via the Get Hot New Stuff technology." ); ?>
  </ul>
 </p>

<h3><a name="documentation"><?php i18n( "Documentation" ); ?></a></h3>
 <p>
  <?php i18n( "You can also help by updating the online help. By providing high quality and up-to-date documentation, you will make a big impact on helping people understand Marble. It would be great if Marble would have some chapters that would cover typical class-room geography topics. Ideally combined with some small hands-on lessons which would make use of Marble to demonstrate geographical aspects." ); ?>
 </p>

<h3><a name="art"><?php i18n( "Artwork and Design" ); ?></a></h3>
 <p>
  <?php i18n( "Marble needs artists! There are many fun things to improve:" ); ?>
  <ul>
  <li><?php i18n( "E.g. we'd like to have a cartoon style map for children included that shows popular animals that are found in a specific area." ); ?></li> 
  <li><?php i18n( "Or a cartoon style sight-seeing map for children that shows popular tourist attractions for each country (like the Eiffel Tower in Paris, the Colosseum in Rome, etc.)." ); ?></li>
  <li><?php i18n( "Of course we are also always interested in getting better Oxygen-style icons." ); ?></li>
  </ul>
 </p>

<h3><a name="webpage"><?php i18n( "Homepage" ); ?></a></h3>
 <p>
  <?php i18n( "Improving and maintaining this web site would be a nice thing to do. Someone needs to regularly update the news items and adjust the content to reflect Marble's improvements." ); ?>
 </p>

<h3><a name="development"><?php i18n( "Development" ); ?></a></h3>
 <p>
  <?php i18n( "You like to write source code and feel like spending some time improving Marble? Let us know, we're looking forward to help you getting started! Before you start it's probably best to read the \"Marble's Secrets\" blog series:" ); ?>
  <ul>
    <li><a href="http://www.kdedevelopers.org/node/3269"><?php i18n( "Marble's Secrets Part I" ); ?></a></li>
    <li><a href="http://www.kdedevelopers.org/node/3272"><?php i18n( "Marble's Secrets Part II" ); ?></a></li>
    <li><a href="http://www.kdedevelopers.org/node/3275"><?php i18n( "Marble's Secrets Part III" ); ?></a></li>
  </ul>
  <p><?php i18n( "Afterwards we suggest to check out the source code and compile Marble for a start. This is usually a matter of about 10-15 mins. Refer to the <a href=\"./obtain.php\">obtain page</a> how to get Marble's source code and how to compile it. Especially the Qt version of Marble is trivial to compile as there is just a single dependency. But with KDE 4 being shipped by various distributors already the difference shouldn't matter too much." ); ?></p>
  <p><?php i18n( "There is a <a href=\"https://projects.kde.org/projects/kde/kdeedu/marble/repository/revisions/master/entry/TODO\">TODO file</a>. You might want to have a look at it. We suggest that you get in touch with us before you start to work on bigger issues to discuss implementation issues." ); ?>
 </p>

<h2><?php i18n( "Quality Assurance" ); ?></h2>
 <p>
    <?php i18n( "With a complex piece of software like Marble it's important for us to make sure that Marble works on all binary packages provided. We'd like to have a team of people testing packages and providing fast feedback about what might be wrong." ); ?>
 </p>

<h2><?php i18n( "Bugs and Features" ); ?></h2>
 <p>
  <?php i18n( "One thing that helps tremendously to improve Marble is your feedback! Already many great improvements were suggested by users. Some ideas are so great, that it's a pleasure to implement them. To help getting features and bugs organized, please use the <a href=\"https://bugs.kde.org/wizard.cgi\">bug and feature reporting wizard</a> at <a href=\"https://bugs.kde.org\">bugs.kde.org</a>." ); ?>
 </p>

<br/>
 <p>
  <?php i18n( "Join us and have a chat with us on our <a href=\"http://webchat.freenode.net/?channels=marble\">Marble Chat</a> (server: irc.freenode.net, channel: #marble) or contact us via the <a href=\"https://mail.kde.org/mailman/listinfo/kde-edu\">kde-edu mailing-list</a>. This is a great opportunity to get involved with an open source project!" ); ?>
 </p>

<?php require 'footer.inc'; ?>
