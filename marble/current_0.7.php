<?php
  $site_root = "../";
  $page_title = 'Marble 0.7 Release';

  include ( "header.inc" );
  $submenu->show();

?>
<h3><a name="description">Visual ChangeLog: Marble 0.7</a></h3>

 <p>Marble 0.7 got released together with KDE 4.2 on January 27th 2009. Compared to <a href="http://edu.kde.org/marble/current_0.6.php">Marble 0.6</a> there have been quite some changes:</p>

<h4>Support for other celestial bodies</h4>

<p>
<dl> <dt> <a href="./screenshots/0.7/moon1.jpg"><img border="0" width="410" height="290" src="./screenshots/0.7/moon1_thumb.jpg" alt="Moon phases in Marble"></a> </dt> <dd><i>Moon phases in Marble</i></dd> </dl>
</p>

<p>Marble 0.7 comes with a few <i>new educational maps</i>. One of them is a map of the <b>moon</b> which features all known craters and moon landing places. Not only does Marble show the <i>Apollo landing sites</i> it also includes the impact location of the most recent indian moon mission (<a href="http://de.wikipedia.org/wiki/Chandrayaan-1"><i>Chandrayaan-1's MIP</i></a>). If you enable sun shading you can also watch the change of the <i>moon phases</i> in Marble.</p>

<p>
<dl> <dt> <a href="./screenshots/0.7/moon2.jpg"><img border="0" width="410" height="290" src="./screenshots/0.7/moon2_thumb.jpg" alt="The big crater Tycho and its rays"></a> </dt> <dd><i>The big crater Tycho and its rays</i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/0.7/mars1.jpg"><img border="0" width="410" height="290" src="./screenshots/0.7/mars1_thumb.jpg" alt="The red planet Mars in Marble"></a> </dt> <dd><i>The red planet Mars in Marble</i></dd> </dl>
</p>

<p>Using the <i>Get Hot New Stuff dialog</i> you can download further maps in Marble. Another celestial body that you'll find there is <b>Mars</b>. After downloading the Mars map theme you can explore this red planet, too and you can pay the two Mars Exploration Rovers Spirit and Opportunity a visit. Of course all Marble features honor the different dimensions of the other bodies in the solar system, so using the measure tool will give proper results in each case.</p>

<p>
<dl> <dt> <a href="./screenshots/0.7/mars2.jpg"><img border="0" width="410" height="290" src="./screenshots/0.7/mars2_thumb.jpg" alt="Mars in flat projection"></a> </dt> <dd><i>Mars in flat projection</i></dd> </dl>
</p>

<h4>Improved KML support</h4>

<p>
<dl> <dt> <a href="./screenshots/0.7/kml1.jpg"><img border="0" width="410" height="290" src="./screenshots/0.7/kml1_thumb.jpg" alt="KML support: Time zones"></a> </dt> <dd><i>KML support: Time zones</i></dd> </dl>
</p>

<p>We have further worked on improving KML support for Marble. KML is the file format used by other virtual globes such as Google Earth to display user data.
Special focus has been put on displaying all kinds of polygons for this release. As an example you can see a KML file displayed inside Marble which shows the <i>timezones</i>.
The other screenshot shows a KML file which we've created ourselves which has the word "Marble" displayed on top of the globe:</p>

<p>
<dl> <dt> <a href="./screenshots/0.7/kml2.jpg"><img border="0" width="410" height="290" src="./screenshots/0.7/kml2_thumb.jpg" alt="KML support: Colorful Marble writing"></a> </dt> <dd><i>KML support: Colorful Marble writing</i></dd> </dl>
</p>

<h4>Support for historical maps</h4>

<p>
<dl> <dt> <a href="./screenshots/0.7/historical1.jpg"><img border="0" width="410" height="290" src="./screenshots/0.7/historical1_thumb.jpg" alt="A historical map from 1687"></a> </dt> <dd><i>A historical map from 1687</i></dd> </dl>
</p>

<p>A feature which had been often requested was to show the earth at different times in history. We've taken one of Wikipedia's <a href="http://en.wikipedia.org/wiki/File:World_Map_1689.JPG">finest pictures</a> of a historical map and <a href="http://techbase.kde.org/Projects/Marble/HistoricalMaps">converted it</a> so that it would be possible to display the map in Marble. Notice how the legend got adjusted to fit well with the "new" old map. We've also added a feature which would show the accurate coastlines as they are known today. This way you can compare how much the cartographer was "off" when he drew the map back in 1687.</p>

<p>
<dl> <dt> <a href="./screenshots/0.7/historical2.jpg"><img border="0" width="410" height="291" src="./screenshots/0.7/historical2_thumb.jpg" alt="Historical map with accurate coast lines blended over"></a> </dt> <dd><i>Historical map with accurate coast lines blended over.</i></dd> </dl>
</p>

<h4>Miles and more ...</h4>

<p>
<dl> <dt> <a href="./screenshots/0.7/miles.jpg"><img border="0" width="410" height="290" src="./screenshots/0.7/miles_thumb.jpg" alt="Support for the Imperial Uni system"></a> </dt> <dd><i>Support for the imperial unit system</i></dd> </dl>
</p>

<p>Additionally we've tweaked Marble in lots of other places: We've added support for the deprecated Imperial Unit System so that people are able to measure distances in miles.</p>
<p>Another feature that users have been frequently asking for is having the map navigation control displayed on the map directly. This feature has been implemented as well. With Marble 0.7 it's now also possible to rearrange the infoboxes to your liking.
</p>

<h4>Marble world clock plasmoid</h4>

<p>
<dl> <dt> <a href="./screenshots/0.7/worldclock.jpg"><img border="0" width="410" height="257" src="./screenshots/0.7/worldclock_thumb.jpg" alt="The beautiful world clock plasmoid."></a> </dt> <dd><i>The beautiful world clock plasmoid.</i></dd> </dl>
</p>

<p>Marble 0.7 is the first release that comes with its own plasmoid for KDE 4's Plasma desktop. The plasmoid shows the time for different cities all over the world. It allows you to change the projection and it has got support for full time zone names.</p>
 <p>
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>

 <?php
   include "footer.inc";
 ?>












