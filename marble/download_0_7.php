<?php
  $site_root = "../";
  $page_title = 'Marble Download';
  
  include ( "header.inc" );

  $submenu->show();

?>

<H3>Download</H3>

  <p>
    Marble is available in two flavours: The Marble-KDE and Marble-Qt:</p> 
    <ul>
      <li> Marble-Qt requires only Qt as a dependency. The only drawback is that
      Marble-Qt doesn't provide the whole functionality of Marble (e.g.
      the settings dialog or automatic map download via "GHNS")</p> 
      If you're sitting on a platform where KDE 4 is not installed already 
      this might be the right choice (e.g. for KDE 3.5 users, Gnome users, 
      Windows and Mac OS X).
      <li> Marble-KDE provides the full experience. It provides all of 
      the user interface and is the recommended flavour.
    </ul>
    <p>If you want to download sources, please check our <a href="./obtain.php">Compiling Marble</a> section.
  </p>


<h4>Marble-Qt</h4>
<p>For MS Windows and MacOSX we provide packages for download. Please provide feedback, as these packages worked for us but we can't ensure that they'll work elsewhere:</p>
<p>
  <a href="http://developer.kde.org/~tackat/marble_0_7/marble-setup-0.7.1.exe"><img border="0" width="400" height="70" src="./marble_dl_windows.png"></a>
</p>
<br>
<p>
  <a href="http://developer.kde.org/~tackat/marble_0_7/Marble-0.7.1.dmg"><img border="0" width="400" height="70" src="./marble_dl_macosx.png"></a>
</p>

<p>For <b>Debian (Lenny)</b> there are also current packages of Marble 0.6 available at </p>


<div id="linux_downloadlinks">
[
  <a href="http://packages.debian.org/lenny/marble">Debian (Lenny)</a> |
]
</div>

<p>For the <b>Asus EeePC</b> there are packages available at:</p> 

<div id="eeepc_downloadlinks">
[
  <a href="http://www.yet-another-geek.org/">Asus EeePC</a> |
]
</div>


<h4>Marble-KDE 0.7.1</h4>
<p>To install Marble-KDE we suggest that you obtain Marble and KDE 4.2 from <a href="http://www.kde.org/download/">your favourite distributor.</a></p>

  <a href="http://www.kde.org/download/"><img border="0" width="400" height="70" src="./marble_dl_kde.png"></a>


<h4>Marble 0.7.1 - Source Code</h4>
<p>Programmers might be interested in getting the latest source code for Marble.
To obtain the most recent source code please follow our instructions in our <a href="./obtain.php">Compiling Marble</a> section. For convenience we offer a compressed archive here (Marble 0.6.0 Revision 841427):</p>

  <a href="http://developer.kde.org/~tackat/marble_0_7/marble-0.7.1_rev938110.tar.gz"><img border="0" width="400" height="70" src="./marble_dl_source.png"></a>


<br><hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

