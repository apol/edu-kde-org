

<?php
  $site_root = "../";
  $page_title = 'Marble 1.4: Visual Changelog';

  include ( "header.inc" );

  $submenu->show();
?>
<p>
Marble 1.4 <a href="http://edu.kde.org/marble/download.php">is going to be released</a> on August 1st, 2012 as part of the KDE 4.9 release. See the <a href="http://edu.kde.org/marble/download.php">Download section</a> for Marble
packages. In the good tradition of recent releases, we have collected those changes directly visible to the user. Please enjoy looking over the new and noteworthy:
</p>

<h4>User Interface Enhancements</h4>

<p>
Global search bar, text glow (outlined) effect, more routing controls, bookmarks shown in the map.
</p>

<h4>FlightGear Support</h4>

<p></p>

<dl>
  <!-- @todo: replace video with a new one -->
  <dt><iframe width="640" height="360" src="http://www.youtube-nocookie.com/embed/-yQcJacLtLc?rel=0" frameborder="0" allowfullscreen></iframe></dt>
  <dd><i>Marble can now show the position of an airplane simulated by FlightGear.</i></dd>
</dl>

<h4>Routing Extensions</h4>

<p>
A number of gradual improvements in the routing area lead to an overall enhanced user experience. Just like it was possible in the mobile versions before, Marble's desktop version now let's you open and save routes as KML files. New turn instruction types and voice commands are supported and tasks like reversing or clearing the route got their own buttons to reduce the number of clicks needed to accomplish common tasks. Support for MapQuest and the Open Source Routing Machine (OSRM) has been added. Just like the existing five routing backends they are queried (if enabled) in the background to deliver alternative routes you can choose from. Both OSRM and MapQuest support worldwide routing; OSRM is known for speedy results thanks to its underlying fast routing algorithm (contraction hierarchies), while MapQuest carefully analyzes your route to generate versatile turn directions.
</p>

<dl> <dt> <a href="./screenshots/1.4/marble-routing.png"><img border="0"
width="400" height="305" src="./screenshots/1.4/marble-routing_thumb.png"
alt=""></a> </dt> <dd><i>A motorcar route calculated by the new MapQuest routing backend.</i></dd>
</dl>

<h4>Postal Codes Online Service</h4>

<p>
With the paperless office still to arrive snail mail plays an ever important role in daily life. Part of the addressing scheme in nearly all countries worldwide are postal codes (also known as post codes, ZIP codes or PIN codes). Each postal code usually covers a certain geographical area. Marble can now help you visualizing these areas and identifying their spatial relationship once you activate the postal code plugin. It was developed by Valery Kharitonov during Google Code-In 2011 to join Marble's list of online services.
</p>

<dl> <dt> <img border="0"
width="480" height="480" src="./screenshots/1.4/marble-postal-codes.png"
alt=""> </dt> <dd><i>Use the postal code plugin to show local postal codes on top of any map theme.</i></dd>
</dl>

<h4>Basic ESRI Shapefile Support</h4>

<p>Now Marble sports initial support for loading ESRI Shapefile polygons. This is done via a dedicated file loading plugin. In order to make use of this feature the Marble compile needs to link against libshp (http://shapelib.maptools.org).
Note that at the current point there is no styling done.

<h4>Further Changes</h4>

<p>Another nifty addition to the latest version of Marble is support for the logfiles of the TangoGPS application.</p>

<p>

Join our Marble Page in your favorite social network:

<table cellspacing="10">
<tr valign="bottom"><td>
<a href="http://opendesktop.org/groups/?id=439"><img src="http://static.opendesktop.org/img/headers/header1_10_1.jpg" class="showonplanet" /></a>
</td><td>
<p><!-- Facebook Badge START --><a href="http://www.facebook.com/marbleglobe" target="_TOP" title="Marble (Virtual Globe)"><img src="http://badge.facebook.com/badge/112069922186463.730.115653362.png" width="120" height="177" style="border: 0px;" /></a><br/><!-- Facebook Badge END -->
</td><td>
<a href="https://plus.google.com/109002795119670287274?prsrc=3" style="text-decoration: none; color: #333;"><div style="display: inline-block; *display: inline;"><div style="text-align: center;"><img src="https://ssl.gstatic.com/images/icons/gplus-64.png" width="64" height="64" style="border: 0;"/></div><div style="font: bold 13px/16px arial,sans-serif; text-align: center;">Marble</div><div style="font: 13px/16px arial,sans-serif;"> on Google+ </div></div></a>
</td>
</tr>
</table>

<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
   include "footer.inc";
?>
