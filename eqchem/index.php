<?php
  $site_root = "../";
  $page_title = 'eqchem';
  
  include ( "header.inc" );

  $appinfo = new AppInfo( "eqchem" );
  $appinfo->setVersion( "0.4.1" );
  $appinfo->setCopyright( "2004", "Thomas Nagy" );
  $appinfo->setLicense("gpl");
  $appinfo->addAuthor( "Thomas Nagy", "tnagy2^8@yahoo.fr" );
  $appinfo->show();
?>

<br />
<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#news">News</a>
]
</div>

<h3><a name="description">Description</a></h3>
<p>eqchem is a KDE application that balances chemical equations. eqchem solves simple chemical equations of the form:<br />
a formula1 + b formula2 ... -> c formula3 + d formula4 ..<br />
and finds integer values for the quantities a, b, c, d.<br />
It can be used to point out mistakes in formula or for searching new ones.<br />
</p>

<h3><a name="dependencies">Dependencies</a></h3>
<ul>
<li><b>Ocaml</b><br /> 
webpage: <a href="http://caml.inria.fr/resources/index.en.html">http://caml.inria.fr/resources/index.en.html</a><br />
download: <a href="http://caml.inria.fr/download.en.html">http://caml.inria.fr/download.en.html</a>
</li>
<li><b>FaCiLe</b>(only needed for packagers and for compiling the source)<br />
webpage: <a href="http://www.recherche.enac.fr/opti/facile/">http://www.recherche.enac.fr/opti/facile/</a><br />
download: <a href="http://caml.inria.fr/download.en.html">http://caml.inria.fr/download.en.html</a>
</li>
</ul>
To install FaCiLe, untar the tarball, ./configure, make, make check, su -c 'make install'
<br />

<?php
  kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>








