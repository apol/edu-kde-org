<?php
  $page_title = "How to obtain eqchem";
  $site_root = "../";
  

  include( "header.inc" );
?>
<p>
eqchem is only in CVS HEAD, in the kdeplayground-edu module.
</p>
<h4>Dependencies</h4>
<ul>
<li><b>objective caml</b>: you can obtain it with your distribution (the package is likely to be named ocaml</li>
<li><b>the FaCiLe library</b> (facile 1.1), you can get a tarball from there: <a href="http://www.recherche.enac.fr/opti/facile/distrib">http://www.recherche.enac.fr/opti/facile/distrib</a> that you can easily compile.
</li>
</ul>

<h4>Get eqchem from CVS HEAD</h4>
You need qt-copy and kdelibs at least from CVS HEAD.
<pre>
    $ cvs co -l kdeplayground-edu
    $ cd kdeplayground-edu
    $ cvs co admin  (or ln -s ../kde-common/admin .admin)
    $ cvs up eqchem
    $ make -f Makefile.cvs
    $ ./configure --prefix=$KDEDIR
    $ cd eqchem
    $ make
    $ su -c 'make install'
</pre>
<p>
Please <b>don't forget to set your QTDIR and KDEDIR variables</b> to point to the correct dir.<br />
</p>

<br />
<hr width="30%" align="center" />
<p>Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>


