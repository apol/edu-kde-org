<ul>
<li>deal with the bugs in bugs.kde.org</li>
<li>update Configure Kiten dialog using KConfig XT</li>
<li>add WhatsThis help for each widget and each dialog</li>
<li>update the website on edu.kde.org</li>
<li>i18n data the way KHangMan does it (in i18n modules)</li>
</ul>