<?xml version="1.0"?>
<!DOCTYPE rss SYSTEM "http://my.netscape.com/publish/formats/rss-0.91.dtd">
<rss version="0.91">
  <channel>
    <title>edu.kde.org: The KDE Education Project</title>
    <link>http://edu.kde.org/</link>
    <description>KDE Edu is promoting the development of educational software within KDE</description>
    <language>en-us</language>
    <webMaster>annma@kde.org (Webmaster)</webMaster>
    <copyright>Copyright (c) 2003, edu.kde.org Webmaster</copyright>

<item>
<title>New Cantor website</title>
<date>November 16th, 2019</date>
<fullstory>
    <p><a href="https://dot.kde.org/2019/09/07/kde-decides-three-new-challenges-wayland-consistency-and-apps">KDE is all about apps</a>! Following this new trend in community, Cantor developers look for some way to comunicate more efficiently to the current and potential user base.</p>
    <p>This way we developed a new website in a more easy to find domain. If you want to follow the news about Cantor features and development, please go to <a href="https://cantor.kde.org/">cantor.kde.org</a> and help us to spread Cantor to new users!</p>
    <p>From now this page in <b>edu.kde.org/cantor</b> will be unmaintained, but all the content here was migrated to the new page.</p>
</fullstory>
</item>
<item>
<title>Cantor in GSoC 2019 - Support to Jupyter notebooks</title>
<date>June 9th, 2019</date>
<fullstory>
    <p>This year Cantor has a slot in Google Summer of Code program. The developer Nikita Sirgienko, mentored by Alexander Semke, is working to provide support to Jupyter notebooks in Cantor.</p>
    <p>The idea is provide a way to import/export Jupyter notebooks to/from Cantor. It could improve the Cantor userbase and popularity if the software can manage the main notebook format available at the moment.</p>
    <p>You can read Nikita's proposal in <a href="https://summerofcode.withgoogle.com/projects/#4583302302793728">GSoC 2019 page</a> and follow the news in the <a href="https://sirgienkogsoc2019.blogspot.com/">project website</a>.</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 19.04</title>
<date>April 18th, 2019</date>
<fullstory>
    <p>Cantor new version was released with <a href="https://kde.org/announcements/announce-applications-19.04.0.php">KDE Applications 19.04</a> bundle. The main changes are:</p>
    <ul>
        <li>Possibility to hide and show results of command entry via context menu;</li>
        <li>Add a way to specify the path to the local documentation for Maxima, Octave, Python and R backends;</li>
        <li>Huge improvements in variable management.</li>
    </ul>
    <p>Read the complete changelog for this version of Cantor in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=19.04.0">KDE Applications 19.04 Full Log Page</a>.</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 18.12</title>
<date>December 13th, 2018</date>
<fullstory>
    <p>Cantor new version was released with <a href="https://kde.org/announcements/announce-applications-18.12.0">KDE Applications 18.12</a> bundle. The main changes are:</p>
    <ul>
        <li>Add Markdown entry type;</li>
        <li>Animated highlighting of the currently calculated command entry;</li>
        <li>Visualization of pending command entries (queued, but not being calculated yet);</li>
        <li>Allow to format command entries (background color, foreground color, font properties);</li>
        <li>Allow to insert new command entries at arbitrary places in the worksheet by placing the cursor at the desired position and by start typing;</li>
        <li>For expressions having multiple commands, show the results as independent result objects in the worksheet;</li>
        <li>Add support for opening worksheets by relative paths from console;</li>
        <li>Add support for opening multiple files in one Cantor shell;</li>
        <li>Change the color and the font for when asking for additional information in order to better discriminate from the usual input in the command entry;</li>
        <li>Added shortcuts for the navigation across the worksheets (Ctrl+PageUp, Ctrl+PageDown);</li>
        <li>Add action in 'View' submenu for zoom reset;</li>
        <li>Enable downloading of Cantor projects from store.kde.org (at the moment upload works only from the website);</li>
        <li>Open the worksheet in read-only mode if the backend is not available on the system.</li>
    </ul>
    <p>Read the complete changelog for this version of Cantor in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=18.12.0">KDE Applications 18.12 Full Log Page</a>.</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 18.08</title>
<date>August 16th, 2018</date>
<fullstory>
    <p>Cantor new version was released with <a href="https://www.kde.org/announcements/announce-applications-18.08.0.php">KDE Applications 18.08</a> bundle. The main changes are:</p>
    <ul>
        <li>Support to Python 3.7;</li>
        <li>Recommended Sage version changed to 8.1 and 8.2;</li>
        <li>Improve LaTeX worksheet export;</li>
        <li>A lot of bug fixes.</li>
    </ul>
    <p>Read the complete changelog for this version of Cantor in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0">KDE Applications 18.08 Full Log Page</a>.</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 18.04</title>
<date>April 19th, 2018</date>
<fullstory>
    <p>Cantor new version was released with <a href="https://www.kde.org/announcements/announce-applications-18.04.0.php">KDE Applications 18.04</a> bundle. The main changes are:</p>
    <ul>
        <li>Add shortcuts to evaluate and interrupt the running calculation;</li>
        <li>Port Qalculate and Scilab to QProcess;</li>
        <li>A lot of bug fixes;</li>
        <li>R backend is now fixed!</li>
    </ul>
    <p>Read the complete changelog for this version of Cantor in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0">KDE Applications 18.04 Full Log Page</a>.</p>
</fullstory>
</item>
<item>
<title>Proposal discussion: Python 3 as the only backend officially supported</title>
<date>January 08th, 2018</date>
<fullstory>
    <p>The Cantor maintainer Filipe Saraiva <a href="http://blog.filipesaraiva.info/?p=1897">wrote a post</a> to discuss the future of Cantor. The main idea in the text is focus effort only in Python 3 backend in order to provide a better experience to the users and allow the work in new features for Cantor itself.</p>
    <p>The other backends would be moved to a third-party repository and they will not be officially maintained. They could be available as extensions in KDE Store if someone would like to maintain some of them.</p>
    <p>This is just a proposal and it is open to discussions. Please, join the conversation in Filipe's blogpost or in <a href="https://mail.kde.org/mailman/listinfo/kde-edu">KDE-Edu mailing list</a>.</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 17.12</title>
<date>December 14th, 2017</date>
<fullstory>
    <p>Cantor new version was released with <a href="https://www.kde.org/announcements/announce-applications-17.12.0.php">KDE Applications 17.12</a> bundle. This version is more focused in stability and bug fixes.</p>
    <p>Read the complete changelog for this version of Cantor in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0">KDE Applications 17.12 Full Log Page</a>.</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Edu Sprint 2017</title>
<date>October 22th, 2017</date>
<fullstory>
    <p>KDE Edu Sprint 2017 was held in Berlin, from 7th to 9th October. During this meeting, Cantor developers Filipe Saraiva and Rishabh Gupta worked in some tasks and the results will be released soon.</p>
    <p>Please, read the reports of <a href="http://blog.filipesaraiva.info/?p=1885">Filipe Saraiva</a> and <a hreh="https://rish9511.wordpress.com/2017/10/22/kde-edu-sprint-2017/">Rishabh Gupta</a> for more information. A <a href="https://dot.kde.org/2017/11/01/2017-kde-edu-sprint">KDE Edu Sprint 2017 report</a> is also available in KDE website.</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 17.08</title>
<date>August 18th, 2017</date>
<fullstory>
    <p>Cantor new version was released with <a href="https://www.kde.org/announcements/announce-applications-17.08.0.php">KDE Applications 17.08</a> bundle. This version is more focused in stability and bug fixes.</p>
    <p>For this release we implemented a new "programming language version recommendation" to inform users about the best programming language version supported by the backend.</p>
    <p>Scilab 6.0 was tested and is correctly supported by Scilab backend.</p>
    <p>Other small new feature is the LaTeX rendering with dark themes.</p>
    <p>Read the complete changelog for this version of Cantor in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0">KDE Applications 17.08 Full Log Page</a>.</p>
</fullstory>
</item>
<item>
<title>Cantor in GSoC 2017 - Port backends to Q/KProcess</title>
<date>May 20th, 2017</date>
<fullstory>
    <p>Cantor was accepted to Google Summer of Code 2017!</p>
    <p>This year we have an Indian student working with us, Rishabh Gupta. His project is port all Cantor backends to use <a href="http://doc.qt.io/qt-5/qprocess.html">QProcess</a> or <a href="https://api.kde.org/frameworks/kcoreaddons/html/classKProcess.html">KProcess</a> (we are <a href="https://phabricator.kde.org/T6111">investigating</a> which is more suitable for Cantor).</p>
    <p>Currently Cantor backends are implemented using a set of different technologies like KProcess (Scilab, Octave, and Sage backends), DBus (Julia, Python 3 and R), and own languages APIs (Python 2, Lua, and KAlgebra). Because this it is hard to support correctly all of these backends, and it is hard to port Cantor to different operating systems.</p>
    <p>If we define a standard technology to implement backends, we belive it will be more easy to maintain Cantor and port the software to others OS platforms. QProcess and KProcess are interesting alternatives to this because they have good support in different OS platforms.</p>
    <p>You can follow the work of Rishabh in <a href="https://rish9511.wordpress.com/">his blog</a> and track the tasks related to this project in <a href="https://phabricator.kde.org/T6110">this task</a> at KDE Phabricator.</p>
    <p>Have a good work Rishabh and welcome to Cantor hackers team!</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 17.04</title>
<date>April 20th, 2017</date>
<fullstory>
    <p>Cantor new version was released with <a href="https://www.kde.org/announcements/announce-applications-17.04.0.php">KDE Applications 17.04</a> bundle. This version is more focused in stability and bug fixes. Read the complete changelog for this version of Cantor in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0">KDE Applications 17.04 Full Log Page</a>.</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 16.12</title>
<date>December 15th, 2016</date>
<fullstory>
    <p>Cantor new version was released with <a href="https://www.kde.org/announcements/announce-applications-16.12.0.php">KDE Applications 16.12</a> bundle. This version has an interesting new backend and several bugfixes. The main changes are:</p>
    <ul>
        <li>New backend: Julia, developed by Ivan Lakhtanov during his GSoC 2016. More infos in <a href="https://juliacantor.blogspot.com.br/2016/08/cantor-gets-support-of-julia-language.html">Ivan's blog</a>;</li>
        <li>Fix syntax highlight of strings and comments in Python backend;</li>
        <li>Introducing a new feature: recommended programming language version in backend description;</li>
        <li>Fix the crash after close a session of Python, Scilab, or Julia backends;</li>
        <li>More robust version check of Sage.</li>
    </ul>
    <p>A few more bugs were solved too. Read the complete changelog for this version of Cantor in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0">KDE Applications 16.12 Full Log Page</a>.</p>
    <p>Happy Holidays!</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 16.08</title>
<date>August 18th, 2016</date>
<fullstory>
    <p>The new version of Cantor was released with <a href="https://www.kde.org/announcements/announce-applications-16.08.0.php">KDE Applications 16.08</a> bundle. Most of the changes were implemented during <a href="http://blog.filipesaraiva.info/?p=1839">LaKademy 2016.</a>The main changes are:</p>
    <ul>
        <li>libcantorlibs is using the same version number used by KDE Applications bundle;</li>
        <li>Fix tab-completion when the completion is selected using the mouse;</li>
        <li>Support to Sage > 7.0 version;</li>
        <li>LaTeX rendering is back to Sage backend;</li>
        <li>Fix the crash when Sage backend is closed;</li>
        <li>Add new commands to the list of plot commands in Octave backend.</li>
    </ul>
    <p>More bugs were solved too. Read the complete changelog for this version of Cantor in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0">KDE Applications 16.08 Full Log Page</a>.</p>
</fullstory>
</item>
<item>
<title>LabPlot integration with Cantor</title>
<date>July 23th, 2016</date>
<fullstory>
    <p>LabPlot, software to data analysis and visualization, brings a new and cool feature in his 2.3.0 version: integration with Cantor. </p>
    <p>That integration was implemented by Garvit Khatri during <a href="http://garvitdelhi.blogspot.com/2015/08/final-evaluation.html">GSoC 2015</a>. Now LabPlot users can perform calculations in some programming language supported by Cantor and do the plots, all it directly in LabPlot.</p>
    <p>That is possible by the use of libcantorlibs, a set of widgets and libraries to bring the Cantor worksheet, panels and assistants to the software.</p>
    <p>Currently LabPlot supports Maxima and Python backends, but more programming languages will come in next releases.</p>
    <p>Thank you to Garvit Khatri and Alexander Semke for bring this feature to LabPlot, for use Cantor to do it, and for the fixes and improvements developed by you and implemented directly in Cantor.</p>
    <p>Read more about this feature in <a href="http://labplot.sourceforge.net/2016/07/labplot-2-3-0-released/">LabPlot 2.3.0 release announcement</a>.</p>
</fullstory>
</item>
<item>
<title>Cantor in GSoC 2016 - Backend for Julia</title>
<date>April 22th, 2016</date>
<fullstory>
    <p>Cantor was accepted to Google Summer of Code 2016!</p>
    <p>This year we have a Russian student working with us, Ivan Lakhtanov. His project is develop a backend for <a href="http://julialang.org/">Julia</a>. If you want to follow the work of Ivan, please keep your eyes in <a href="http://juliacantor.blogspot.ru/">his blog</a>.</p>
    <p>Have a good work Ivan and welcome to Cantor and KDE!</p>
</fullstory>
</item>
<item>
<title>Cantor in KDE Applications 16.04</title>
<date>April 20th, 2016</date>
<fullstory>
    <p>The new version of Cantor was released with <a href="https://www.kde.org/announcements/announce-applications-16.04.0.php">KDE Applications 16.04</a>. The main changes are: </p>
    <ul>
        <li>Now Cantor is using the version number of KDE Applications version;</li>
        <li>Support to Sage > 6.5 version;</li>
        <li>Fix Octave tab completion;</li>
        <li>The authors will update Cantor website to inform the news of the project;</li>
        <li>Filipe Saraiva is the new maintainer.</li>
        </ul>
    <p>The complete changelog of Cantor can be read in <a href="https://www.kde.org/announcements/fulllog_applications.php?version=16.04.0#cantor">KDE Applications 16.04 Full Log Page</a>.</p>
    <p>We would like to improve the Cantor development, calling developers to coordinate the bugs hunt and the development of new features. Wait for more news soon!</p>
</fullstory>
</item>
<item>
<title>Cantor in kdereview</title>
<date>October 1st, 2009</date>
<fullstory>
    Cantor is now in kdereview in order to be peer-reviewed before joining KDE-Edu for the KDE 4.4.0 release! you are encouraged 
    to try it and report any problems to the author.<br />
</fullstory>
</item>

</channel>
</rss>
