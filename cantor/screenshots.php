<?php
  $site_root = "../";
  $page_title = 'Cantor - Screenshots KDE 4.4.0';
  
  include ( "header.inc" );
?>
<br />

<?php
  $gallery = new EduGallery("Cantor - KDE 4.4.0");
  $gallery->addImage("pics/maxima_plot_160.png", "pics/maxima_plot.png", 160, 155,  "[Screenshot]", "", "Integrated plot in Maxima backend");
  $gallery->addImage("pics/R_plot_160.png", "pics/R_plot.png", 160, 123,  "[Screenshot]", "", "Integrated plot in R backend");
  $gallery->show();
?>


 <br />
 <hr width="30%" align="center" />
 <p>
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>














