<?php
  $site_root = "../";
  $page_title = 'Blinken';
  
  include ( "header.inc" );
  include ( "../site_includes/class_edugallery.inc" );

  $appinfo = new AppInfo( "Blinken" );
  $appinfo->setIcon( "../images/icons/blinken_32.png", "32", "32" );
  $appinfo->setVersion( "0.2" );
  $appinfo->setCopyright( "2005", "Albert Astals Cid" );
  $appinfo->setLicense("gpl");
  $appinfo->addAuthor( "Albert Astals Cid", "aacid@kde.org" );
  $appinfo->addContributor( "Danny Allen", "danny@dannyallen.co.uk", "icon and artwork, design" );
  $appinfo->show();
?>

<br />
<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#news">News</a>
]
</div>

<h3><a name="description">Description</a></h3>
<p>Blinken is the KDE version of the well-known game Simon Says.<br />
Follow the pattern of sounds and lights as long as you can! Press the start game button to begin. Watch the computer and copy the pattern it makes. Complete the sequence in the right order to win.<br />
</p>
<center>
<?php
  $gallery = new EduGallery("Blinken - Screenshot");
  $gallery->addImage("pics/blinken1_sm.png", "pics/blinken1.png", 300, 256, "[Screenshot]", "", "How Blinken looks like");
  $gallery->show();
  ?>
 </center>
<?php
  kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
