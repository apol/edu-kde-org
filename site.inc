<?php

$translation_file = "edu-kde-org";
require_once( "functions.inc" );

require_once( "site_includes/applications.inc" );

$templatepath = "derry/";
$site_title = i18n_var( "The KDE Education Project" );
$site_logo_left = siteLogo( "/media/images/edu.png", "48", "64" );
$site_external = true;
$name = i18n_var( "edu.kde.org Webmaster" );
$mail = "annma@kde.org";
$showedit = false;

$rss_feed_link = "/rss.php";
$rss_feed_title = i18n_var( "Latest KDE-Edu News" );

// track using piwik
$piwikSiteID = 6;
$piwikEnabled = true;

// some global variables
$kde_current_version = "4.4";
$kde_coming_version = "4.5";
$kde_stable_doc_path = "http://docs.kde.org/stable/en/kdeedu/";
$kde_development_doc_path = "http://docs.kde.org/development/en/kdeedu/";

if( $_SERVER['PHP_SELF'] == $url_root."/applications/applicationlist.php" || $_SERVER['PHP_SELF'] == $url_root."/applications/applicationpage.php" ) {
  $current_relativeurl = "applications/" . $_GET['category'] . "/";
  if( isset( $_GET['application'] ) ) $current_relativeurl .= $_GET['application'] . "/";
}

include_once "site_includes/class_submenu.inc";
$submenu = new subMenu();

$site_logo = "pics/kde-edu-22.png";

$site_languages = array( 'en', 'ca', 'de', 'el', 'es', 'et', 'fi', 'fr', 'gl', 'it', 'ko', 'ru', 'pl', 'pt', 'pt_BR', 'sv', 'uk' );
?>
