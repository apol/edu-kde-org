<?php
  $site_root = "../";
  $page_title = 'Kanagram';
  
  include ( "header.inc" );

  $appinfo = new AppInfo( "Kanagram" );
  $appinfo->setIcon( "../images/icons/kanagram_32.png", "32", "32" );
  $appinfo->setVersion( "0.2" );
  $appinfo->setCopyright( "2005", "Joshua Keel" );
  $appinfo->setLicense("gpl");
  $appinfo->addAuthor( "Joshua Keel", "joshuakeel@gmail.com" );
  $appinfo->addMaintainer( "Jeremy Whiting", "jeremy@scitools.com", "Current maintainer" );
  $appinfo->addContributor( "Danny Allen", "danny@dannyallen.co.uk", "design and artwork" );
  $appinfo->show();
?>

<br />
<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#news">News</a> |
  <a href="#contributing">Contributing</a>
]
</div>

<h3><a name="description">Description</a></h3>
<p>Kanagram is a replacement for KMessedWords, and is new in KDE 3.5. Kanagram mixes up the letters of a word (creating an anagram), and you have to guess what the mixed up word is. Kanagram features several built-in word lists, hints, and a cheat feature which reveals the original word. Kanagram also has a vocabulary editor, so you can make your own vocabularies, and distribute them through Kanagram's KNewStuff download service.
</p>
<h3><a name="contributing">Contributing</a></h3>
<p>
Kanagram is in need of translators to translate English vocabularies to other languages.
To help out with this effort, it is necessary to get the English vocabulary files (in kanagram/data/en/filename.kvtml).
All that is necessary is to translate one or more of these files into your own language, and send them to the KDE translation team that translates your language.
</p>
<p><img src="../images/web/tutorial.png" width="40" height="58" alt="Tutorial" style="float: left" />   <b>Make a list of new words (Kanagram 3.5)</b><br />  <a href="tutorials/kanagram-addwords.pdf">pdf tutorial</a><br />  <a href="tutorials/kanagram-addwords.odp">odf tutorial</a><br />
</p>

<center>
<?php
  $gallery = new EduGallery("Kanagram - Screenshot");
  $gallery->addImage("pics/kanagram1_sm.png", "pics/kanagram1.png", 165, 126, "[Screenshot]", "", "What Kanagram looks like");
  $gallery->show();
  ?>
 </center>
<?php
  kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<hr width="30%" align="center" />
<p>Author: Joshua Keel<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
