<?php
  $page_title = "Frequently Asked Questions";
  $site_root = "../";
  

  include( "header.inc" );
?>

<?php
  $faq = new FAQ();

  $faq->addQuestion( "Has Kalzium a molecular calculator?",
      "Yes, it has. Kalzium 3.5 has a mini-tool on its sidebar where you can
enter formulas (like C6H12O6) to calculate their weight.<br />
Note that this mini-tool is not a real molecule calculator, so you can not use
it to calculate things like H + 2O" );

  $faq->addQuestion( "The Kalzium simple molecular calculator is not enough for
me, is there anything more advanced?",
      "Yes, there is an equation solver you can reach using the Tools menu
(Tools -&gt; Equation Solver). The solver is called EqChem, and you can find
more information about it at its website:
<a href=\"http://edu.kde.org/eqchem/\">http://edu.kde.org/eqchem/</a>." );

  $faq->addQuestion( "What about the quiz? When will it be back?",
      "I hope soon :-) But probably not inside Kalzium but inside a kdeedu
application yet to be written. Any help is very welcome!");

  $faq->addQuestion( "How can I help Kalzium?",
      "There are a lot ways to help! We really need an updated documentation.
Beside that searching and reporting bugs (via <a
href=\"http://bugs.kde.org\">bugs.kde.org</a>) also helps us.<br/>
If you want to help coding a good start would be the
<a href=\"http://websvn.kde.org/trunk/KDE/kdeedu/kalzium/ideas/TODO\">TODO</a>-file.
You will find a lot points to start from.");

  $faq->addQuestion( "How can make proposals for Kalzium features?",
      "The best way is to file a wishlist on <a
href=\"http://bugs.kde.org\">bugs.kde.org</a>. This way we can't miss it. Other
than that, feel free to write a mail to one of us or visit us in #kalzium or
#kde-edu on irc.kde.org.");

  $faq->addQuestion( "How can I say 'Thanks you' to the developers, donating money and so on?",
      "One way would be to send Carsten one or more items from his <a
href=\"http://www.amazon.de/exec/obidos/registry/269AYCJPSAZ8A/ref=wl_em_to/\">Amazon-Wishlist</a>.
If you don't want to do this contact Carsten. Of course, the same applies to
the other kde-edu developers.");

  $faq->show();
?>

<?php
  include("footer.inc");
?>
