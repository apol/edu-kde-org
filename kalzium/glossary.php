<?php
  $page_title = "The Glossary";
  $site_root = "../";
  

  include( "header.inc" );
?>
<p>This page contains the items of Kalziums glossary. If you have an idea for another item please sent a mail to the developer with the name of the item and the description. The format is not important, we will take care about that.</p>

<h3>Knowledge-items</h3>
<ul>
	<li>State of matter</li>
	<li>Chemical Symbol</li>
	<li>Chromatography</li>
	<li>Distillation</li>
	<li>Element</li>
	<li>Emulsion</li>
	<li>Extraction</li>
	<li>Filtering</li>
	<li>Mix</li>
	<li>Accuracy</li>
	<li>Law of Mass Preservation</li>
	<li>Law of constant proportions</li>
	<li>Crystallization</li>
	<li>Solution</li>
	<li>Mass</li>
	<li>Matter</li>
	<li>Phase</li>
	<li>Precision</li>
	<li>Correctness</li>
	<li>SI-Unit</li>
	<li>Significant Point</li>
	<li>Standard deviation</li>
	<li>Suspension</li>
	<li>Alloys</li>
	<li>Alpha rays</li>
	<li>Atom</li>
	<li>Atomic nucleus</li>
	<li>Atomic Mass</li>
	<li>Beta rays</li>
</ul>
<h3>Tool-items</h3>
<ul>
		<li>Heating Coil</li>
		<li>Cork Ring</li>
		<li>Dropping Funnel</li>
		<li>Separating Funnel</li>
		<li>Test Tube Rack</li>
		<li>Vortexer</li>
		<li>Wash Bottle</li>
		<li>Rotary Evaporator</li>
		<li>Reflux Condenser</li>
		<li>Pileusball</li>
		<li>Test Tube</li>
		<li>Protective Goggles</li>
		<li>Round-bottomed Flask</li>
		<li>Full Pipette</li>
		<li>Drying Tube</li>
		<li>Test tube fastener</li>
		<li>Graduate</li>
		<li>Thermometer</li>
		<li>Magnetic stir bar</li>
		<li>Distillation bridge</li>
		<li>Syringe</li>
		<li>Separation Beaker</li>
		<li>Burner</li>
		<li>Extractor Hood</li>
		<li>Contact Thermometer</li>
		<li>Clamps</li>
		<li>Litmus Paper</li>
		<li>Short-stem Funnel</li>
		<li>Buret</li>
</ul>


<hr width="30%" align="center" />
<p>Author: Carsten Niehaus<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include("footer.inc");
?>
