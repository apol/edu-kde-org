<?php
  $page_title = "Chemical Information";
  $site_root = "../";
  

  include( "header.inc" );
?>

<h3></h3>
<p>There are several chemical applications out there. This is a list
of the ones I find especially useful.
</p>

<h4>XDrawChem</h4>
<p>XDrawChem is a <a href="http://www.trolltech.com">Qt</a>-based application
to draw molecular structures.</p>
<p>Homepage: <a href="http://xdrawchem.sourceforge.net/">http://xdrawchem.sourceforge.net/</a></p>

<h4>Jmol</h4>
<p>With Jmol you can view molecules inside your browser. All you need is java enabled in your browser.</p>
<p>Homepage: <a href="http://jmol.sourceforge.net/">http://jmol.sourceforge.net/</a></p>

<h4>GChemistry</h4>
<p>The Gnome Chemistry Utils provide C++ classes and Gtk+-2 widgets related to chemistry.</p>
<p>Homepage: <a href="http://www.nongnu.org/gchemutils/">http://www.nongnu.org/gchemutils/</a></p>

<h4>ACDLabs</h4>
<p>This software package -- free for students, runs via wine on Linux -- is in my opinion
the best drawingapplication. It is really almost perfect and even supporting a 3D-viewer.</p>
<p>Homepage: <a href="http://www.acdlabs.com/">http://www.acdlabs.com/</a></p>

<h4>ChemTool</h4>
<p>ChemTool is a GTK application for drawing chemical structures.</p>
<p>Homepage: <a href="http://ruby.chemie.uni-freiburg.de/~martin/chemtool/chemtool.html">http://ruby.chemie.uni-freiburg.de/~martin/chemtool/chemtool.html</a></p>

<h4>Open Babel</h4>
<p>Open Babel is a project designed to interconvert between many file formats used in molecular modeling and computational chemistry.</p>
<p>
Since KDE4, Kalzium is using OpenBabel (2.1+) if you have it installed. It is used to 
calculate and display 3D-structures in a OpenGL-based moleculeviewer
</p>
<p>Homepage: <a href="http://openbabel.sourceforge.net/">http://openbabel.sourceforge.net/</a></p>

<h4>Ghemical</h4>
<p>Homepage: <a href="http://www.bioinformatics.org/ghemical/ghemical/index.html">http://www.bioinformatics.org/ghemical/ghemical/index.html</a></p>

<hr width="30%" align="center" />
<p>Author: Carsten Niehaus<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include("footer.inc");
?>
