<?php
  $page_title = "Iconsets";
  $site_root = "../";

  include( "header.inc" );
?>

<h3>What is this all about?</h3>
<p>
Since KDE4, Kalzium is able to display different iconsets. Of course, to 
display iconsets you need to have the icons, in the first place. 
</p>
<h4>What do you mean by 'Iconset'?</h4>
<p>
The word "Icon" is a bit misleading. When I say <i>icon</i> I mean an 
illustration of an element. For example, carbon is used for many many 
things like your crayon or in the framework of cars. So the "icon" for
carbon could be the picture of a crayon or a car/sailing boat.
</p>
<p>
Of course, an element might have a lot of applications. This means the
many icons might fit for an element. For example, I currently have several
icons for Hydrogen. Also, radioactive elements are not used (outside
power plants). That is why they will all get the same icon (unless I find
a good reason to do otherwise).
</p>

<h3>How can I contribute?</h3>
<p>
If you have artistic abilities then you might want to help out Kalzium! 
</p>

<p>
<a href="http://elements.wlonk.com/">This is a good</a> example of how Kalzium 
would look like, I "just" need the images, the code is done. 
So if you have artistic skills and some free time I would really welcome 
your help! Per element one svg-file is needed, I would do the rest.
</p>

<p>
Even if you would only create one or five icons, each icon would help a lot! 
I created a list of ideas what each icons could display. You find the
<a href="icon_ideas.php">list here</a>. The icons need to be 40:40 (x:y ratio). 
Put the symbol of the icon in the grey box at the bottom. 
Please start with the <a href="files/template.svg">template file</a>
if you start a new icon. If you have an icon you can send it to Carsten (cniehaus _at_ kde _dot_ org) or Kalziums mailinglist.
</p>

<h3>What to do in order to have a consistent iconset?</h3>
<p>
<ul>
<li>
You should use the palette defined by Oxygen. You will find the 
file <a href="http://websvn.kde.org/*checkout*/trunk/playground/artwork/Oxygen/utils/oxygen.gpl">here</a>. You will find detailed instructions how to use the palette <a href="http://developernew.kde.org/Projects/Oxygen/Style">here</a>.
</li>
<li>
The icons have to have a 1:1 (x:y) ratio. That is because the icons will be displayed with 40:40 pixel.
</li>
<li>
Please use a transparent background
</li>
<li>
Add the symbol of the element to the bottom of the icon. Have a look at the example-icon how to do it.
</li>
</ul>
</p>

<h3>A perfect example</h3>
<a href="files/1_4.svg"><img src="files/example.png" alt="The perfect example" /></a>

<h3>Current status</h3>
<table border="1">
  <tr>
    <th>H, Hydrogen</th>
    <th><a href="files/1.svg"><img src="files/1.png" /></a>
    <th><a href="files/1_2.svg"><img src="files/1_2.png" /></a>
    <th><a href="files/1_3.svg"><img src="files/1_3.png" /></a>
    <th><a href="files/1_4.svg"><img src="files/1_4.png" /></a>
  </tr>
  <tr>
    <th>He. Helium</th>
    <th><a href="files/2.svg"><img src="files/2.png" /></a>
    <th><a href="files/2_1.svg"><img src="files/2_1.png" /></a>
  </tr>
  <tr>
    <th>Li. Lithium</th>
    <th><a href="files/3.svg"><img src="files/3.png" /></a>
    <th><a href="files/3_1.svg"><img src="files/3_1.png" /></a>
  </tr>
  <tr>
    <th>Be, Beryllium</th>
    <th><a href="files/4.svg"><img src="files/4.png" /></a>
  </tr>
  <tr>
    <th>B, Boron</th>
    <th><a href="files/5.svg"><img src="files/5.png" /></a>
  </tr>
  <tr>
    <th>C, Carbon</th>
    <th><a href="files/6.svg"><img src="files/6.png" /></a>
  </tr>
  <tr>
    <th>N, Nitrogen</th>
    <th><a href="files/7.svg"><img src="files/7.png" /></a>
  </tr>
  <tr>
    <th>O, Oxygen</th>
    <th><a href="files/8.svg"><img src="files/8.png" /></a>
  </tr>
  <tr>
    <th>F, Flourine</th>
    <th><a href="files/9.svg"><img src="files/9.png" /></a>
  </tr>
  <tr>
    <th>Ne, Neon</th>
    <th><a href="files/10.svg"><img src="files/10.png" /></a>
    <th><a href="files/10_1.svg"><img src="files/10_1.png" /></a>
    <th><a href="files/10_2.svg"><img src="files/10_2.png" /></a>
  </tr>
  <tr>
    <th>Na, Sodium</th>
    <th><a href="files/11.svg"><img src="files/11.png" /></a>
  </tr>
  <tr>
    <th>Mg, Magnesium</th>
    <th><a href="files/12.svg"><img src="files/12.png" /></a>
  </tr>
  <tr>
    <th>Al, Aluminum</th>
    <th><a href="files/13.svg"><img src="files/13.png" /></a>
    <th><a href="files/13_2.svg"><img src="files/13_2.png" /></a>
    <th><a href="files/13_3.svg"><img src="files/13_3.png" /></a>
  </tr>
  <tr>
    <th>Si, Silicone</th>
    <th><a href="files/14.svg"><img src="files/14.png" /></a>
  </tr>
  <tr>
    <th>S, Sulfur</th>
    <th><a href="files/16.svg"><img src="files/16.png" /></a>
  </tr>
  <tr>
    <th>Cl, Chlorine</th>
    <th><a href="files/17.svg"><img src="files/17.png" /></a>
  </tr>
  <tr>
    <th>Ar, Argon</th>
    <th><a href="files/18.svg"><img src="files/18.png" /></a>
    <th><a href="files/18_2.svg"><img src="files/18_2.png" /></a>
    <th><a href="files/18_3.svg"><img src="files/18_3.png" /></a>
  </tr>
  <tr>
    <th>K, Potassium</th>
    <th><a href="files/19.svg"><img src="files/19.png" /></a>
  </tr>
  <tr>
    <th>Ca, Calcium</th>
    <th><a href="files/20.svg"><img src="files/20.png" /></a>
  </tr>
  <tr>
    <th>Sc, Scandium</th>
    <th><a href="files/21.svg"><img src="files/21.png" /></a>
  </tr>
  <tr>
    <th>Cr, Chromium</th>
    <th><a href="files/24.svg"><img src="files/24.png" /></a>
  </tr>
  <tr>
    <th>Fe, Iron</th>
    <th><a href="files/26.svg"><img src="files/26.png" /></a>
    <th><a href="files/26_2.svg"><img src="files/26_2.png" /></a>
    <th><a href="files/26_3.svg"><img src="files/26_3.png" /></a>
  </tr>
  <tr>
    <th>Co, Cobalt</th>
    <th><a href="files/27.svg"><img src="files/27.png" /></a>
    <th><a href="files/27_2.svg"><img src="files/27_2.png" /></a>
    <th><a href="files/27_3.svg"><img src="files/27_3.png" /></a>
  </tr>
  <tr>
    <th>Ni, Nickel</th>
    <th><a href="files/28.svg"><img src="files/28.png" /></a>
  </tr>
  <tr>
    <th>Cu, Copper</th>
    <th><a href="files/29.svg"><img src="files/29.png" /></a>
  </tr>
  <tr>
    <th>Zn, Zinc</th>
    <th><a href="files/30.svg"><img src="files/30.png" /></a>
  </tr>
  <tr>
    <th>Ga, Gallium</th>
    <th><a href="files/31.svg"><img src="files/31.png" /></a>
  </tr>
  <tr>
    <th>Ge, Germanium</th>
    <th><a href="files/32.svg"><img src="files/32.png" /></a>
  </tr>
  <tr>
    <th>As, Arsen</th>
    <th><a href="files/33.svg"><img src="files/33.png" /></a>
    <th><a href="files/33_1.svg"><img src="files/33_1.png" /></a>
    <th><a href="files/33_3.svg"><img src="files/33_3.png" /></a>
  </tr>
  <tr>
    <th>Se, Selenium</th>
    <th><a href="files/34.svg"><img src="files/34.png" /></a>
  </tr>
  <tr>
    <th>Br, Bromium</th>
    <th><a href="files/35.svg"><img src="files/35.png" /></a>
  </tr>
  <tr>
    <th>Kr, Krypton</th>
    <th><a href="files/36.svg"><img src="files/36.png" /></a>
  </tr>
  <tr>
    <th>Rb, Rubidium</th>
    <th><a href="files/37.svg"><img src="files/37.png" /></a>
  </tr>
  <tr>
    <th>Sr, Strontium</th>
    <th><a href="files/38.svg"><img src="files/38.png" /></a>
  </tr>
  <tr>
    <th>Y, Yttrium</th>
    <th><a href="files/39.svg"><img src="files/39.png" /></a>
    <th><a href="files/39_2.svg"><img src="files/39_2.png" /></a>
  </tr>
  <tr>
    <th>Tc, Tecneticum</th>
    <th><a href="files/43.svg"><img src="files/43.png" /></a>
  </tr>
  <tr>
    <th>Ag, Silver</th>
    <th><a href="files/47.svg"><img src="files/47.png" /></a>
    <th><a href="files/47_2.svg"><img src="files/47_2.png" /></a>
    <th><a href="files/47_3.svg"><img src="files/47_3.png" /></a>
    <th><a href="files/47_4.svg"><img src="files/47_4.png" /></a>
  </tr>
  <tr>
    <th>In, Indium</th>
    <th><a href="files/49.svg"><img src="files/49.png" /></a>
  </tr>
  <tr>
    <th>Sn, Tin</th>
    <th><a href="files/50.svg"><img src="files/50.png" /></a>
  </tr>
  <tr>
    <th>Sb, Antimony</th>
    <th><a href="files/51.svg"><img src="files/51.png" /></a>
  </tr>
  <tr>
    <th>Te, Tellur</th>
    <th><a href="files/52.svg"><img src="files/52.png" /></a>
  </tr>
  <tr>
    <th>I, Iod</th>
    <th><a href="files/53.svg"><img src="files/53.png" /></a>
  </tr>
  <tr>
    <th>Xe, Xenon</th>
    <th><a href="files/54.svg"><img src="files/54.png" /></a>
  </tr>
  <tr>
    <th>Cs, Caesium</th>
    <th><a href="files/55.svg"><img src="files/55.png" /></a>
  </tr>
  <tr>
    <th>Ba, Barium</th>
    <th><a href="files/56.svg"><img src="files/56.png" /></a>
  </tr>
  <tr>
    <th>La, Lanthan</th>
    <th><a href="files/57.svg"><img src="files/57.png" /></a>
  </tr>
  <tr>
    <th>Ce, Cer</th>
    <th><a href="files/58.svg"><img src="files/58.png" /></a>
  </tr>
  <tr>
    <th>Nd, Neodym</th>
    <th><a href="files/60.svg"><img src="files/60.png" /></a>
  </tr>
  <tr>
    <th>Pm, Praesodymium</th>
    <th><a href="files/61.svg"><img src="files/61.png" /></a>
  </tr>
  <tr>
    <th>Sm, Samarium</th>
    <th><a href="files/62.svg"><img src="files/62.png" /></a>
  </tr>
  <tr>
    <th>Hf, Hafnium</th>
    <th><a href="files/72.svg"><img src="files/72.png" /></a>
  </tr>
  <tr>
    <th>W, Tungsten</th>
    <th><a href="files/74.svg"><img src="files/74.png" /></a>
    <th><a href="files/74_2.svg"><img src="files/74_2.png" /></a>
  </tr>
  <tr>
    <th>Au, Gold</th>
    <th><a href="files/79.svg"><img src="files/79.png" /></a>
    <th><a href="files/79_1.svg"><img src="files/79_1.png" /></a>
    <th><a href="files/79_2.svg"><img src="files/79_2.png" /></a>
  </tr>
  <tr>
    <th>Hg, Mercury (Quicksilver)</th>
    <th><a href="files/80.svg"><img src="files/80.png" /></a>
  </tr>
  <tr>
    <th>Tl, Thallium</th>
    <th><a href="files/81.svg"><img src="files/81.png" /></a>
    <th><a href="files/81_1.svg"><img src="files/81_1.png" /></a>
  </tr>
  <tr>
    <th>Pb, Lead</th>
    <th><a href="files/82.svg"><img src="files/82.png" /></a>
  </tr>
  <tr>
    <th>Bi, Bismuth</th>
    <th><a href="files/83.svg"><img src="files/83.png" /></a>
    <th><a href="files/83_2.svg"><img src="files/83_2.png" /></a>
  </tr>
  <tr>
    <th>Po, Polonium</th>
    <th><a href="files/84.svg"><img src="files/84.png" /></a>
  </tr>
  <tr>
    <th>At, Astat</th>
    <th><a href="files/85.svg"><img src="files/85.png" /></a>
  </tr>
  <tr>
    <th>Rn, </th>
    <th><a href="files/86.svg"><img src="files/86.png" /></a>
  </tr>
  <tr>
    <th>Fr, Fracium </th>
    <th><a href="files/87.svg"><img src="files/87.png" /></a>
  </tr>
  <tr>
    <th>Ra, Radon</th>
    <th><a href="files/88.svg"><img src="files/88.png" /></a>
  </tr>
  <tr>
    <th>All radioactive elements (Po, At...)</th>
    <th><a href="files/88.svg"><img src="files/88.png" /></a>
    <th><a href="files/89.svg"><img src="files/89.png" /></a>
    <th><a href="files/90.svg"><img src="files/90.png" /></a>
    <th><a href="files/91.svg"><img src="files/91.png" /></a>
  </tr>
</table>

<p>Author: Carsten Niehaus<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include("footer.inc");
?>
