<?php
  $page_title = "How to obtain Kalzium";
  $site_root = "../";
  

  include( "header.inc" );
?>
<p>
Blinken should be included with your distribution in the kdeedu package.
<br />
Latest stable version (2.2.x) is in SVN branches/4.2/KDE, in the kdeedu module and was shipped with KDE 4.2.
</p>

<br />
<div id="quicklinks"> 
	[ 
	<a href="#binpacks">Binary Packages</a> 
	|
	<a href="#tarballs">Source Tarballs</a> 
	|
	<a href="#svn">SVN</a> 
	]
</div>
<br />

<?php
  show_obtain_instructions( "Kalzium", "kdeedu", true );
?>

<hr width="30%" align="center" />
<p>Author: Carsten Niehaus<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include("footer.inc");
?>
