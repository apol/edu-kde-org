<?php
  $page_title = "Spread the word!";
  $site_root = "../../";
  

  include( "header.inc" );
?>
<p>Get the <a href="EDUflyer.pdf">PDF!</a><br />
SCRIBUS source: <a href="edu-flyer-sla.tar.gz">edu-flyer-sla.tar.gz</a></p>
<p/>Thanks to <b>Melissa Steinhagen</b> for making the flyer more professional!<br />
Picture: <b>Nuno Pinheiro</b></p>
<img src="EDUflyer-page1.png"
width="595" height="842">


<hr width="30%" align="center" />
<p>
Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>


