<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );

  $site_root = "../";
  $page_title = i18n_noop('Graphics');
  
  include ( "header.inc" );
?>

<!--TODO: define appropriate! -->
<p><?php i18n( "Here you can find our graphics, feel free to use them, when appropriate.") ?></p>
<p><img src="kdeedu-white.png" /></p>
<p><img src="kdeedu-blue.png" /></p>
<p><?php i18n( "You'll find more detailed artwork <a href='KDEEdu_artwork.svg'>here</a>" )?></p>

<?php
  include "footer.inc";
?>
