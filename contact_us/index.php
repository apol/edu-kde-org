<?php
  $translation_file = "edu-kde-org";
  include_once( "functions.inc" );
  $page_title = i18n_noop('Contact Us');
  include "header.inc";
?>

<h3><?php i18n("Mailing Lists"); ?></h3>

<p><?php i18n('There is one mailing list concerning the KDE-Edu Project. 
In this list people discuss Free Educational Software issues more or less related to the KDE-Edu project. 
To subscribe to the list, go <a href="http://mail.kde.org/mailman/listinfo/kde-edu">here</a>. 
You can also browse through the <a href="http://mail.kde.org/pipermail/kde-edu/">online archive</a> of this list.'); ?></p>


<h3><?php i18n('Forums'); ?></h3>

<p><?php i18n('Educational topics are discussed on <a href="http://forum.kde.org">forum.kde.org</a> in the forum
<a href="http://forum.kde.org/viewforum.php?f=21">Games &amp; Education</a>.'); ?></p>


<h3><?php i18n('IRC'); ?></h3>

<p><?php i18n('Join us on irc.freenode.net, channel <strong><a href="irc://irc.freenode.org/#kde-edu">#kde-edu</a>.</strong>.'); ?></p>


<h2><?php i18n('Press'); ?></h2>

<p><?php i18n('Information about promotion <a href="press.php">here</a>.'); ?></p>


<h2><?php i18n('Graphics'); ?></h2>

<p><?php i18n('How to use the logo and icons? Get answers <a href="graphics.php">here</a>.'); ?></p>

<?php
  include "footer.inc";
?>
