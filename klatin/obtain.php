<?php
  $page_title = "How to obtain KLatin";
  $site_root = "../";
  

  include( "header.inc" );
?>
<p>
You can get KLatin for KDE 3.x from your distribution.
</p>
<p>
Since KDE 4.0, KLatin is not anymore part of the kdeedu module. If you are interested in working on this program for KDE 4.1 you can get the KDE 3 source from <a href="http://websvn.kde.org/tags/unmaintained/4/klatin/">http://websvn.kde.org/tags/unmaintained/4/klatin/</a>
</p>


<br />
<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
