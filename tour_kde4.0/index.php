<?php
  $site_root = "../";
  $page_title = 'The KDE Education Project Tour';

  include ( "header.inc" );
?>

<p align="center"><img src="http://games.kde.org/new/counter/?day=0.5" alt="KDE
4.0 Release Counter" width="377" height="47" /> </p>

<h2>KDE Education project tour</h2>

<div id="quicklinks">
[
  <a href="#languages">Languages</a> |
  <a href="#mathematics">Mathematics</a> |
  <a href="#miscellaneous">Miscellaneous</a> |
  <a href="#science">Science</a>
]
</div>

<h2><a name="languages">Languages</a></h2>
  <p><b><a href="../klettres">KLettres</a></b> is a tool that helps
associate spoken letters and syllables with their written representation. It's
suited for young or for adults who want to learn their letters or a foreign
language. The program offers a simple game with different grade of difficulty,
user have to insert letters inside the input box and if it's correct the program
will assign a new word, otherwise it will repeat the word. There are also
various theme for the interface like Kid, Desert and Savannah and various levels
of difficulty for training. You can also dowloand new alphabets from the
internet and learn new words.There are twenty 
languages available at the moment: Arabic, Czech, Brazilian Portuguese, British
English, Danish, Dutch, English, French, German, Hebrew, Hungarian, Italian, 
Kannada, Low Saxon, Luganda, Malayalam, Romanized Hindi, Spanish, Slovak and
Telugu.</p>
  <img src="KLettres.png" width="804" height="628" alt="KLettres" />
  <hr /> 
  <p><b><a href="../kanagram">Kanagram</a></b> is a game consisting of solving
anagrams of words. You can choose the difficulty and ask for hints or solution.
You can choose from a large dictionary of words, download or create new
dictionaries and add new words.</p>
  <img src="KAnagram.png" width="653" height="497" alt="KAnagram" />
  <hr /> 
  <p><b><a href="../khangman">KHangMan</a></b> is the game of hangman proposed
on the KDE desktop. The goal of game is to find the hidden word by inputting new
characters. If the character is correct it will be placed in the word at the
proper position(s), if it isn't correct it will build the hanging of the
man. It permits you to choose the words by category, language and gui theme
between desert or sea. You can download new words dictionary from the internet
directly from the application.</p>
  <img src="KHangMan.png" width="667" height="470" alt="KHangman" />
  <hr /> 
  <p><b><a href="../parley">Parley</a></b> is a vocabulary trainer aiming to
help people to learn new words or expressions from languages. You can create
your own collection of words or download new ones from the internet. Parley uses
the method of flash cards / quizzes to teach you new words and helps with hints.
After the session training you can see the mistakes you made, the correct words
you gave and the not answered words.</p>
  <img src="Parley.png" width="809" height="627" alt="Parley" />
  <hr /> 
  <p><b><a href="../kwordquiz">KWordQuiz</a></b> is a tool that gives you a
powerful way to create quizzes suitable for all situations. You can create
simply a vocabulary by typing the word in one of the two columns and its meaning in
the other one. You can also create flashcards, multiple choice quiz or question
and answer text based on the columns in editor.</p>
  <img src="KWordQuiz.png" width="799" height="531" alt="KWordQuiz" />
  <hr /> 
  <p><b><a href="../kiten">Kiten</a></b> is a Japanese language
reference tool. You can translate words from English to Japanese or from
Japanese to English. There is also a Kanji dictionary that helps you to look up
specific characters. With Kiten you can a do a different grade of searching like
Regular Searching, Radical Searching, Grade Searching or Stroke Searching. You
can also load your dictionary and starting using it directly from program.</p>
  <img src="Kiten.png" width="542" height="420" alt="Kiten" />
  <hr /> 
<h2><a name="mathematics">Mathematics</a></h2>
  <p><b><a href="../kbruch">KBruch</a></b> is a program to learn fractions and
integer operations. You can choose to learn gradually or to do exercises. You
can have a visual feedback of what fractions means through the program and you
can decide what kind of exercise you want to do by operations and number of
terms.</p> 
  <img src="KBruch.png" width="746" height="567" alt="KBruch" />
  <hr /> 
  <p><b><a href="../kig">Kig</a></b> is an educational instrument for geometry.
It allows you to create points, lines, segments, circles by point orb y radius,
ellipsis, hyperbolae, parabolae and other complex figures. You can also graph
functions like reflect, translate, searching for analogies or similitudy. The
worksheet is dynamic and permits to zoom in or out and select the area to be
shown.You can also define macros to speed up the work. The program itself also
has an browsable history, so you can see what you've done and undo it.</p>
  <img src="Kig.png" width="857" height="702" alt="Kig" />
  <hr /> 
  <p><b><a href="../kalgebra">KAlgebra</a></b> is a mathematical tool to
calculate and evaluate functions. It can evaluate functions or can display them
on a 2D or 3D graph. It also has a dictionary of built-in functions ready to
use. You can define new functions and create new library by saving the older
session. 2D graphs and 3D graphs have various options like zoom in graph or
rotation.</p>
  <img src="KAlgebra.png" width="904" height="524" alt="KAlgebra" />
  <hr />
  <p><b><a href="../kmplot">KmPlot</a></b> is a program that helps one to
visualize the function on cartesian graphs. It plots the function on the graphs
between a maximum or a minimum limit, the derivates of function, up to the
second one, and its integral, between an interval. There are also some tools
like the calculator, which helps to solve formulas, the plot area, which permits
to plot the area of a graph, and the finding of maximum and minimum in the
function. You can colorize the graphs, zoom in or out the plane and export it as
image to an external file.</p>
  <img src="KmPlot.png" width="926" height="636" alt="KmPlot" />
  <hr /> 
<h2><a name="miscellaneous">Miscellaneous</a></h2>
  <p><b><a href="../kgeography">KGeography</a></b> allows you to open world maps
and explore them. You can see the regions which form them and also you can
obtain information about the capitals and flags. In the sidebar there are
various buttons that offer test about various subject like capitals or to place
the right region in proper space.</p>
  <img src="KGeography.png" width="816" height="583" alt="KGeography" />
  <hr /> 
  <p><b><a href="../kturtle">KTurtle</a></b> is an education programming
environment that aims to make learning how to program as easy as possible. The
program shows a simple programming workbench where kids can write programs to
control a turtle inside on a canvas. The programming language used is
TurtleScript which is a simple programming language but not powerless. You can
control the flow of program selecting the speed of execution and running,
stopping or aborting it. The program includes also an inspector to inspect the
variables and the functions of programs and also a tree to see a tree view of
program. The program is equipped with ready-to-use examples and the ability to
save program to an external file.</p>
  <img src="KTurtle.png" width="854" height="494" alt="KTurtle" />
  <hr /> 
  <p><b><a href="../blinken">Blinken</a></b> is a game where the user has to
remember the correct sequence of colors as they blinked. There are different
levels of difficulty and user can choose among them. If user remembers exactly
the first sequence, he will pass to a new sequence where a new color is added.
When he fails to pass, the whole game restarts. The goal of game is to get the
highest score.</p>
  <img src="Blinken.png" width="608" height="516" alt="Blinken" />
  <hr /> 
  <p><b><a href="../ktouch">KTouch</a></b> is an educational program to learn to
type without looking at the keyboard. It offers a huge selection of lectures to
learn and a statistics of lecture about errors about typing, speed of typing and
characters you need to focus most on. It offers also a graphs of statistics
between session about words, characters, correctness and skills. While typing
you can see a virtual keyboard on the bottom showing the keys you are pressing
and the keys you should press. It is suitable for kids who want to learn how to
type correctly on the PC or to people who want to learn how to type quicker and
better.</p>
  <img src="KTouch.png" width="858" height="705" alt="KTouch" />
  <hr />
<h2><a name="science">Science</a></h2>
  <p><b><a href="../kalzium">Kalzium</a></b> is a powerful software that offers
a periodic table of elements and related instruments. It shows the electrons
disposition around the nucleus, the isotopes or the spectrum of elements among
with extra information. Additional instruments are offered to plot or calculate
data to solve chemical equations. It can show the behaviour of elements at some
conditions, like high or lower temperature, the state of matter or the pratical 
use of them.</p>
  <img src="KAlzium.png" width="857" height="592" alt="Kalzium" />
  <hr />  
  <p><b><a href="../marble">Marble</a></b> is a world atlas. You can explore the
Earth or the Moon, and by selecting a the city from the sidebar, information
about city, the type of globe projection, using gps for the current location and
find routing from one place to another one. You can download new Maps or Region
and export a map to a file, you can also use online services like wikipedia,
Photos, or Weather.</p>
  <img src="Marble.png" width="770" height="537" alt="Marble" />
  <hr /> 
  <p><b><a href="../kstars">KStars</a></b> is a desktop planetarium for KDE. It
offers an interactive sky planetarium with all stars, asteroids, planets,
constellation, milky way and deep sky objects. The scene is zoomable and you can
see a representation of object if you zoom in enough. You can set how the time
flow in the scene and trace the trajectory of celestial bodies. There are a lot
of facilities to take the measure you need like positioning to Zenith, North,
South, West or East, tracking of objects or setting up of a telescope on the
earth to see the visible things from the set position. You can also do
mathematical operation with built-in calculator for time or space, observation
about sky, Jupiter's moon, solar system, altitude vs time of objects. You can
create and download from internet new scripts to be executed automatically and
save your sky image to a file.</p>
  <img src="KStars.png" width="1013" height="624" alt="KStars" />
  <hr />
  <p><b><a href="../step">Step</a></b> is an interactive physical simulator for
KDE. It helps you to discover physical world and the laws inside it through a
simulator. You have a workspace where you can create new objects, then you
define their properties and press the simulate button to play the simulation and
see the evolution of your system. You can download, save and share examples on
the internet.</p>
  <img src="Step.png" width="862" height="705" alt="Step" />
  <hr />
  <p><b><a href="../rocs">Rocs</a></b> aims to be a Graph Theory IDE for helping
professors to show the results of a graph algorithm and also helping students to
do the algorithms. Rocs includes a script module to create new scripts in
QtScript among with the debugger and an editor. You can create new nodes in the
worksheet and edges between them, setting the color of them and their
values.</p>
  <img src="Rocs.png" width="869" height="561" alt="Rocs" />
  <hr />
  <p><b><a href="../cantor">Cantor</a></b> is a mathematical frontend to well
known Math applications: Sage, Maxima, R and KAlgebra. It uses those backends to
create a powerful workbench with extra instruments. It is integrated into the
KDE Platform and offers styling text and formulas that can be integrated in a
powerful and complete worksheet.</p>
  <img src="Cantor.png" width="975" height="759" alt="Cantor" />
<br />
<br />
<hr width="30%" align="center" />
<p>Authors: Diego Candido from 2010 GCI and Stephanie Whiting<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>

