<?php
/* The following function provides a common obtain/compile/install instruction section.
 *
 * Usage: 
 *   show_obtain_instructions( $appname, $modulename, $libkdeedu );
 *
 *   $appname: Name of your application, e.g. "KMyProgram"
 *   $modulename: Name of the module containing your sources, commonly "kdeedu" or "playground" (optional). "kdeedu" is the default.
 *   $libkdeedu: Set this to true if your application needs libkdeedu. false is the default.
 *
 * For usage with "quicklinks" the headings are named:
 *   For apps in kdeedu only:
 *     "binpacks" -> Binary Packages
 *     "tarballs" -> Source Tarballs
 *   For all apps:
 *     "svn" -> SVN
 * The headings are <h3> tags.
 *
 * (c) 2005 Matthias Messmer <matthias AT familie-messmer DOT de>
 */

function show_obtain_instructions( $appname, $modulename = "kdeedu", $libkdeedu = false )
{
	$appfolder = strtolower( $appname );
	if( $modulename == "playground" ) 
	{
		$libkdeedu = $FALSE;
		$modulefolder = "trunk/".$modulename."/edu";
		$targetfolder = "edu";
		$libkdeedu = FALSE;
		$svnname="Stable KDE 4.4 branch";
	}
	if( $modulename == "kdeedu" )
	{
		$modulefolder = "branches/KDE/4.4/".$modulename;
		$targetfolder = "kdeedu";
		echo "<h3><a name=\"binpacks\">Linux Binary Packages</a></h3>\n"
			."<p>".$appname." is shipped out by the distributors with the kdeedu package. "
			."Some like <a href=\"http://www.debian.org\">Debian</a> build separate packages for every application hosted by the KDE Edu Project.</p>\n";
		echo "<h3><a name=\"tarballs\">Source Tarballs</a></h3>\n"
			."<p>KDE itself offers a tarball containing the sources of the kdeedu module. It can be found on the <a href=\"http://www.kde.org/download/\">KDE download page</a>. "
			."Follow the instructions in the <code>INSTALL</code> file inside.</p>\n";
	}
	if( $modulename == "trunk" )
	{
		$modulefolder = "trunk/KDE/kdeedu";
		$targetfolder = "kdeedu";
		$svnname="Unstable KDE 4.5 trunk";
	}
?>

<h3><a name="svn">SVN</a></h3>
<p>You can fetch the source code from the SVN repository. This small tutorial will show you how to fetch, compile, and install the latest development version of <?php echo $appname; ?>. If you need other versions, please refer the official <a href="http://developer.kde.org/documentation/tutorials/subversion/">"Subversion with KDE" tutorial</a>.</p>

<h4>SVN Client Version</h4>

<p>Ensure that you have a SVN client version 1.1 or later installed on your machine.</p>

<h4>Fetching the Sources</h4>

<p>Follow these steps:</p>

<pre><?php
	echo "\$ svn co svn://anonsvn.kde.org/home/kde/".$modulefolder."\n";
	echo "\$ cd ".$targetfolder."\n";
?>
</pre>

<h4>Compiling and Installing</h4>

<p>To compile and install <?php echo $appname; ?> change into the <code><?php echo $targetfolder; ?></code> directory. Now follow these steps:</p>

<pre><?php
	echo "\$ mkdir build\n";
	echo "\$ cd build\n";
	echo "\$ cmake -DCMAKE_INSTALL_PREFIX=\$KDEDIRS -DCMAKE_BUILD_TYPE=debugfull ..\n";
	if ($libkdeedu == "true")
	{
	    echo "\$ cd libkdeedu\n";
	    echo "\$ make\n";
	    echo "\$ su -c \"make install\"\n";
	    echo "\$ cd ../\n";
	}
	echo "\$ cd ".$appfolder."\n";
	echo "\$ make\n";
	echo "\$ su -c \"make install\"\n";
?>
</pre>

<p>Last command will prompt you to enter the root password.</p>

<h3><a name="windows">On Windows</a></h3>
<p>You can install KDE on Windows by following the instructions here: <a href="http://windows.kde.org/download.php">http://windows.kde.org/download.php</a>.</p>

<?php
} // end of function show_obtain_instructions
?>