<?php

/**
 * Written by Chris Howells <howells@kde.org>
 * Heavilty based on Dirk Mueller's 404 handler page
 *
 * Improved by Matthias Meßmer <matthias@familie-messmer.de>
 * 
 * addDir( "/from", "/to" ) will replace "from" with "to" in the uri
 * new: addDir( "/from/*", "/to" ) will send all requests underneath /from/ to /to
 */



require_once("classes/class_handler404.inc");

class Handler404_Edu extends Handler404
{

	function execute()
	{
      // copy of the original code
		if (strstr($_SERVER['REQUEST_URI'], ".html") && !strstr($_SERVER['REQUEST_URI'], "..")
		&& (file_exists("../" . str_replace(".html", ".php", $_SERVER['REQUEST_URI']))
		|| file_exists("./" . str_replace(".html", ".php", $_SERVER['REQUEST_URI']))))
		{
			header("Location: " . str_replace(".html", ".php", $_SERVER['REQUEST_URI']));
			exit();
		}

		if (strstr($_SERVER['REQUEST_URI'], ".htm") && !strstr($_SERVER['REQUEST_URI'], "..")
		&& (file_exists("../" . str_replace(".htm", ".php", $_SERVER['REQUEST_URI']))
		|| file_exists("./" . str_replace(".htm", ".php", $_SERVER['REQUEST_URI']))))
		{
			header("Location: " . str_replace(".htm", ".php", $_SERVER['REQUEST_URI']), true, 301);
			exit();
		}

		if (strstr($_SERVER['REQUEST_URI'], ".phtml") && !strstr($_SERVER['REQUEST_URI'], "..")
                && (file_exists("../" . str_replace(".phtml", ".php", $_SERVER['REQUEST_URI']))
		|| file_exists("./" . str_replace(".phtml", ".php", $_SERVER['REQUEST_URI']))))
                {
                        header("Location: " . str_replace(".phtml", ".php", $_SERVER['REQUEST_URI']), true, 301);
                        exit();
                }

		$uri = $_SERVER['REQUEST_URI'];
		$server = $_SERVER['SERVER_NAME'];

		# remove trailing slash for mapping
      		if (strlen($uri) > 1 && $uri[strlen($uri)-1] == "/")
		{
			$uri = substr($uri, 0, strlen($uri)-1);
		}

		if ($this->mapper[$uri])
		{
			header("Location: " . $this->mapper[$uri], true, 301);
			exit();
		}
		// end of copy of the original code
		
		// last try, map whole dirs
      foreach ($this->dirmapper as $from => $to) {
         // do we have a wildcard at the end?
         if( substr( $from, -2 ) == "/*" ) {
            // remove the wildcard and the trailing slash
            $from = substr($from, 0, -2);
            // set fix redirect target
            $location = $to;
         }
         else {
            // replace directory substring
            $location = str_replace($from, $to, $_SERVER['REQUEST_URI']);
         }
         // does the requested uri match this directory?
         if ( (strncmp ($from, $_SERVER['REQUEST_URI'], strlen($from)) == 0)
          && ( (strlen($from) == strlen($_SERVER['REQUEST_URI']))
          || ($_SERVER['REQUEST_URI'][strlen($from)] == "/") ) ) {
            header("Location: " . $location, true, 301);
            exit();
         }
      }
	
		$this->showError();
	}

}

