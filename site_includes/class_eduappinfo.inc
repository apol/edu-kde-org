<?php
	class EduAppInfo extends AppInfo
	{
	function setIcon($icon, $width="", $height="")
	{
		$this->icon = $icon;
		// get omitted icon size
		if( $width == "" )
		{
			$imagesize = getimagesize( $this->icon );
			$width = $imagesize[0];
			$height = $imagesize[1];
		}
		$this->width = $width;
		$this->height = $height;
	}
		function showLicence()
		{
			print "<p>";
      	if (isset($this->license))
         {
      		print $this->license;
	      }
   	   else
      	{
         	print "License not defined, don't forget to use setLicense().";
	      }
	      print "</p>\n";
		}
		function showPeople()
		{
			print "<div>\n";
		if (count($this->authors))
		{
			print "<b>Original Author";
			if( count( $this->authors ) > 1 )
			{
				 print "s";
			}
			print ":</b>\n";
			print "<ul>\n";

			for ($i=0; $i < count( $this->authors ); $i++)
			{
				$this->authors[$i]->show();
			}
			print "</ul>\n";
		}

		if (count($this->maintainers))
		{
			print "<b>Maintainer";
			if( count( $this->maintainers ) > 1 )
			{
				 print "s";
			}
			print ":</b>\n";
			print "<ul>\n";

			for ($i=0; $i < count( $this->maintainers ); $i++)
			{
				$this->maintainers[$i]->show();
			}
			print "</ul>\n";
		}

		if (count($this->contributors))
		{
			print "<b>Contributor";
			if( count( $this->contributors ) > 1 )
			{
				print "s";
			}
			print ":</b>\n";
			print "<ul>\n";

			for ($i=0; $i < count( $this->contributors ); $i++)
			{
				$this->contributors[$i]->show();
			}
			print "</ul>\n";
		}

		if (count($this->thankstos))
		{
			print "<b>Thanks To:</b>\n";
			print "<ul>\n";

			for ($i=0; $i < count( $this->thankstos ); $i++)
			{
				$this->thankstos[$i]->show();
			}
			print "</ul>\n";
		}
		print "</div>\n";
		}
		}
?>