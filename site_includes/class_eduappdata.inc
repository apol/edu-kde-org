<?php

/**
 * Extends AppData[1] to return age and subject category name
 * [1] Written by Daniel Laidig <d.laidig@gmx.de>
 * Extension written by Matthias Meßmer <matthias@familie-messmer.de>
 *
 * Usage:
 * <?php
 *  TODO
 * ?>
 */

require_once( "functions.inc" );
require "classes/class_appdata.inc";

class EduAppData extends AppData {

  function age() {
     return $this->data['age'];
  }

  function subject() {
     return $this->data['subject'];
  }
  
  function hasDependencies() {
     return isset( $this->data['dependencies'] ) && ( $this->data['dependencies'] != false );
  }
  
  function dependenciesHtml() {
    if( $this->data['dependencies']=='libkdeedu' ) {
      return '<p>'.i18n_var( "You need to build and install libkdeedu before building %1. To get it type:", $this->name() ).'</p><p><code>git clone git://anongit.kde.org/libkdeedu</code></p>';
    }
    else {
      return '<p>'.$this->data['dependencies'].'</p>';
    }
  }
}