<?php

include_once("functions.inc");
include_once("class_eduappdata.inc");

function nameToUrl($s)
{
    return str_replace(' ', '', strtolower($s));
}

function printSidebar($app, $category, $backToOverview=false)
{
    $content = '';

    $content .= '<div id="infobox-return"><strong class="categories">'.i18n_var( "Categories" ).'</strong>';
    if ($backToOverview) {
        $content .= '<a href="/applications/'.nameToUrl($category).'/'.nameToUrl($app->name()).'">'.i18n_var( "Back to overview" ).'</a>';
    } else {
        $content .= '<a href="/applications/'.nameToUrl($app->age()).'"><img id="infobox-overviewicon" src="/images/icons/categories/education-'.nameToUrl($app->age()).'_22.png" alt="" />'.i18n_var( "Back to %1", i18n_var($app->age()) )."</a><br />\n";
        $content .= '<a href="/applications/'.nameToUrl($app->subject()).'"><img id="infobox-overviewicon" src="/images/icons/categories/education-'.nameToUrl($app->subject()).'_22.png" alt="" />'.i18n_var( "Back to %1", i18n_var($app->subject()) ).'</a>';
    }
    $content .= '</div>';

    if ($app->hasHomepage() || $app->hasKDEApps()) {
        $content .= '<div class="infobox"><strong class="more">'.i18n_var( "More about %1", $app->name() ).'</strong>';
        if ($app->hasHomepage()) {
            $content .= '<p><a href="'.$app->homepage().'">'.i18n_var( "%1 Homepage", $app->name() ).'</a></p>';
        }
        if ($app->hasKDEApps()) {
            $url = htmlspecialchars("http://kde-apps.org/content/show.php?content=".$app->KDEAppsId());
            $content .= '<p><a href="'.$url.'">'.i18n_var( "%1 on KDE-Apps.org", $app->name() ).'</a></p>';
        }
        $content .= '</div>';
    }

    ///TODO: "Get $app" link?

    $content .= '<div class="infobox"><strong class="help">'.i18n_var( "Get help" ).'</strong>';
    if ($app->hasUserbase()) {
        $content .= '<p><a href="'.$app->userbase().'">'.i18n_var( "%1 on UserBase", $app->name() ).'</a></p>';
    }
    $content .= '<p><a href="'.$app->forumUrl().'">'.i18n_var( "KDE Community Forums" ).'</a></p>';
    if ($app->hasHandbook()) {
        $content .= '<p><a href="'.$app->handbook().'">'.i18n_var( "%1 Handbook", $app->name() ).'</a></p>';
    }
    $content .= '</div>';

    $content .= '<div class="infobox"><strong class="contact">'.i18n_var( "Contact the authors" ).'</strong>';

    //Bug tracker links
    if ($app->hasBugTracker()) {
        if ($app->isBugTrackerExternal()) {
            $content .= '<p><a href="'.htmlentities($app->bugzillaProduct()).'">'.i18n_var( "Report a bug" ).'</a></p>';
        } else { //KDE Bugzilla
            $componentstring = "";
            if ($app->bugzillaComponent()) {
                $componentstring = '&amp;component='.$app->bugzillaComponent();
            }
            $content .= '<p><a href="https://bugs.kde.org/enter_bug.cgi?format=guided&amp;product='.$app->bugzillaProduct().$componentstring.'">'.i18n_var( "Report a bug" ).'</a></p>';
        }
    } else { //Empty bugtracker, use default link
        $content .= '<p><a href="https://bugs.kde.org/wizard.cgi">'.i18n_var( "Report a bug" ).'</a></p>';
    }
    foreach ($app->ircChannels() as $channel) {
        $content .= '<p>IRC: <a href="irc://irc.freenode.org/'.$channel.'">'.i18n_var( "%1 on Freenode", $channel ).'</a></p>';
    }
    foreach ($app->mailingLists() as $ml) {
        //KDE mailing lists
        if (substr($ml, -8, 8) == "@kde.org") {
            $base = substr($ml, 0, -8);
            $content .= '<p>'.i18n_var( "Mailing List:" ).' <a href="mailto:'.htmlspecialchars($ml).'">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="https://mail.kde.org/mailman/listinfo/'.$base.'">'.i18n_var( "subscribe" ).'</a>, <a href="https://mail.kde.org/mailman/listinfo/'.$base.'/">'.i18n_var( "list information" ).'</a>)</p>';
        } else if (substr($ml, -22, 22) == "@lists.sourceforge.net") { //Sourceforge.net
            $base = substr($ml, 0, -22);
            $content .= '<p>'.i18n_var( "Mailing List:" ).' <a href="mailto:'.htmlspecialchars($ml).'">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="https://lists.sourceforge.net/lists/listinfo/'.$base.'">'.i18n_var( "subscribe" ).'</a>, <a href="http://sourceforge.net/mailarchive/forum.php?forum_name='.$base.'">'.i18n_var( "archive" ).'</a>)</p>';
        } else if (substr($ml, 0, 31) == "http://groups.google.com/group/") { //Google Groups (web)
            $base = substr($ml, 31, strlen($ml)-31);
            $content .= '<p>'.i18n_var( "Mailing List:" ).' <a href="mailto:'.htmlspecialchars($base).'@googlegroups.com">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="http://groups.google.com/group/'.$base.'/subscribe?note=1">'.i18n_var( "subscribe" ).'</a>, <a href="http://groups.google.com/group/'.$base.'/topics">'.i18n_var( "archive" ).'</a>)</p>';
        } else if (substr($ml, -17, 17) == "@googlegroups.com") { //Google Groups (mail)
            $base = substr($ml, 0, -17);
            $content .= '<p>'.i18n_var( "Mailing List:" ).' <a href="mailto:'.htmlspecialchars($ml).'">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="http://groups.google.com/group/'.$base.'/subscribe?note=1">'.i18n_var( "subscribe" ).'</a>, <a href="http://groups.google.com/group/'.$base.'/topics">'.i18n_var( "archive" ).'</a>)</p>';
        } else { //Default mail
            $content .= '<p>'.i18n_var( "Mailing List:" ).' <a href="mailto:'.htmlspecialchars($ml).'">'.$app->name().'</a></p>';
        }
    }
    $content .= '</div>';

    if( !$backToOverview )
    {
        $content .= '<div class="infobox"><strong class="info"><a href="/applications/'.nameToUrl($category).'/'.nameToUrl($app->name()).'/development">'.i18n_var( "Development information" ).'</a></strong></div>';
    }
    return $content;
}

function printPage($app)
{
    print '<div class="main-content">';

    //Print screenshot or dummy "no screenshot available" image
    $screenshotUrl = '/images/screenshots/'.nameToUrl($app->name()).'.png';
    $screenshotResizedUrl = '/images/screenshots/resized/'.nameToUrl($app->name()).'.png';
    if ( file_exists( $_SERVER["DOCUMENT_ROOT"] . $screenshotUrl )) {
        print '<div class="app-screenshot"><a href="'.$screenshotUrl.'">
        <img src="'.$screenshotResizedUrl.'" alt="'.i18n_var( "Screenshot" ).'" />
        </a></div>'; ///TODO: image size
    } else {
        print '<div class="app-screenshot">
        <img src="/images/screenshots/no_screenshot_available.png" alt="'.i18n_var( "No screenshot available" ).'" />
        </div>'; ///TODO: image size
    }

    print $app->descriptionHtml();
    if ($app->hasFeatures()) {
        print "<h2 id='features'>".i18n_var( "Features" )."</h2>";
        print $app->featureHtml();
    }
    if ($app->hasVersions() || $app->hasAuthors() || $app->hasLicense()) {
        print '<p id="app-additional-info-paragraph" class="app-additional-info-paragraph-hidden"></p>';
        print '<div id="app-additional-info">';
        if ($app->hasAuthors()) {
            print '<h2>'.i18n_var( "Developed By" ).'</h2>';
            print $app->authorHtml();
        }
        if ($app->hasVersions()) {
            ///TODO: Versions not yet implemented
        }
        if ($app->hasLicense()) {
            print '<h2>'.i18n_var( "License" ).'</h2>';
            print $app->licenseHtml();
        }
        print '</div>';
    }

    //If JS available, hide the application details (authors) and show a toggle link
    ?>
    <script type="text/javascript">
    /* <![CDATA[ */
        $(document).ready(function(){
            $('#app-additional-info').toggle();
            $('#app-additional-info-paragraph').append('<a href="#" id="app-additional-info-toggle"><?php i18n("Show more information about "); print $app->name(); ?></a>');
            //Add toggle effect
            $('#app-additional-info-toggle').click(function(){
               if ($('#app-additional-info-toggle').data('show') && $('#app-additional-info-toggle').data('show') == true) {
                    $('#app-additional-info-toggle').data('show', false)
                    $('#app-additional-info-paragraph').removeClass('app-additional-info-paragraph-shown');
                    $('#app-additional-info-paragraph').addClass('app-additional-info-paragraph-hidden');
                    $('#app-additional-info-toggle').text('<?php i18n("Show more information about "); print $app->name(); ?>');
               } else {
                    $('#app-additional-info-toggle').data('show', true);
                    $('#app-additional-info-paragraph').removeClass('app-additional-info-paragraph-hidden');
                    $('#app-additional-info-paragraph').addClass('app-additional-info-paragraph-shown');
                    $('#app-additional-info-toggle').text('<?php i18n("Show less information"); ?>');
               }
               $('#app-additional-info').slideToggle('slow');
               return false;
            });
        });
    /* ]]> */
    </script>
    <?php
    print '<div style="clear:left;"></div>';
    print '</div>';
}

function printDevelopmentPage($app)
{
    print '<div class="main-content">';

    print '<h2>'.i18n_var( "Source code repository" ).'</h2>';

    print $app->browseSourcesHtml();

    print $app->checkoutSourcesHtml();

    if( $app->hasDependencies() ) {
        print $app->dependenciesHtml();
    }

    //Show bugzilla related links only for applications hosted at bugs.kde.org
    if ($app->hasBugTracker() && !$app->isBugTrackerExternal()) {
        print '<h2>'.i18n_var( "Search for open bugs" ).'</h2>';

        $product = $app->bugzillaProduct();

        $componentstring = "";
        if ($app->bugzillaComponent()) {
            $componentstring = '&component='.$app->bugzillaComponent();
        }

        $majorBugs  = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash';
        $minorBugs  = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=normal&bug_severity=minor';
        $wishes     = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist';
        $juniorJobs = 'https://bugs.kde.org/buglist.cgi?keywords=junior-jobs&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit';

        print '<ul>';
        print '<li><a href="'.htmlspecialchars($majorBugs).'">'.i18n_var( "Major Bug reports" ).'</a></li>';
        print '<li><a href="'.htmlspecialchars($minorBugs).'">'.i18n_var( "Minor Bug reports" ).'</a></li>';
        print '<li><a href="'.htmlspecialchars($wishes).'">'.i18n_var(" Wish reports" ).'</a></li>';
        print '<li><a href="'.htmlspecialchars($juniorJobs).'">'.i18n_var( "Junior Jobs" ).'</a></li>';
        print '</ul>';
    }

    if ($app->hasEbn()) {
        print '<h2>'.i18n_var( "Code checking" ).'</h2>';
        print '<p>'.i18n_var( "Show results of automated code checking on the English Breakfast Network (EBN)." ).'</p>';
        print '<ul>';
        print '<li><a href="'.htmlspecialchars($app->ebnCodeCheckingUrl()).'">'.i18n_var( "Code Checking" ).'</a></li>';
        print '<li><a href="'.htmlspecialchars($app->ebnDocCheckingUrl()).'">'.i18n_var( "Documentation Sanitation" ).'</a></li>';
        print '</ul>';
    }

    print '</div>';
}

?>
