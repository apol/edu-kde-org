<?php
/*
 * Image gallery extended from the common ImageGallery
 * table is spread over the whole width
 *
 * (c) 2005 Matthias Messmer <matthias AT familie-messmer DOT de>
 */

class EduGallery extends ImageGallery
{
        function addImage( $src_url, $dest_url, $width_pixels=0, $height_pixels=0, $alt_text="", $caption_text = "", $description_text = "")
        {
		if( !( $width_pixels || $height_pixels ) )
		{
                        $imagesize = getimagesize( $src_url );
                        $width_pixels = $imagesize[0];
                        $height_pixels = $imagesize[1];
		}
		if( !$alt_text ) $alt_text = $dest_url;
		if( !$caption_text ) $caption_text = $dest_url;
                $item = new Item($src_url, $dest_url, $width_pixels, $height_pixels, $alt_text, $caption_text, $description_text);
                array_push($this->items, $item);
        }


	function show()
	{
		if (count($this->items))
		{
			print "<table summary=\"$this->summary\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\">\n";
			$tr_open = false;

			for ($i=0; $i < count($this->items); $i++)
			{
				$this->items[$i]->show($tr_open);
			}

			if ($tr_open)
				print "</tr>\n";
			print "</table>\n";
		}
	}
}
?>
