<?php
require_once( "functions.inc" );

// collect blog posts to a simplepie instance:
require "rss.inc";

//
// display $length newest posts in an ul list
//
function show_news( $length = 0, $show_description = false ) {

  global $feedu; // from rss.inc
  
  if( !$show_description ) {

    // begin the list
    echo "<ul>\n";
    
    // here come the list items, only using the title
	  foreach($feedu->get_items(0, $length ) as $key => $item){
	  	$date = date('j / M / Y', strtotime($item->get_date())); 
	  $desc = substr($item->get_description(), 0, 40);
      echo "  <li><a href=\"".$item->get_permalink()."\"><span class='title'>".$item->get_title()."</span><span class='date'>".$date."</span> <span class='desc'>".$desc."...</span></a></li>\n";
    }
    
    // list finished    
    echo "</ul>\n";
    
  } else {
  
  	$dateFormat = '%sM%s%sj%s,%sY%s';
  	$span1 = implode('\\', str_split('<span class="month">'));
  	$span2 = implode('\\', str_split('<span class="day">'));
  	$span3 = implode('\\', str_split('<span class="year">'));
  	$spanClose = implode('\\', str_split('</span>'));
    $dateFormat = sprintf($dateFormat, $span1, $spanClose, $span2, $spanClose, $span3, $spanClose);
  	
    // here come the posts
	  foreach($feedu->get_items(0, $length ) as $key => $item){
	  	
	    echo "<div class=\"edu-news\">\n";
      echo "  <div class=\"edu-news-title\"><a href=\"".$item->get_permalink()."\">".$item->get_title()."</a></div>\n";
      echo "  <div class=\"edu-news-date\"><span class='month'>" . $item->get_date('M') . "</span><span class='day'>" . $item->get_date('j') . "</span><span class='year'>"  . $item->get_date('Y') . "</span></div>\n";
      echo "  <div class=\"edu-news-description\">" . $item->get_description() . "</div>\n";
      echo "  <div class=\"edu-news-readmore\"><a href=\"".$item->get_permalink()."\">".i18n_var( "Read more on" )." ".$item->get_feed()->get_title() . " &raquo;</a></div>\n";
      echo "</div>\n";
         
      
    }
  }
}


?>