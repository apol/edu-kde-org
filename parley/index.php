<?php

header("Location: http://edu.kde.org/applications/all/parley");

  $site_root = "../";
  $page_title = 'Parley';

  include ( "header.inc" );
  $appinfo = new AppInfo( "Parley" );
  $appinfo->setIcon( "pics/ox256-app-parley.png", "256", "192" );
  $appinfo->setVersion( "0.9.1" );
  $appinfo->setCopyright( "2000", "The Parley Developers" );
  $appinfo->setLicense("gpl");

//  $appinfo->addAuthor("Frederik Gladhorn", "frederik.gladhorn@kdemail.net",
//                      "Current maintainer");
//  $appinfo->addAuthor("Peter Hedlund", "peter AT peterandlinda DOT com",
//                      "Maintainer for KVocTrain 2002-2007" );
//  $appinfo->addAuthor("Ewald Arnold", "kvoctrain AT ewald-arnold DOT de", 
//                      "Original KVocTrain author." );

//  $appinfo->addContributor("Jeremy Whiting", "jeremy@scitools.com", 
//                           "Rewriting the kvtml library for KDE4");

  $appinfo->show();

?>
<center>

<!--
<?php
  $gal = new EduGallery( "Parley - Screenshot" );
  $gal->addImage( "screenshots/parley-promo-white_small.png", "screenshots/parley-promo-white.png", 256, 144, "[Screenshot]", "Practice Vocabulary", "Parley for KDE 4.0" );
  $gal->show();
?>
-->

<img src="screenshots/mainpage/parley_welcome_small.png" alt="Parley Screenshot" usemap="#map" />
<map name="map">
<area shape="poly" coords="7,57,312,74,312,225,7,242" href="screenshots/mainpage/parley_4_1_practice_goat.png" />
<area shape="poly" coords="210,25,439,7,439,224,312,213,312,74,211,68" href="screenshots/mainpage/parley_4_1_table.png" />
</map>

<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#features">Features</a> |
  <a href="#developerblogs">Developer Blogs</a> |
  <a href="#feedback">Feedback</a> |
  <a href="#news">News</a>
]
</div>

<!--<div id="pagelinks">
[
  <a href="./screenshots.php">Screenshots</a> |
  <a href="./obtain.php">Get Parley</a> |
  <a href="./getinvolved.php">Get Involved</a> |
  <a href="../contrib/kvtml2/kvtml2.php">Data Files</a> |
  <a href="./tips.php">Tips and Tricks</a> |
  <a href="./todo.php">TODO</a> |
  <a href="./authors.php">Authors</a>
]
</div>-->

</center>

<h3><a name="description">Description</a></h3>
 <p><b>Parley</b> is a program to help you memorize things.<br/>
	Parley supports many language specific features but can be used for other learning tasks just as well.
	It uses the 
    <a href="http://en.wikipedia.org/wiki/Spaced_repetition">spaced repetition</a> learning method, also known as flash cards.
 </p>

 <p>
  Creating new vocabulary collections with Parley is easy, 
  but of course it is even better if you can use some of our premade files. 
  Have a look at the new
   <a href="../contrib/kvtml2/kvtml2.php">KDE 4 contributed learning files</a> and <a href="../contrib/kvtml.php">KDE 3 vocabulary files page</a>.
 <p>
 </p>

<h3><a name="features">Features</a></h3>
<p>
<ul>
<li>Different training types
    <ul>

	<li>Mixed Letters (order the letters, anagram like) to get to know new words</li>
	<li>Multiple choice</li>
	<li>Written training - type the words (including clever correction mechanisms)</li>
	<li>Example sentences can be used to create 'fill in the gap' training</li>
	<li>Article training</li>
	<li>Comparison forms (adjectives and/or adverbs)</li>
	<li>Conjugations</li>
	<li>Synonym/Antonym/Paraphrase</li>
    </ul>
</li>
<li>Fast training setup with all options in one dialog</li>
<li>More than two languages (for example English, Chinese Traditional and Chinese Simplified)</li>
<li>Find words (also by word type) quickly</li>
<li>Easy lesson management</li>
<li>Premade vocabulary files ready to use</li>
<li>Share and download vocabulary using Get Hot New Stuff</li>
<li>Open XML file format (shared with KWordQuiz, Kanagram and KHangMan) that can be edited by hand and is easily usable with scripts</li>
</ul>
</p>

<h3><a name="developerblogs">Developer Blogs</a></h3>
<p><a href="http://www.kdedevelopers.org/blog/4326">Frederik Gladhorn</a></p>

<h3><a name="feedback">Feedback</a></h3>
 <p>
  For general questions and feedback you can send email to the KDE Education
  mailing list: <a href="mailto:kde-edu@kde.org">kde-edu@kde.org</a>
 </p>

 <p>
  <a href="mailto:parley-devel@kde.org">Send mail to the development list: Parley-devel@kde.org</a>
 </p>
 
 <p><a href="https://mail.kde.org/mailman/listinfo/parley-devel">Join the Parley development mailing list.</a>
 </p>

 
 <p>Or contact 
  <a href="mailto:gladhorn@kde.org">Frederik Gladhorn</a> directly.
 </p>
 
 <p>
  If you encounter bugs or would like to propose features, use the 
  <a href="https://bugs.kde.org/wizard.cgi">bug reporting wizard</a> at
  <a href="https://bugs.kde.org">bugs.kde.org</a>
 </p>

 <?php
  kde_general_news("./news.rdf", 10, true);
 ?>

<center>
<img src="http://games.kde.org/new/counter/" alt="KDE 4.0 Release Counter" width="377" height="47" />
</center>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

