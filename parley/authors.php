<?php
  $site_root = "../";
  $page_title = 'Parley';
  
  include ( "header.inc" );

  $appinfo = new AppInfo( "Parley" );
  $appinfo->setIcon( "../images/icons/parley_32.png", "32", "32" );
  $appinfo->setVersion( "0.9.0" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2000", "The Parley Developers" );

  $appinfo->addAuthor("Frederik Gladhorn", "gladhorn@kde.org", "Current maintainer");
  $appinfo->addAuthor("Daniel Laidig", "d.laidig AT gmx DOT de", "Fixes, fixes and lots of fixes; Welcome screen");
  $appinfo->addAuthor("Peter Hedlund", "peter.hedlund AT kdemail DOT net" , "Countless fixes, former maintainter, port to KDE 4" );
  $appinfo->addAuthor("Ewald Arnold", "kvoctrain AT ewald-arnold DOT de", "Original KVocTrain author." );

  $appinfo->addContributor("Jeremy Whiting", "jeremy@scitools.com", "Rewriting the kvtml library for KDE4");

    $appinfo->addContributor("Andrea Marconi", "",
                        "Initial Italian localization");

    $appinfo->addContributor("Hans Kottmann", "",
                        "Initial French localization");

    $appinfo->addContributor("Grzegorz Ilczuk", "",
                        "Initial Polish localization");

    $appinfo->addContributor("Eric Bischoff", "",
                        "Converting documentation to docbook format");

    $appinfo->addContributor("Waldo Bastian", "",
                        "Help with port to Qt3/KDE3");

    $appinfo->addContributor("Kevin Kramer", "",
                        "Tool to create lists with ISO639 codes");

    $appinfo->addContributor("Andreas Neuper", "",
                        "Converter script for vokabeln.de files.");

    $appinfo->addContributor("Dennis Haney", "",
                        "Patch to implement Leitner learning method");

    $appinfo->addContributor("Anne-Marie Mahfouf", "annma AT kde DOT org",
                        "Port to KConfig XT");

    $appinfo->addContributor("Markus Büchele", "",
                        "Bug reports and testing on the way to KDE4");

    $appinfo->addContributor("Ramona Knapp", "",
                        "Suggested Parley as new name");

    $appinfo->addContributor("Lee Olson", "",
                        "The shiny new Oxygen icons");

//    $appinfo->addContributor("KDE Team", "",
//                        "Many small enhancements");

  $appinfo->show();
?>

<h4>Thanks to...</h4>
<p>
When I was down, demotivated and want to throw it all away,
something happened to keep me going.
I got emails with suggestions and criticism or simply a thank you.
Reminders that people use Parley and want it to be as good as I want it to be.
Keep on filing bugs and sending emails!
Of course there is another type of email that is very great:
one that contains a new vocabulary file!
We need them,  I'm also quite impressed by the first files using
images, keep them comming!
Thank you... <br/>
Lee, who did not only create some icons, but keeps on improving them.
Markus who helped find many bugs and kept me going during the alpha stadium.
Ilja who sometimes had some hard words for my sometimes silly ideas,
but a sharp perception and numerous suggestions to improve
usability and the gui in general wich still provide some
challenges to implement.
I'm very grateful to Peter who kept the program alive despite having his own projects
and little time. And for passing it on to me.
All those contributing code, Johannes, Pavi and Pete for example, keep going!
Jeremy, who is always there and helps.
Special thanks goes out to the KDE-Edu team (keep rocking E-Team) for help with pretty much everything.
<br/>
Frederik
</p>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

