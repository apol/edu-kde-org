<?php
  $site_root = "../";
  $page_title = 'KEduca';
  
  include ( "header.inc" );
 

  $appinfo = new AppInfo( "KEduca" );
  $appinfo->setIcon( "../images/icons/keduca_32.png", "32", "32" );
  $appinfo->setVersion( "1.3" );
  $appinfo->setCopyright( "2001", "Javier Campos" );
  $appinfo->setLicense("gpl");
  $appinfo->addAuthor( "Javier Campos", "javi AT asyris DOT org" );
  $appinfo->addMaintainer("Matt Williams", "matt AT milliams DOT com", "Current Maintainer" );
  $appinfo->addContributor("Henrique Pinto", "henrique DOT pinto AT kdemail DOT net", "Contributor" );
  $appinfo->addContributor("Klas Kalass", "klas AT kde DOT org", "Previous maintainer" );
  $appinfo->addContributor("Nenad Grujicic", "mengele AT linuxo DOT org", "Crystal svg icons");
  $appinfo->show();
?>


<p>KEduca was abandoned for KDE 4.0 due to lack of maintainer. There was a new start of KEduca in playground/edu but it is abandoned again.
</p>   

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

