<?php
  $site_root = "../";
  $page_title = 'KVocTrain';
  
  include ( "header.inc" );

  $appinfo = new AppInfo( "KVocTrain" );
  $appinfo->setIcon( "../images/icons/kvoctrain_32.png", "32", "32" );
  $appinfo->setVersion( "0.8.3" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2000", "Ewald Arnold" );
  $appinfo->addAuthor( "Ewald Arnold", "kvoctrain AT ewald-arnold DOT de" );
  $appinfo->addContributor("Frederik Gladhorn", "frederik.gladhorn@gmx.de", "Current maintainer");
  $appinfo->addContributor("Peter Hedlund", "peter AT peterandlinda DOT com" , "Contributor" );
  $appinfo->show();
?>

<br />
<h3>Description</h3>
      <p><b>KVocTrain</b> is an advanced vocabulary trainer for KDE 3. It is replaced for KDE 4 by <a href="../parley/index.php">Parley</a> or <a href="../kwordquiz/index.php">KWordQuiz</a>.</p> 
    
      <p>Like most of the other vocabulary trainers it uses the "flash card" approach. If you prefer a more general flash card program, please try <a href="../kwordquiz/index.php">KWordQuiz</a></p>

      <p>You remember: write the original expression on the front side of the card and the translation on the back. Then look at the cards one after another. If you knew the translation, put it away. If you
      failed, put it back to try again.</p>

      <p>KVocTrain offers the possibility to enter additional properties for your words (e.g. type, usage label, conjugations) and also allows to use them for queries. </p>

      <p>
        There are <a href="../contrib/kvtml.php">vocabulary files</a> for KVocTrain
        which can also be used with <a href="../kwordquiz/index.php">KWordQuiz</a>.
      </p>

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

