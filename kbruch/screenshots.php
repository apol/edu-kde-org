<?php
  $page_title = "KBruch - Screenshots";
  $site_root = "../";
  

  include( "header.inc" );
?>
<p>
			<img src="pics/gui_main.png" alt="[exercise Fraction Task]" border="1"
				width="450" height="220" align="middle" />
                                	 </p>

			<p>Here you can see the exercise Fraction Task. The user has to enter
numerator and denominator. KBruch checks the result and gives a short note, if
the user entered the result unreduced.
			</p>
       <p>
			<img src="pics/compare.png" alt="[exercise Comparison]" border="1"
				width="450" height="220" align="middle" />
                                </p>
			<p>In this screenshot you can see the exercise Comparison. The user has
to select the correct comparison sign and KBruch will check the result.
			</p>
       <p>
			<img src="pics/convert.png" alt="[exercise Conversion]" border="1"
				width="450" height="220" align="middle" />
                                </p>
			<p>In this screenshot you can see the exercise Conversion. The user has
to convert a given number into a fraction. He has to enter the result reduced.
			</p>
       <p>
			<img src="pics/factorize.png" alt="[a task after checking]" border="1"
				width="450" height="227" align="middle" />
                                </p>
			<p>In this screenshot you can see the exercise Factorization. The user has
to enter all prime factors of the given number.
			</p>
      <br />
      <br />

<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>










