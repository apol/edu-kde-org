<table border="0" cellpadding="5" cellspacing="10">

<tr>
<th colspan="2" class="contentheader">Mathematics</th>
<th class="contentheader" style="text-align:center">Since KDE</th>
</tr>

<tr>
<td valign="top"><a href="../kalgebra/index.php"><img alt="KAlgebra" align="top" src="../images/icons/kalgebra_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kalgebra/index.php">KAlgebra</a> is 2D and 3D plotter and calculator.</td>
<td style="text-align:center">4.0</td>
</tr>

<tr>
<td valign="top"><a href="../kbruch/index.php"><img alt="KBruch" align="top" src="../images/icons/kbruch_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kbruch/index.php">KBruch</a> is a program for generating tasks with fractions.</td>
<td style="text-align:center">3.2</td>
</tr>

<tr>
<td valign="top"><a href="../kig/index.php"><img alt="Kig" align="top" src="../images/icons/kig_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kig/index.php">Kig</a> is a program for exploring geometric constructions.</td>
<td style="text-align:center">3.2</td>
</tr>

<tr>
<td valign="top"><a href="../kmplot/index.php"><img alt="KmPlot" align="top" src="../images/icons/kmplot_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kmplot/index.php">KmPlot</a> is a mathematical function plotter.</td>
<td style="text-align:center">3.1</td>
</tr>

<tr><td><br/></td><td><br/></td></tr>
<tr>
<th colspan="2" class="contentheader">New in KDE 4.4</th>
<th class="contentheader" style="text-align:center"> - </th>
</tr>

<tr>
<td valign="top"><a href="../cantor/index.php"><img alt="Cantor" align="top" src="../images/icons/cantor_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../cantor/index.php">Cantor</a> is a worksheet for mathematical 
software such as <a href="http://www.sagemath.org/">Sage</a>, <a href="http://maxima.sourceforge.net/">Maxima</a> and <a href="http://www.r-project.org/">R</a>.</td>
<td style="text-align:center"> 4.4 </td>
</tr>
<tr><td><br/></td><td><br/></td></tr>
<tr>
<th colspan="2" class="contentheader">Discontinued in KDE 4.2</th>
<th class="contentheader" style="text-align:center"> - </th>
</tr>

<tr>
<td valign="top"><a href="../kpercentage/index.php"><img alt="KPercentage" align="top" src="../images/icons/kpercentage_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kpercentage/index.php">KPercentage</a> is a small math application that will help pupils
to improve their skills in calculating percentagesand was merged with <a href="../kbruch/index.php">KBruch</a>.</td>
<td style="text-align:center"> - </td>
</tr>

</table>
