<?php
  $page_title = "KPlotWidget Screenshots";
  include "header.inc";

  $gallery = new EduGallery("KPlotWidget-Screenshot");
  $gallery->addImage("kplotwidget-01.png", "kplotwidget-01.png", 0, 0,  "Screenshot no. 1", "KPlotWidget@Kalzium", "plotting the mean mass of some chemical elements" );
  $gallery->show();

  include "footer.inc";
?>


