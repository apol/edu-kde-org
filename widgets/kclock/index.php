<?php
	$page_title = "KClock";
	
	include( "header.inc" );
	$appinfo = new AppInfo( "KClock" );
	$appinfo->setLicense("gpl");
	$appinfo->setCopyright( 2004, "Arnold Kraschinksi" );
	$appinfo->addAuthor( "Arnold Kraschinksi", "arnold.kra67 AT gmx DOT de" );
	$appinfo->addAuthor( "Michael Goetsche", "michael DOT goettsche AT kdemail DOT net" );
	$appinfo->show();
?>

<h3><a name="description">Description</a></h3>

<p>KClock is a KDE-Edu widget displaying the time. </p>
<?php
  $gallery = new EduGallery("KClock- Screenshot");
  $gallery->addImage("kclock.png", "kclock.png", 132, 177,  "[Screenshot]", "", "KClock");
  $gallery->show();
  ?>
<p>KClock can be found in the KDE svn repository in playground/edu/widgets. KClock is easy to use and can be seen in action in KVerbos also in playground/edu.
</p>
<br />
<br />

<hr width="30%" align="center" />
<p>.
Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
