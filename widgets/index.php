<?php
  $page_title = "Widgets";

  include( "header.inc" );
?>

<p>Some KDE-Widgets designed for our applications could be useful in other applications, too.</p>

<ul>
  <li><a href="kclock">KClock</a> - A clock.</li>
  <li><a href="kplotwidget">KPlotWidget</a> - A generic data plotting widget.</li>
</ul>

<?php
  include( "footer.inc" );
?>
