<?php
  $site_root = "../";
  $page_title = 'Developer Resources';
  
  include ( "header.inc" );
?>

<br />
  <div id="quicklinks">
[
  <a href="#svn">SVN</a> |
  <a href="#techbase">KDE TechBase</a> |
  <a href="#tips">Tips</a> |
  <a href="#status">Status</a> |
  <a href="#links">Links</a>
]
</div>

<h3><a name="svn">SVN</a></h3>
<ul>
  <li>For some instructions on the basics of SVN and how to use SVN for the KDE
     modules, please see this mini-<a href="svn.php">SVN HOWTO</a>.</li>
  <li>Creating and maintaining your webpage project on edu.kde.org: please read
     carefully this <a href="../stuff/readme.php">README</a> for precise
     instructions on how to do that.</li>
</ul>

<h3><a name="techbase">KDE TechBase</a></h3>
The <a href="http://techbase.kde.org">KDE TechBase</a> is the main source
for development articles, <a href="http://techbase.kde.org/Development/Tutorials">tutorials</a>, and development resources for both KDE 3 and KDE 4. So consider taking a look there first when looking for
any development resource.

  <h3><a name="tips">Development tips</a></h3>
  <ul>
  <li>
      Internationalization (a.k.a. i18n), optimize your code, sound, pictures...<br />
      Read these <a href="tips.php">smashing tips</a> and contribute with your own recipes!<br />
  </li>
  </ul>
   <h3><a name="status">Development status</a></h3>
      <ul>
        <li>
          <b>trunk</b><br />
          <i>svn co svn://anonsvn.kde.org/home/kde/trunk/KDE/kdeedu</i><br />
          Will lead to KDE 4.1. See the general KDE 4.1 release schedule plan at<br />
          <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Release_Schedule">http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Release_Schedule</a><br />
          <br />
<!--
          See our <b>own KDE 4.0 feature plan</b> <a href="features.php">here</a>.<br />
          <br />
-->
        </li>
        <li>
          <b>branch 4.0</b><br />
          <i>svn co svn://anonsvn.kde.org/home/kde/branches/KDE/4.0/kdeedu</i><br />
          Will lead to KDE 4.0.1.<br />
          Only bugfixes and backports, no new features.<br />
	  See the general KDE 4.0 release schedule plan at<br />
          <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.0_Release_Schedule">http://techbase.kde.org/index.php?title=Schedules/KDE4/4.0_Release_Schedule</a><br />
          <br />
        </li>

<!--
    <li><b>All the projects</b><br />
      You can find <a href="../software">here</a> the state of KDE-Edu,
      i.e. all the projects that exist with their maintainer and webpage.<br />
   </li>
-->
   </ul>

    <h3><a name="links">Useful links</a></h3>
      KDE standard style guide: <a href="http://developer.kde.org/documentation/standards/kde/style/basics/index.html">KDE User interface guidelines</a><br />
      <br />
      Website usability for children: <a href="http://www.useit.com/alertbox/20020414.html">http://www.useit.com/alertbox/20020414.html</a><br />
      <br />
   <br />
   KDE Library Documentation Policy: <a href="http://developer.kde.org/policies/documentationpolicy.php">How you should document your classes using Doxygen</a>
   <br />
   <br />
   C++ Pitfalls: <a href="http://developer.kde.org/~wheeler/cpp-pitfalls.html">C++ Stuff you must know</a>
   <br />
   <br />
   <a href="http://wiki.kde.org/tiki-index.php?page=Quality+Team+KDE+Edu">http://wiki.kde.org/tiki-index.php?page=Quality+Team+KDE+Edu</a>
   <br />
   <br />

<p>Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
