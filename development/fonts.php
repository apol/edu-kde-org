<?php
  $site_root = "../";
  $page_title = 'About Fonts';
  
  include ( "header.inc" );
?>

<h3>What are free fonts?</h3>
<p>You might want to use in your KDE program some fancy fonts that are not usually shipped in Linux distros. In order to do so, you will need GPL fonts: the license will usually be on a txt file and also mention of the license should be found in the Font Info dialog when the ttf file is edited with <a href="http://fontforge.sf.net/">FontForge</a>.</p>
<p>If you want to create free fonts and release them under the GPL, you can look at the following form the Free Sofware Foundation: <a href="http://www.fsf.org/licensing/licenses/gpl-faq.html#FontException">http://www.fsf.org/licensing/licenses/gpl-faq.html#FontException</a>.</p>

<h3>Where can I get some?</h3>

<h3>How to install them in the KDE-Edu application?</h3>
<i>Proposal</i>: we install the fonts like a png to $appdata/fonts through the Makefile.am<br />
and the program prompts a dialog "You need to install font foo, do you agree?" (might be added as a Setting in case the user does not want it at first run but after).<br />
if the user clicks yes, the program code calls kfmclient copy ...and the font installer pops up<br />
<br />
<hr width="30%" align="center" />
<p>Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
