<?php
  $site_root = "../";
  $page_title = 'Tips for Developers';
  
  include ( "header.inc" );
?>

<?php
  $faq = new FAQ();

  $faq->addSection( "Edu Software" );

  $faq->addQuestion( "How do I develop an educational Application?",
    "</p><ul>
     <li>Choose the age category you want to design software for</li>
     <li>Chooose the subject</li>
     <li>Subscribe to the <a href=\"http://mail.kde.org/mailman/listinfo/kde-edu\">mailing list</a></li>
     <li><a href=\"mailto:we&#0098;mas&#00116;&#101;&#x72;&#x40;edu&#x2e;k&#0100;e&#046;&#x6f;r&#x67;\">Ask for a subdirectory</a> to be
     created on the web server so that the project's web site can be created.</li>
     <li>Please ensure the software stays within the common look and feel of KDE Educational software</li>
     </ul><p>" );

  $faq->addQuestion( "How can I figure out wether my application name is a trademark?",
    "The name you choose for your application must not be a trademark. You can first
run a Google search on it.<br />
Then, to search for European trademarks, you can use:<br />
<a href=\"http://oami.eu.int/search/trademark/la/de_tm_search.cfm\">http://oami.eu.int/search/trademark/la/de_tm_search.cfm</a><br />
For German trademarks, you can use (only on Mo-Fr between 7:30am-6pm MET): <br />
<a href=\"https://dpinfo.dpma.de/\">https://dpinfo.dpma.de/</a>" );

  $faq->addQuestion( "What are prefered code formatting rules?",
    "Here are some suggested guidelines:</p>
<ul>
     <li>Use either spaces or tab to indent your code but be consistent by
using the same all along</li>
<li>Insert a space after a comma, after a
begin parenthesis and before an end parenthesis</li>
<li>{ and } should be
in the same column</li>
<li>Put the pointer * and reference &amp; signs adjacent
to the variable they belong to</li>
</ul>
<p>Eva pointed out a useful application called  <a
href=\"http://astyle.sourceforge.net\">astyle</a> that is a reindenter and reformatter of C++, C and Java source code." );

  $faq->addSection( "KDE/Qt Framework" );

  $faq->addQuestion( "How do I manage bug reporting for my KDE program?",
    "Each program must use the <a href=\"http://bugs.kde.org\">KDE bugs database</a> properly. When importing an app, please remember to:</p>\n"
    ."<ol>\n"
    ."  <li>Change the last parameter in the KAboutData constructor to \"submit@bugs.kde.org\" (rather than somebody's personal e-mail address).</li>\n"
    ."  <li>Add an entry for the app in bugs/Maintainers.xml (preferably with the same description as in the .desktop file).</li>\n"
    ."</ol><p>" );

  $faq->addQuestion( "What is the use of <code>#include \"myfile.moc\"</code>?",
    "Laurent Montel sent me this tip to reduce compiling time (this is very
important, even for small projects)  and this tip also allows the compilation on
multi-processors machines. <br />
In each .cpp file that generates a moc, add the following line (let's say that
the cpp file is named myfile.cpp):</p>
<blockquote><code>
#include \"myfile.moc\"
</code></blockquote>
<p>After including this line, do a</p>
<blockquote><code>
make clean<br />
touch Makefile.am
</code></blockquote>
<p>to regenerate the makefile in the directory." );

  //$faq->addQuestion( "How can I parse XML with the Qt's DOM classes?",
  // -broken link - // "Please see the following <a href=\"http://zez.org/article/articleprint/28/\">documentation</a>." );

  $faq->addQuestion( "How do I implement a Highscore table?",
    "Have a look at the KHighscore class (you can find it in
kdegames/libkdegames/khighscore.cpp and khighscore.h). This is well
documented." );

  $faq->addQuestion( "How do I use pictures?",
    "If you use KDevelop, you add your picture in the project then you right
click on it and you select Properties.</p>
<p>In Installation, check Install and in the lineedit, type:</p>
<blockquote><code>$(kde_datadir)/project_name</code></blockquote>
<p>This will install your picture in \$KDEDIR/share/apps/project_name. Then, in your code, you have to include the following header:</p>
<blockquote><code>kstandarddirs.h</code></blockquote>
<p>and the path to your picture is</p>
<blockquote><code>locate(\"data\",\"project_name/my_pic.png\")</code></blockquote><p>" );

  $faq->addSection( "Internationalization (i18n) Of Applications" );

  $faq->addQuestion( "What KDE reference do you suggest for i18n?",
  "Please read the <a href=\"http://techbase.kde.org/Development/Tutorials/Localization/i18n\">KDE i18n tutorial</a> on KDE TechBase, and the tutorial about <a href=\"http://techbase.kde.org/Development/Tutorials/Localization/i18n_Mistakes\">i18n mistakes</a>." );

  $faq->addQuestion( "How do I add text to pictures properly?",
    "<a href=\"mailto:kevin.krammer@gmx.at\">Kevin Krammer</a> has contributed some code "
    ."to replace strings in a picture by the use of a painter that draws a message on a background picture.<br/><br/>"
    ."<code>\n"
    ."QPixmap* winnerPic = new QPixmap(locate( \"appdata\", \"win.png\" ) ); <br />\n"
    ."QPainter painter;<br />\n"
    ."painter.begin(winnerPic);<br />\n"
    ."// set up font and stuff <br />\n"
    ."// ... <br />\n"
    ."painter.drawText(i18n(\"You win\"));<br />\n"
    ."painter.end();\n"
    ."</code>" );

  $faq->addQuestion( "Where shall I store language sensitive data?",
    "For the applications that uses several languages, it is suggested that you
put your sounds and data in <tt>l10n/&lt;lang&gt;/data/kdeedu/&lt;your_app_name&gt;</tt>.<br />\n"
    ."They will then install along with the l10n package.<br />\n"
    ."Have a language dialog in your app and if the user wants another language,
he should install the corresponding l10n package." );

  $faq->show();
?>

<hr width="30%" align="center" />
Author: Anne-Marie Mahfouf and Matthias Me&szlig;mer<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
<?php
  include "footer.inc";
?>
