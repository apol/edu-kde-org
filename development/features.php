<?php
  $site_root = "../";
  $page_title = 'KDE-Edu Features for 4.0';
  
  include ( "header.inc" );
?>
<h3>General requirements</h3>
<ul>
<li>Make sure that all applications have <b>WhatsThis</b> help. This is a <b>new</b> requirement since 3.3.
</li>
<li>Make sure that each app in trunk has its own <b>up-to-date doc</b> in kdeedu/docs</li>
<li>All sounds in <b>OGG</b> format</li>
<li>Take care of a proper <b>i18n</b> in all apps: that means strings in <tt>i18n()</tt> function and that the widgets resize if the string is longer or shorter. Please use correct layouts to ensure that. That also means that data files should be translated.</li>
<li>Try to add some <b>data</b> for the applications so that each application can work at least with a minimal set.</li>
<li>Use <b>KConfig XT</b> whenever possible</li>
<li>All comments and variables names in english, use <b>Doxygen</b> comments to document your classes</li>
</ul>

<h3>KHotNewStuff</h3>
The following programs use KHotNewStuff and thus have less data in KDE SVN.<br/>
In case of language data, the data for the other languages will be available using the corrispondent kde-i18n module or downloaded using the right menu item.
<ul>
<li>KHangMan: shipped with English only.</li>
<li>KLettres: shipped with English and French only.</li>
<li>KStars</li>
<li>KAnagram</li>
<li>Marble</li>
</ul>
Implement the upload action for KDE 4.1.

<h3>Possibles additions &amp; changes</h3>
<ul>
      <li>Put Kbruch and KPercentage under a same GUI?</li>
</ul>

<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
