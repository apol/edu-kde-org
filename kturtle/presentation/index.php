<?php
  $site_root = "../../";
  $page_title = 'Slideshow - Table of Contents';
  
  include ( "header.inc" );
?>
<p align="center"><a href="html/slide_1.html">Click here to start the Slideshow</a></p>
<p><b>Table of Contents</b></p>
<ol>
  <li><a href="html/slide_1.html">KTurtle </a></li>
  <li><a href="html/slide_2.html">KTurtle 2nd</a></li>
  <li><a href="html/slide_3.html">Topics </a></li>
  <li><a href="html/slide_4.html">First look at KTurtle </a></li>
  <li><a href="html/slide_5.html">The very basics</a></li>
  <li><a href="html/slide_6.html">Reasons</a></li>
  <li><a href="html/slide_7.html">Internals</a></li>
  <li><a href="html/slide_8.html">Developers</a></li>
  <li><a href="html/slide_9.html">Features</a></li>
  <li><a href="html/slide_10.html">Features 2nd</a></li>
  <li><a href="html/slide_11.html">TODO's</a></li>
  <li><a href="html/slide_12.html">Future</a></li>
</ol>
Created on February 22, 2004 by <i>Cies Breijs</i> with <a href="http://www.koffice.org/kpresenter">KPresenter</a>
<br />
<br />
Get the KPresenter source here: <a href="kturtlepresentation.kpr">kturtlepresentation.kpr</a>.
<br />
<br />
<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>