<?php
  $site_root = "../";
  $page_title = 'KLettres Screenshots';
  
  include ( "header.inc" );

  $gallery = new EduGallery("KLettres - Screenshots");
  $gallery->addImage("pics/klettres1_pm.png", "pics/klettres1.png", 160, 124,  "[Screenshot]", "", "Theme: Desert - French - Level 3 - Mode Grow-up.<br />
   This look has a menuBar. ");
  $gallery->addImage("pics/klettres2_pm.png", "pics/klettres2.png", 160, 97,  "[Screenshot]", "", "Installing the Telugu sounds using <br /> Get Alphabet in a New Language... menu action.");
  $gallery->startNewRow();
  $gallery->addImage("pics/klettres3_pm.png", "pics/klettres3.png", 160, 124,  "[Screenshot]", "", "Theme: Savannah - Telugu  - Level 3 -  Mode Grown-up.");
  $gallery->addImage("pics/klettres4_pm.png", "pics/klettres4.png", 160, 124,  "[Screenshot]", "", "Theme: Kid - English  - Level 3 -  Mode Kid.");
  $gallery->show();
  ?>

 <p>
 <b>Note</b>: These screenshots are from KLettres version 2.0 from KDE 4.0.
 </p>
 <br />
 <hr width="30%" align="center" />
 <p>
 Author: Anne-Marie Mahfouf<br />
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>



