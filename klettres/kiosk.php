<?php
  $site_root = "../";
  $page_title = 'KLettres - Setting Kiosk Mode';
  
  include ( "header.inc" );
  ?>
 <h3>How to set Kiosk mode in KLettres</h3>

<p>The <b>kiosk-framework</b> provides an easy way to disable certain features within
KDE to create a more controlled environment.</p>
<p>Please see <a href="http://websvn.kde.org/trunk/KDE/kdelibs/kdecore/doc/README.kiosk">
http://websvn.kde.org/trunk/KDE/kdelibs/kdecore/doc/README.kiosk</a> for more precision about 
KDE kiosk mode.</p>
<p>You can find all the actions KLettres support using the following dbus call:<br />
qdbus org.kde.klettres-id | grep actions | cut -d '/' -f 4,5<br />
where id is the KLettres pid.
</p>
<ul>
<li>
<b>Having KLettres only in Kid mode</b><br />
If you want to block KLettres in Kid mode only, suppressing Grown-up mode actions, you will
need to add in the klettresrc file in $KDEHOME:<br />
<br />
<tt>[General]<br />
MenuBarBool[$i]=false<br />
myMode[$i]=kid<br />
<br />
[KDE Action Restrictions][$i]<br />
action/menubar[$i]=false<br />
action/mode_grownup[$i]=false</tt><br />
</li>
<li><p>
<b>Blocking KLettres to Level X (1&lt;=X&lt;=4)</b><br />
If you want to block KLettres to one level, say Level X (1&lt;=X&lt;=4), you will
need to add in the klettresrc file in $KDEHOME:<br />
<br />
<tt>[KDE Action Restrictions][$i]<br />
action/levels[$i]=false<br /></tt>
and in [General] group, set<br />
<tt>myLevel=X</tt></p>
<br />
</li>
<li>
You can suppress the download option that allows to get new sounds in KLettres (in File -&gt;Get 
Alphabet in New language) by having the following:<br />
<tt>[KDE Action Restrictions][$i]<br />
action/downloadnewstuff[$i]=false</tt>
<br />
</li>
</ul>
<p>Of course you can combine all those options. For example having<br />
<br />
<tt>[KDE Action Restrictions][$i]<br />
action/downloadnewstuff[$i]=false<br />
action/levels[$i]=false<br />
action/menubar[$i]=false<br />
action/mode_grownup[$i]=false<br />
[General]<br />
Language=fr<br />
myLevel=4<br /></tt>
will set KLettres in kid mode and Level 4 with download being impossible.
</p>
<br />
<p>
 Author: Anne-Marie Mahfouf<br />
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>
