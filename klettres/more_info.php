<?php
  $site_root = "../";
  $page_title = 'About KLettres';
  
  include ( "header.inc" );
?>

<?php
  include("klettres.inc");
  $appinfo->show();
?>

<br />
<br />

<hr width="30%" align="center" />
<p>
Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php include("footer.inc"); ?>