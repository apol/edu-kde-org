<?php
    include_once("../site_includes/applicationpage.inc");
    include_once("../site_includes/class_edugallery.inc");

    $site_root = "../";
    $page_title = "KAlgebra";

    include("header.inc"); // generate page header

    $app = new EduAppData("kalgebra");
    $appinfo = new AppInfo( "KAlgebra" );
    $appinfo->setIcon("../images/icons/kalgebra_32.png", "32", "32");
    $appinfo->setVersion("0.7");
    $appinfo->setLicense("gpl");
    $appinfo->setCopyright("2006", "Aleix Pol Gonzalez");
    $appinfo->addAuthor("Aleix Pol Gonzalez", "aleixpol AT kde DOT org");
    $appinfo->show();
?>

<br />

<?php
    // for now only use the auto-generated information
    printPage($app);
?>

<?php
    include("footer.inc");
?>

