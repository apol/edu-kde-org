<?php
  $site_root = "../";
  $page_title = 'KStars Buzz';
  
  include ( "header.inc" );
?>

<h2>What are people saying about KStars?</h2>

<p>
We've gotten a lot of positive feedback about KStars, and I wanted to 
post some of the reviews and fan mail here.  On behalf of the KStars 
developers, thanks for the feedback, everyone.  A big motivation for 
developing Free Software is the satisfaction that comes from knowing that
lots of people are using and enjoying something you've created.
</p><p>
Of course, bug reports and constructive criticisms are just as welcome as
praise, so keep the comments of all kinds coming!
</p>

<hr />
<p>
"I just wanted to let you know that I finally got to use my Discovery 15" Truss scope with KStars for real last night.  It all worked flawlessly with the Argo Navis. It was far easier to create an Observing list and use KStars to guide me to the objects than it was to dial them into the Argo Navis. You guys have done a great job on KStars.  Thanks a bunch for awesome program! I can actually locate a object, with a 16mm eyepiece in the scope, by guiding my scope with KStars."<br />
--Douglas Phillipson 
</p>
<hr />

<p>
"the more I use it the more I'm impressed by it...it is a great little 
program."<br />
-- Neale Monks, in his 
<a href="http://www.applelust.com/reviews/archives/kstarsrev/">Kstars on OSX</a>
review.
</p>
<hr /><p>
"No need for a trip into hyperspace, when the KDE planetarium brings the stars
into your living room"<br />
-- Stefanie Teufel, in a 
<a href="http://www.linux-magazine.com/issue/25/KStars.pdf">review</a> in Linux
magazine.
</p>
<hr /><p>
"Amateur astronomers who know their way around a linux 
system...may want to check out KStars"<br />
-- in <a href="http://skyandtelescope.com">Sky &amp; Telescope</a> magazine's 
Software Showcase column for 2002 April.
</p>
<hr /><p>
"The graphics are amazing! You can even resize the skymap and control 
how quickly...time passes in the program." <br />
-- Linux DaveCentral, awarding KStars their 
<a href="http://linux.davecentral.com/articles/view/1138/">Best of Linux</a>
rating.
</p>
<hr /><p>
"Tried your kstars program tonight after seeing it mentioned in S&amp;T. Nice."<br />
-- Elwood C. Downey (creator of 
<a href="http://www.clearskyinstitute.com/xephem/">XEphem</a>)
</p>
<hr /><p>
"I am absolutely blown away by the sheer beauty of the 
interface. Thank you and all the other people who worked on it for your 
effort; the skies will, to touch the old cliche, never quite look the same 
again."<br />
--Scot Stevenson
</p>
<hr /><p>
"KStars is the ultimate astronomy tool for me! KStars is sophisticated, 
yet the highly intuitive graphical user interface is a joy to use."<br />
--optimus
</p>
<hr /><p>
"Kstars is an amazing software.  It's pretty accurate and a very good help 
for our amateur observations."<br />
-- Martin Heroux
</p>
<hr /><p>
"What an impressive piece of software. Absolutely the most professional Open 
Source software for the Desktop that I have seen. Really fantastic." <br />
-- Conrad Schuler
</p>
<hr /><p>
"Thank You, and your partners, for your exquisite piece of software"<br />
-- Svein Johansen
</p>
<hr /><p>
"I really like what I see in Kstars. It seems to be pretty stable for such an
early release. I'm looking forward to what develops." <br />
-- Len P.
</p>
<hr /><p>
"KStars is great! I have been at astronomy for about 4 years now and have used
quite a bit of software for astronomy and kstars has it all!" <br />
-- Astroman
</p>
<hr /><p>
"I just did something useful with kstars :-)
</p><p>
I created a snapshot of kstars showing the sky in the winter (orion, taurus
etc.), inverted it and printed it in a way that it would cover four Din 
A4-folies.  Using an overhead projector I projected them onto the ceiling in 
my bedroom.  By using night-glowing paint I got a nice 3.5x3.5 sqm - heaven over 
my bed which looks quite realistic - and it was done using kstars :-)  (painting 
all the approx. 1500 stars took me about 13-15 hours)" <br /> 
-- Tackat<br />
[<a href="ceiling.jpg">image of Tackat's celestial ceiling</a>]
</p>
<hr /><p>
"What can I say but VERY WELL DONE.  It's the first program I have seen in a
long time that got me really excited." <br />
-- Tim
</p>
<hr /><p>
"Kudos to you...a very cool program!" <br />
-- Ken
</p>
<hr /><p>
"I used it for the first time today and I must say that I think that it is
really great." <br />
-- William
</p>
<hr /><p>
"I really liked your program "kstars (ver 0.7)" well done!!!" <br />
-- I.M.
</p>

<hr />
<p>
Jason Harris<br />
<a href="mailto:kstars AT 30doradus DOT org">kstars AT 30doradus DOT org</a>
</p>

<?php
  include "footer.inc";
?>
