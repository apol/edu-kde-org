<?php

  $site_root = "../";
   $page_title = 'KStars:  New Features in KDE 3.4';
   
  include ( "header.inc" );
?>

<p>
The KDE 3.4 release cycle was once again pretty short, 
so the list of new KStars features is short and sweet
this round.  Still, we managed to get some pretty 
cool stuff added, so let's take a look.
</p>
<p>
(New-feature tours are also available for the 
<a href="kstars-3.3-features.php">3.3</a> and 
<a href="kstars-3.2-features.php">3.2</a> releases).
</p>

<h2>Celestial To-Do Lists</h2>
<table>
<tr>
<td valign="top">
<p>
Have a list of objects you'd like to observe tonight?
Just add them to the Observing List tool, and you'll 
have easy access to common actions such as Center &amp; 
Track, Point Telescope, Show Details, and Show Alt. vs. 
Time.  Objects are added to the list through the popup 
menu, or by clicking the object and pressing the "O" 
key.  The tool also provides easy access to the "observing 
log" for each object, so you can attach notes each one.  
</p>
<p>
In future releases, you'll be able to save Observing 
Lists to disk, and we'll provide a wizard for creating 
lists with logical rules (such as "include all 
bright galaxies in the constellation Virgo").
</p>
</td>
<td valign="top">
<a href="feature/ObservingList.png"><img border="0" alt="new observing list tool" src="feature/ObservingList_thumb.png" width="301" height="256"/></a>
</td>
</tr>
</table>
<br/>

<h2>Dance of the Planets</h2>
<table>
<tr>
<td valign="top">
<p>
We've made substantial improvements to the Solar System Viewer.
First of all, the orbital tracks are now the actual ellipses 
followed by each planet, rather than the 
<a href="feature/SolarSystem.png">circular approximations</a> we 
used to use.  Also, the display is no longer tied to the Sun, 
you can recenter it with the arrow keys or by dragging with the 
mouse.  You can even double-click on a planet to recenter the 
display on that body.
</p>
<p>
Best of all, the tool now has an independent clock, so you can 
easily see the configuration of the planets on any date.  You 
can also set the clock in motion, and watch the celestial dance 
of the planets as they travel around the Sun.
</p>
</td>
<td valign="top">
<a href="feature/SolarSystem-3.4.png"><img border="0" alt="improved solar system viewer" 
src="feature/SolarSystem-3.4_thumb.png" width="260" height="280"/></a>
</td>
</tr>
</table>

<h2>Other Improvements</h2>
<table>
<tr>
<td valign="top">
<a href="feature/dmsBox.png"><img border="0" alt="hint text in angle edit boxes" src="feature/dmsBox_thumb.png" width="250" height="180"/></a>
</td>
<td valign="top">
There are many places in KStars where you are required to enter 
an angle value, such as a coordinate.  The edit boxes used for this
now provide visual feedback that they expect an angle value by 
displaying hint text ("dd mm ss.s") in grey.  The hint disappears 
when the box gets input focus.  We also provide an extensive tooltip
for these widgets.
</td>
</tr>
</table>
<br/>

<table>
<tr>
<td valign="top">
<p>
We added a large number of DCOP functions for telescope control, so 
your observing sessions can now be fully scripted!  These functions 
have been incorporated into the Script Builder tool.  
</p>
<p>
In addition, the INDI protocol now supports Takahashi Temma telescopes, 
and device automation is now possible.
</p>
</td>
<td>
<a href="feature/ScriptBuilder-3.4.png"><img border="0" alt="new INDI DCOP functions" src="feature/ScriptBuilder-3.4_thumb.png" width="330" height="240"/></a>
</td>
</tr>
</table>
<br/>

<table width="80%">
<tr>
<td valign="top">
<a href="feature/PopupMenu-3.4.png"><img border="0" alt="constellation in the popup menu" src="feature/PopupMenu-3.4_thumb.png" width="229" height="105"/></a>
</td>
<td valign="bottom">
The constellation in which an object is found is now displayed in the 
popup menu (as highlighted in yellow at left), and also in the Details 
window.
</td>
</tr>
</table>
<br/>

<table>
<tr>
<td valign="top">
<p>
In addition to the "Eyepiece" and "Camera" modes for designing a 
Field-of-View indicator, there is now a "Radio Telescope" mode.
You specify the diameter of the dish and the wavelength observed, 
and it computes a FOV indicator with the correct beam size.
Also, we added a "semitransparent circle" shape for FOV 
indicators.
</p>
</td>
<td valign="top">
<a href="feature/FOVEdit-3.4.png"><img border="0" align="right" alt="radio astronomy tab in FOV editor" 
src="feature/FOVEdit-3.4_thumb.png" width="300" height="175"/></a>
</td>
</tr>
</table>

<p>
Last but not least, we added some "Object Action" keyboard 
shortcuts, which perform actions on the last-clicked object.  
These include "Show Details window" (D); "Popup menu" (P); 
"Add to Observing List" (O); "Toggle Label" (L); and 
"Toggle Trail" (T; solar system bodies only).
</p>

<hr />
<a href="http://edu.kde.org/kstars">Back to the KStars Homepage</a>


<?php
include "footer.inc";
?>
