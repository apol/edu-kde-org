<?php
  $site_root = "../";
  $page_title = 'KStars';
  
  include ( "header.inc" );
?>

<center>
<a href="kstars-welcome-large.png">
   <img alt="[Screenshot]" src="kstars-welcome.png" width="600" />
</a>
<br />
<br />
<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#download">Download</a> |
  <a href="#features">Features</a> |
  <a href="screenshots.php">Screenshots</a> |
  <a href="indi/">Devices</a> |
  <a href="#credits">Credits</a> |
  <a href="#bugs">Problems and Bugs</a> |
  <a href="#join">Join us!</a>
]
</div>
<br />
</center>

<h3><a name="description">Description</a></h3> <p><b>KStars</b> is free, open source, cross-platform Astronomy Software. It provides an accurate graphical simulation of the night sky, from any location on Earth, at any date and time. The display includes up to 100 million stars, 13,000 deep-sky objects,all 8 planets, the Sun and Moon, and thousands of comets, asteroids, supernovae, and satellites. For students and teachers, it supports adjustable simulation speeds in order to view phenomena that happen over long timescales, the KStars Astrocalculator to predict conjunctions, and many common astronomical calculations.</p>
<p>For the amateur astronomer, it provides an observation planner, a sky calendar tool, and an FOV editor to calculate field of view of equipment and display them. Find out interesting objects in the "What's up Tonight" tool, plot altitude vs. time graphs for any object, print high-quality sky charts, and gain access to lots of information and resources to help you explore the universe!
Included with KStars is Ekos astrophotography suite, a complete astrophotography solution that can control all INDI devices including numerous telescopes, CCDs, DSLRs, focusers, filters, and a lot more. Ekos supports highly accurate tracking using online and offline astrometry solver, autofocus and autoguiding capabilities, and capture of single or multiple images using the powerful built in sequence manager.</p>

<p>Have a look at the <a href="features.php">Feature</a> listing for more. </p> <hr />

<h3><a name="android">KStars Lite for Android</a></h3>
<p>Get <a href="https://play.google.com/store/apps/details?id=org.kde.kstars.lite&hl=en">KStars Lite</a> for Android on Google Play Store. KStars Lite supports many features of the desktop version optimized for tablet/phone use. It supports telescope and camera control, color schemes, and different projection systems. Just point it any where in the night sky and identify what objects and constellations you are looking at, plus a lot more!</p>

<h3><a name="download">KStars for Windows/OSX/Linux</a></h3>
<p>
KStars 3.5.1 is released on Jan. 9th, 2021 and can be installed on Microsoft Windows, Mac OS, and GNU/Linux.
<ul>
<li><b>Release Notes</b>: <a href="https://knro.blogspot.com/2020/11/kstars-v350-is-released.html">Overview of v3.5.0 Changes</a></li>
<li><b>Windows Download</b>: <a href="https://www.indilib.org/jdownloads/kstars/kstars-3.5.1.exe">KStars 3.5.1 Installer</a> for Windows 7, 8, &amp; 10.
<p>Use <a href="https://sourceforge.net/projects/quickhash/">Quick Hash GUI</a> to verify the integrity of the executable file. The following are MD5 and SHA256 hashes:</p>
<ul>
<li><a href="https://www.indilib.org/jdownloads/kstars/kstars-3.5.1.exe.md5">MD5</a></li>
<li><a href="https://www.indilib.org/jdownloads/kstars/kstars-3.5.1.exe.sha256">SHA256</a></li>
</ul>
</li>
<li><b>MacOS Download</b>: <a href="https://www.indilib.org/jdownloads/kstars/kstars-3.5.1.dmg">KStars 3.5.1 DMG Installer</a> is available for MacOS 10.13+. After mounting the DMG, please follow the instructions within the DMG.
<ul>
<li><a href="https://www.indilib.org/jdownloads/kstars/kstars-3.5.1.dmg.md5">MD5</a></li>
<li><a href="https://www.indilib.org/jdownloads/kstars/kstars-3.5.1.dmg.sha256">SHA256</a></li>
</ul>
</li>
<li><b>Snap Packages</b>: Install the <a href="https://snapcraft.io/kstars">latest and stable KStars</a> on many linux distributions using Snap.</li>
<li><b>Linux Download</b>: Install KStars from your Linux distribution software center. Under Ubuntu/Mint, you can install the latest KStars by typing the following commands in the console:</li>
<pre>
sudo apt-add-repository ppa:mutlaqja/ppa
sudo apt-get update
sudo apt-get install indi-full kstars-bleeding
</pre>
<li><b>Source Tarball</b>: <a href="https://download.kde.org/stable/kstars/kstars-3.5.1.tar.xz.mirrorlist">KStars 3.5.1 Source tarball mirror list</a> is availabe to build from source.</li>
<li>For other platforms, see our <a href='install.php'>Installation Page</a> for instructions.
</ul>
</p>

<h3><a name="features">Features</a></h3>
<p>
KStars is probably the most feature-rich free astronomy
software. KStars caters to a wide-variety of use cases. Whether you
are a student, an educator, an amateur astronomer or an astronomy
enthusiast, you will find tools in KStars that are useful to you.
</p>

<p>
Click <a href="features.php">here</a> to see a full listing of features, organized by use case.
</p>
<p>
<strong>Short summary of important features:</strong>
<ul>
<li>Graphical simulation of the sky with the planets, up to 100 million stars, 10000 deep-sky objects, comets and asteroids. Adjustable simulation rate.</li>
<li>Access to several internet resources for information, imagery and data</li>
<li>Complete astrophotography workflow</li>
<li>A host of tools that predict conjunctions, plot time variation of positions of planets, perform calculations etc. </li>
<li>Powerful observation planner to plan your observations.</li>
</ul>
</p>

<h3><a name="resources">Resources</a></h3>
<ul>
    <li><a href="gsoc.php">GSoC Guide</a> - Google Summer of Code guide for new prospective students.</li>
    <li><a href="https://webchat.kde.org/#/room/#kstars:kde.org">KStars Web Chat</a>.</li>
    <li><a href="https://docs.kde.org/trunk5/en/extragear-edu/kstars/index.html">KStars Handbook</a> — Detailed KStars manual</li>
    <li><a href="https://api.kde.org/appscomplete-api/kstars-apidocs/kstars/html/index.html">KStars API</a> — KStars API documentation</li>
    <li><a href="https://invent.kde.org/education/kstars">Git repository</a> — hosts the latest code of KStars</li>
    <li>The <a href="https://mail.kde.org/mailman/listinfo/kstars-devel">kstars-devel mailing list</a> — for developers of KStars and enthusiasts to discuss</li>
    <li><a href="https://www.indilib.org/about/ekos.html">Ekos</a> — Complete astrophotography and observatory control framework</li>
    <li><a href="https://www.indilib.org/index.php?title=Main_Page">INDI</a> — protocol to operate astronomical instruments</li>
</ul>

<!--
<?php
  kde_general_news("./news.rdf", 10, true);
?>
-->

<hr />

<h3><a name="Credits">Credits</a></h3>
<p>
<strong>Original Author:</strong> Jason Harris <jharris at 30doradus dot org> <br />
<strong>Current Maintainer:</strong> Jasem Mutlaq <mutlaqja at ikarustech dot com> <br />
<strong>INDI/Ekos:</strong> Jasem Mutlaq <mutlaqja at ikarustech dot com> <br />
For our full credits list, see <a href="https://edu.kde.org/kstars/contributors.php">our contributors' page</a>.
</p>

<h3><a name="bugs">Problems and Bugs</a></h3>
<p>
To report a bug against KStars, you can use the <a
href="https://bugs.kde.org/">KDE bug reporting interface</a>. Choose
"kstars" as the product to file the bug against.
</p>
<p>
KStars has a lot of known issues, and many unknown issues. In either case, reporting a bug is always helpful. If you find problems with KStars, you are also most welcome to tell us on our <a href='https://mail.kde.org/mailman/listinfo/kstars-devel'>mailing list</a> or on our <a href='https://webchat.kde.org/#/room/#kstars:kde.org'>Web Chat channel</a> where you can chat directly with developers and users. For some of these issues, there are temporary workarounds, and asking on the mailing list will help you discover these antidotes for smyptomatic relief. We welcome your feedback, and discussion on both the mailing list and IRC channel. See the <a href="#join">Join Us</a> section for more information and pointers.
</p>

<?php
  include "joinus.inc";
?>

<h3>Check out these other free and open source astronomy software</a></h3>
<p>
<ul>
  <li><strong><a href="https://www.stellarium.org">Stellarium</a></strong>:
  Stellarium has the most aesthetic rendering of the sky map. Ideal
  for giving public shows. Has a larger star catalog than KStars. It
  can distort images for projection on fish-eye planetarium projectors.</li>
  <li><strong><a href="https://www.ap-i.net/skychart/">SkyChart (Cartes
  du Ciel)</a></strong>: Cartes du Ciel has the largest database of
  astronomical objects for any free software. It has support for the
  very very extensive star catalog, PPMXL. Also has tons of other
  downloadable catalogs.
</ul>
</p>

<br />
<hr width="50%" align="center" />
<p>Webmasters: Jasem Mutlaq, Akarsh Simha<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
