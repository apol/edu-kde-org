<?php
  $site_root = "../";
  $page_title = 'KStars: AstroInfo Project';
  
  include ( "header.inc" );
?>

<h2>The AstroInfo Project: The Everything2 of Astronomy</h2>

<p>
KStars is primarily a learning tool, to teach people about the wonder
of the night sky, our window to the universe.  There is a <b>lot</b>
to learn, however, and the program alone can't do everything.  We are
writing a collection of short articles to be included with KStars
to help explain the concepts behind KStars.  There are a lot of articles
we'd like to include; many more than we can possibly write ourselves in a
reasonable amount of time.
</p><p>
If you are interested in contributing an article to AstroInfo, please
join us on the 
<a 
href="https://lists.sourceforge.net/lists/listinfo/kstars-info">kstars-info
mailing list</a>.  The list is used for submission of new articles, and 
review of existing articles.  Feel free to use plain text, HTML, or 
DocBook XML when writing or editing an article; we can convert
the article to DocBook XML from other formats.
</p><p>
You can read the current AstroInfo articles in the <a
href="https://docs.kde.org/stable/en/kdeedu/kstars/">KStars Handbook</a>, 
in the 
<a href="https://docs.kde.org/stable/en/kdeedu/kstars/astroinfo.html">AstroInfo 
Chapter</a>.
</p>
<hr />
<?php
  include "footer.inc";
?>


