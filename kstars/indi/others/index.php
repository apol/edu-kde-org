<?php
  $site_root = "../../";
  $page_title = 'Other devices supported under KStars';

  include ( "header.inc" );
?>

<a href="indi-welcome.png">
   <img alt="[Screenshot]" src="indi-welcome.png" width="600" />
</a>

<br />
<div id="quicklinks">
[
  <a href="#Overview">Overview</a> |
  <a href="#Devices">Devices</a> |
]
</div>
<br />

<h3><a name="Overview">Overview</a></h3> <p> <img alt="Other Devices" src="other_devices.png" style="float:right"><b>KStars</b> supports
a large number of astronomical instruments provided by the <a href="http://www.indilib.org">INDI Library</a>. For devices that do not fall in the traditional categories, we list them here. They may include custom drivers for motion controllers, encoders, spectrometers, domes, weather stations...etc. If you are using or developing non-traditional INDI drivers, please let us know and we will add them here. Drivers should be not tightly coupled to a specific configuration of a device or setup in an observatory.</p> <hr />

<h3><a name="Devices">Devices</a></h3><p>
The following is a list of other devices supported under KStars:</p>
<table style="border: 1px solid black;" width="100%">
	<tr>
		<th>Manufaturer</th>
		<th>Model</th>
		<th>Image</th>
		<th>Driver</th>
		<th>Support Status</th>
	</tr>


	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Shoestring Astronomy</td>
		<td style='vertical-align:middle;border: 1px solid black;'>GPUSB</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="gpusb.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_gpusb</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>
	
	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Diffraction Limited</td>
		<td style='vertical-align:middle;border: 1px solid black;'>MaxDome II : Observatory Dome Control System</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="maxdomeii.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_maxdomeii</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Radio Astronomy Supplies</td>
		<td style='vertical-align:middle;border: 1px solid black;'>Spectracyber 1420Mhz hydrogen line spectrometer</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="spectracyber.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_spectracyber</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

</table>

<hr/>


<br />
<hr width="50%" align="center" />
<p>Webmasters: Jasem Mutlaq<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
