<?php
  $site_root = "../../";
  $page_title = 'CCDs supported under KStars';

  include ( "header.inc" );
?>

<a href="indi-welcome.png">
   <img alt="[Screenshot]" src="indi-welcome.png" width="600" />
</a>

<br />
<div id="quicklinks">
[
  <a href="#Overview">Overview</a> |
  <a href="#CCDs">CCDs</a> |
  <a href="#FAQ">FAQ</a> |
]
</div>
<br />

<h3><a name="Overview">Overview</a></h3> <p> <img alt="FITS Viewer" src="fitsviewer.png" style="float:right"><b>KStars</b> supports
a large number of astronomical instruments provided by the <a href="http://www.indilib.org">INDI Library</a>. By default, KStars only ships the CCD simulator driver, all other CCD drivers are shipped separately as 3rd party INDI drivers. Binary packages for these drivers are available for several distributions, including Ubuntu. Please check INDI website for download details. All CCD drivers (including webcams) send the image in FITS format, which are then displayed in KStars FITS Viewer tool. Using INDI control panel, you can capture images, set temperature, subframe, binning, and frame type (dark, light, bias..etc). To capture a sequence of images with filter wheel support, please use Ekos capture module included with KStars. The drivers can record in the FITS file the target's equatorial coordinates, filters used, JD, and more metadata. A CCD may be either designated as the primary CCD or a guider CCD in KStars Ekos tool. After an image is displayed in the FITS Viewer tool, you may perform some basic adjustments to the image including low pass and histogram equalization filters. Furthermore, KStars can detect and mark stars given that overall noise within an image is not too high. As with other INDI drivers, you may run a CCD driver either directly or remotely. For remote operations, compression of FITS data is recommended.
 </p> <hr />

<h3><a name="CCDs">CCDs</a></h3><p>
The following is a list of CCDs supported under KStars:</p>
<table style="border: 1px solid black;" width="100%">
	<tr>
		<th>Manufaturer</th>
		<th>Model</th>
		<th>Image</th>
		<th>Driver</th>
		<th>Support Status</th>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Quantum Scientific Imaging (QSI)</td>
		<td style='vertical-align:middle;border: 1px solid black;'>All models</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="qsi.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_qsi_ccd</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Santa Barbra Instruments Group (SBIG)</td>
		<td style='vertical-align:middle;border: 1px solid black;'>All models</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="sbig.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_sbig_ccd</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>QHY, Orion</td>
		<td style='vertical-align:middle;border: 1px solid black;'>QHY5, Orion Star Shoot Autoguider</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="qhy5.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_qhy_ccd</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Starlight Xpress</td>
		<td style='vertical-align:middle;border: 1px solid black;'>All models, including Loadstar Autoguider.</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="sx.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_sx_ccd</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Cannon</td>
		<td style='vertical-align:middle;border: 1px solid black;'>EOS DSLR</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="cannon.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_gphoto_ccd</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Apogee</td>
		<td style='vertical-align:middle;border: 1px solid black;'>Alta-E & Alta-U</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="alta.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_apogeeu_ccd, indi_apogeee_ccd</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Finger Lakes Instruments</td>
		<td style='vertical-align:middle;border: 1px solid black;'>All models</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="fli.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_fli_ccd</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Webcams</td>
		<td style='vertical-align:middle;border: 1px solid black;'>Any camera supported by Video4Linux. KStars also support live video feed from webcams.</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="v4l.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_v4l_generic</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>




	
</table>

<hr/>


<h3><a name="FAQ"></a></h3>
<?php
 
 $faq = new FAQ();

$faq->addQuestion("How can I download driver for my CCD?", "You can either:
<ul><li>Download the source archive (tar.gz) driver from <a href='https://sourceforge.net/projects/indi/files/'>INDI's Sourceforge site</a> and compile it yourself. This requires libindi development files.</li>
<li>Search for packages in your distribution software center or package manager.</li>
<li>If you run Ubuntu, you can download latest drivers directly from <a href='http://indilib.org/index.php?title=Download_INDI#Ubuntu'>Jasem Mutlaq's Personal Package Archive (PPA).</a></li></ul>");


  $faq->show();
?>

<hr/>

<br />
<hr width="50%" align="center" />
<p>Webmasters: Jasem Mutlaq<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
