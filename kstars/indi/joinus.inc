<h3><a name="join">Join Us!</a></h3>

<p>
Interested in helping develop KStars? We need programmers, of course,
but even if you don't code, we can still use your help. If you are an
astronomy enthusiast, there is our <a
href="astroinfo.html">AstroInfo</a> project. We could also use help
writing documentation and translating. Even bug reports and feature
requests are a big help, so keep them coming!</p>
<p>

<p>
Some pointers to get started:
<ul>
<li>Join our <a href='https://mail.kde.org/mailman/listinfo/kstars-devel'>mailing list</a></li>
<li>Some junior and not-so-junior jobs <a href='http://techbase.kde.org/Projects/Edu/KStars/JuniorJobs'>here!</a></li>
<li>Details on building KDE trunk <a href='http://techbase.kde.org/Getting_Started/Build/KDE4'>on the KDE techbase</a></li>
</ul>
</p>
