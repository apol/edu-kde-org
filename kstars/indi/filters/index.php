<?php
  $site_root = "../../";
  $page_title = 'Filter wheels supported under KStars';

  include ( "header.inc" );
?>

<a href="indi-welcome.png">
   <img alt="[Screenshot]" src="indi-welcome.png" width="600" />
</a>

<br />
<div id="quicklinks">
[
  <a href="#Overview">Overview</a> |
  <a href="#Filters">Filters</a> |
  <a href="#FAQ">FAQ</a> |
]
</div>
<br />

<h3><a name="Overview">Overview</a></h3> <p> <img alt="Filter Selection" src="kstars_filter.png" style="float:right"><b>KStars</b> supports
a large number of astronomical instruments provided by the <a href="http://www.indilib.org">INDI Library</a>. By default, KStars ships drivers for Trutech filter wheel and filter simulator. Other filter wheel drivers are shipped separately as 3rd party INDI drivers. Binary packages for these drivers are available for several distributions, including Ubuntu. Please check INDI website for download details. 
Filter wheel may be either directly controlled from INDI Control Panel, or from Ekos Capture module as illustrated in the figure. Some filter driver do no support assigning names to each filter slot (e.g. Slot 0 Red, Slot 1 Blue..etc). Nevertheless, you can assign designated names to each filter wheel slot in KStars options.</p> <hr />

<h3><a name="Filters">Filter Wheels</a></h3><p>
The following is a list of filter wheels supported under KStars:</p>
<table style="border: 1px solid black;" width="100%">
	<tr>
		<th>Manufaturer</th>
		<th>Model</th>
		<th>Image</th>
		<th>Driver</th>
		<th>Support Status</th>
	</tr>


	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Quantum Scientific Imaging (QSI)</td>
		<td style='vertical-align:middle;border: 1px solid black;'>All models</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="qsi_wheel.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_qsi_ccd</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Santa Barbra Instruments Group (SBIG)</td>
		<td style='vertical-align:middle;border: 1px solid black;'>All models</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="sbig_wheel.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_sbig_ccd</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Starlight Xpress</td>
		<td style='vertical-align:middle;border: 1px solid black;'>All models</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="sx_wheel.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_sx_wheel</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>TruTechnology</td>
		<td style='vertical-align:middle;border: 1px solid black;'>All models</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="trutech_wheel.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_trutech_wheel</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

</table>

<hr/>


<h3><a name="FAQ"></a></h3>
<?php
 
 $faq = new FAQ();

$faq->addQuestion("How can I download driver for my filter wheel?", "You can either:
<ul><li>Download the source archive (tar.gz) driver from <a href='https://sourceforge.net/projects/indi/files/'>INDI's Sourceforge site</a> and compile it yourself. This requires libindi development files.</li>
<li>Search for packages in your distribution software center or package manager.</li>
<li>If you run Ubuntu, you can download latest drivers directly from <a href='http://indilib.org/index.php?title=Download_INDI#Ubuntu'>Jasem Mutlaq's Personal Package Archive (PPA).</a></li></ul>");


  $faq->show();
?>

<hr/>

<br />
<hr width="50%" align="center" />
<p>Webmasters: Jasem Mutlaq<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
