<?php
  $site_root = "../../";
  $page_title = 'Focusers supported under KStars';

  include ( "header.inc" );
?>

<a href="indi-welcome.png">
   <img alt="[Screenshot]" src="indi-welcome.png" width="600" />
</a>

<br />
<div id="quicklinks">
[
  <a href="#Overview">Overview</a> |
  <a href="#Focusers">CCDs</a> |
  <a href="#FAQ">FAQ</a> |
  <a href="../">Back</a> 
]
</div>
<br />

<h3><a name="Overview">Overview</a></h3> <p> <img alt="Focus Simulator" src="kstars_focus.png" style="float:right"><b>KStars</b> supports
a large number of astronomical instruments provided by the <a href="http://www.indilib.org">INDI Library</a>. By default, KStars ships the Meade, RoboFocus, Optec TCF-S, and focuser simulator drivers. Other focuser drivers are shipped separately as 3rd party INDI drivers. Binary packages for these drivers are available for several distributions, including Ubuntu. Please check INDI website for download details. Some focusers support absolute positioning, which makes them very useful for automatic focusing application. Autofocus is still possible with relative or focusers without positional feedback as the optimal focus point is determined by calculating the Half-Flux-Radius (HFR) of stars within an image. Using KStars Ekos tool, you can control the focuser in manual and automatic modes. The image must have a suffuciently good signal to noise ratio in order for KStars to detect stars and calculate their respective HFR. 
 </p> <hr />

<h3><a name="Focusers">Focusers</a></h3><p>
The following is a list of Focusers supported under KStars:</p>
<table style="border: 1px solid black;" width="100%">
	<tr>
		<th>Manufaturer</th>
		<th>Model</th>
		<th>Image</th>
		<th>Driver</th>
		<th>Support Status</th>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Technical Innovations</td>
		<td style='vertical-align:middle;border: 1px solid black;'>RoboFocus</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="robofocus.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_robo_focus</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Optec</td>
		<td style='vertical-align:middle;border: 1px solid black;'>TCF-S</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="tcfs.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_tcfs_focus, indi_tcfs3_focus</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Finger Lakes Instruments</td>
		<td style='vertical-align:middle;border: 1px solid black;'>Precision Digital Focuser</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="fli_pdf.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_fli_focus</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Meade</td>
		<td style='vertical-align:middle;border: 1px solid black;'>LX200GPS Microfocuser & 1206 Primary Mirror Focuser</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="meade_micro.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_lx200autostar</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>JMI</td>
		<td style='vertical-align:middle;border: 1px solid black;'>NGF Series & MOTOFOCUS</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="jmi_motofocus.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_lx200autostar</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	

	
</table>

<hr/>


<h3><a name="FAQ"></a></h3>
<?php
 
 $faq = new FAQ();

$faq->addQuestion("How can I download driver for my Focuser?", "You can either:
<ul><li>Download the source archive (tar.gz) driver from <a href='https://sourceforge.net/projects/indi/files/'>INDI's Sourceforge site</a> and compile it yourself. This requires libindi development files.</li>
<li>Search for packages in your distribution software center or package manager.</li>
<li>If you run Ubuntu, you can download latest drivers directly from <a href='http://indilib.org/index.php?title=Download_INDI#Ubuntu'>Jasem Mutlaq's Personal Package Archive (PPA).</a></li></ul>");


  $faq->show();
?>

<hr/>

<br />
<hr width="50%" align="center" />
<p>Webmasters: Jasem Mutlaq<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
