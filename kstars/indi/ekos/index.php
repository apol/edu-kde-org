<?php
  #$site_root = "../../";
  #$page_title = 'Ekos Astrophotography tool for KStars';

  #include ( "header.inc" );
  
  header('Location: http://indilib.org/about/ekos.html');
  
  exit();
?>

<a href="indi-welcome.png">
   <img alt="[Screenshot]" src="indi-welcome.png" width="600" />
</a>

<br />
<div id="quicklinks">
[
  <a href="#Overview">Overview</a> |
  <a href="#Features">Features</a> |
  <a href="#FAQ">FAQ</a> |
]
</div>
<br />

<h3><a name="Overview">Overview</a></h3> <p> <img alt="Ekos Tool" src="ekos.png" style="float:right"><b>Ekos</b> is a tool that aims to provide astronomers using Linux with a single, consistent, and extensible framework to perform astrophotography tasks. This includes polar alignment using drift method, auto-focus & auto-guide capabilities, and capture of single or stack of images with filter wheel support. 
 </p> <hr />

<h3><a name="Features">Features</a></h3><p>
<ul>
	<li>Polar Alignment using drift method.</li>
	<li>Auto and manual focus modes using Half-Flux-Radius (HFR) method.</li>
	<li>Auto guiding based on lin_guider code.</li>
	<li>Batch capture of images with optional prefixes, timestamps, and filter wheel selection.</li>
</ul>

<hr/>


<h3><a name="FAQ"></a></h3>
<?php
 
$faq = new FAQ();

$faq->addQuestion("When will Ekos be ready? I want to use it now!", "Ekos was released in <a href='http://www.kde.org/announcements/4.10/'>KDE 4.10</a> and supports all the above features with the exception of the polar alignment drift method. It continues to be improved.");
$faq->addQuestion("Can I use my CCD/Telescope/Focuser with Ekos?", "Any devices that is supported under INDI is automatically supported under Ekos as it uses INDI for device control.");
$faq->addQuestion("When performing autofocus, what do I set the initial focus step size to?", "A general rule of the thumb is to set the step size to a smaller value the closer you are to optimal focus. The autofocus algorithm moves in discreet number of steps to build the V curve in order to converge to the optimal focus point, and adjusts the step size accordingly.");

$faq->show();
?>

<hr/>

<br />
<hr width="50%" align="center" />
<p>Webmasters: Jasem Mutlaq<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
