<?php
  $site_root = "../";
  $page_title = 'Features';
  
  include ( "header.inc" );
?>

<center><h1> Features organized by use-case </h1> </center>

<p>Pick your use case to see what KStars can do for you.</p>

<br />
<center>
<br />
<div id="quicklinks">
[
  <a href="#education">Education</a> |
  <a href="#astrophotography">Astrophotography</a> |
  <a href="#observation">Visual Observation</a> |
  <a href="#enthusiast">Astronomy Enthusiasts</a> |
]
</div>
<br />
</center>



<a name='education'><h3>Features for Educators</h3></a>
<p>
If you are an instructor, you could recommend KStars to your students
for use as a learning aid. If you're a student, you can learn about
the sky using KStars.
</p>
<p>
Here are some things you can do in KStars to explore astronomy and astrophysics:
</p>
<ul>
   <li> Change your geographic location to see how the view of the sky changes with latitude </li>
   <li> Understand the difference between <emph>sidereal days</emph> and <emph>solar days</emph> by changing the speed of simulation </li>
   <li> Attach trails to planets and set a high simulation rate to see their trajectories as they perform <emph>retrograde motion</emph> </li>
   <li> When technical terms are underlined in blue, click on them to bring up an explanation from the included AstroInfo project </li>
   <li> Explore coordinate systems by switching between equatorial and horizontal coordinates, and perform coordinate conversions in the KStars Astrocalculator </li>
   <li> Learn about various topics in astronomy and astrophysics through the AstroInfo project (part of the KStars Handbook accessible through the Help menu). </li>
   <li> Set the time ahead by a couple 1000 years to see the effects of precession </li>
   <li> Switch on and switch off gravitational lensing effects around the sun to understand Eddington's experiment </li>
   <li> Switch to equatorial coordinates, set simulation rate to 1 year, double click on a star to keep it in focus, and zoom in to see the effects of <emph>aberration of light</emph> </li>
   <li> Switch to equatorial coordinate, set simulation rate to 1 year, double click on an empty region, to see the effects of <emph>precession</emph> and <emph>nutation</emph>. </li>
   <li> Set the time to the distant future to see how the constellations change shape due to the proper motions of stars (prominent in Gemini, for example) </li>
   <li> Right click on any object and open the Details view to easily access lots of information, internet resources, images and professional surveys. </li>
   <li> Obtain sky survey images for any location from the right-click context menu </li>
   <li> Control your observatory using KStars' <a href="indi/">extensive support for astronomy equipment</a>. </li>
   <li> KStars speaks your language -- you are very likely to find a translation / localization for your region. </li>
</ul>

<a name='astrophotography'><h3>Features for Imaging / Astrophotography</h3></a>
<p>
Whether you are at a professional observatory, or an amateur astrophotographer, KStars can help you image the sky. The Ekos imaging suite provides you with a smooth hassle-free workflow, removing the need to switch between multiple software tools for each part of the workflow.
</p>
<ul>
   <li> With extensive support for <a href='indi/'>INDI</a>, KStars can control almost any sort of astronomical equipment, including telescope mounts, motorized focusers, filter wheels and CCD and CMOS cameras. </li>
   <li> Use the FOV calculator to calculate the field-of-view of your imaging setup </li>
   <li> <a href='http://edu.kde.org/kstars/indi/ekos/'>Ekos</a>, KStars' astrophotography suite, provides a complete astrophotography workflow within KStars. Ekos can help with polar alignment, focusing, auto-guiding, and capture. </li>
   <li> The FITS Viewer lets you view FITS data files. </li>
   <li> Calculate conjunctions between celestial objects to find rendezvous that you can capture. </li>
   <li> KStars speaks your language -- you are very likely to find a translation / localization for your region. </li>
</ul>

<a name='observation'><h3>Features for Visual Observation</h3></a>
<p>
KStars has an extensive set of features that enhance your visual observation workflow. With the observation planner, all you need to do is add objects to your list. KStars takes care of ordering them in observing order, and with one click, lets you download DSS images for off-line comparison for observing sites where internet access is unavailable or poor. If you cannot take your laptop to your observing site, you can always export high-quality vector-graphic images of the sky that you can print for use as finder charts.
</p>
<ul>
   <li> With stars down to 16th magnitude, finding sufficient reference stars in the field is easy. </li>
   <li> If you want more than a complete NGC/IC catalog, you can add your own custom catalogs of objects. </li>
   <li> The powerful observation planner in KStars will help you plan your observing session, put objects in observation order, and cache DSS images for offline comparison with only a few clicks. </li>
   <li> You can also log your observations in KStars (this feature needs more polish to be fully usable, though.) </li>
   <li> KStars supports the OAL XML schema for observation logs, which makes these logs compatible with many software and log-sharing websites. </li>
   <li> Calculate your telescope's FOV for various eyepieces and save them in KStars, so you can quickly switch between FOV indicators with Page Up / Page Down keys. </li>
   <li> High quality finder charts allow you to do observation in places without electric power. </li>
   <li> Scripting support gives you the flexibility to write your own scripts to automate creation of finder charts etc. </li>
   <li> The experimental star-hopper feature automatically works out a star-hopping route from a star to a deep-sky object, given your field-of-view. </li>
   <li> The Jupiter's moons tool tells you the positions of Jupiter's Galilean moons, as is normally seen in astronomical ephemerides books. </li>
   <li> Configure KStars to alert you every time it learns of a new supernova in the sky! </li>
   <li> Predict conjunctions between celestial objects. </li>
   <li> Interface with XPlanet to render views of planets at any given time. </li>
   <li> KStars speaks your language -- you are very likely to find a translation / localization for your region. </li>
</ul>


<a name='enthusiast'><h3>Features for any astronomy enthusiast!</h3></a>
<p>
If you're new to astronomy, or are an enthusiast in general, KStars can be your guide as you explore the heavens. Already in the developers' version, KStars can auto-suggest objects for you to see in the night-sky.
</p>
<ul>
   <li> KStars renders an accurate representation of the sky, with the planets, Jupiter's moons, 100 million stars, 10000 deep-sky objects, comets and asteroids. </li>
   <li> Predict conjunctions between celestial objects. </li>
   <li> Learn more about astronomy and astrophysics through the helpful information links and the included AstroInfo project pages. </li>
   <li> (Coming very soon) Get predictions for interesting astronomical objects to observe with your equipment / sky conditions on any particular night. </li>
   <li> KStars speaks your language -- you are very likely to find a translation / localization for your region. </li>
</ul>

<br />
<br />

<center> <h1>Full listing of important features</h1> </center>
<h3>Features unique to KStars</h3>
<p>
Unique features in KStars not found in other free astronomy software
(to the best of our knowledge):
</p>
<ul>
	<li>Fetch sky images from Digitized Sky Surveys, and the internet
	in general.</li>
	<li>Access information resources on sky objects from within KStars.</li>
	<li>Print high-quality, highly customizable, star charts with
	multiple charts at different zoom levels for each object.</li>
	<li>Full fledged astrophotography suite <a href="indi/ekos">Ekos</a> for a smooth imaging workflow</a></li>
	<li>Plan and execute your observation sessions and save DSS
	images for offline use with the Observation Planner and
	"Execute" feature.</li>
	<li>Set up field of view symbols computed from your eyepiece
	and telescope focal length.</li>
	<li>Generate the locations of Jupiter's moons for the near
	future and near past and plot them as shown in professional
	astronomical ephemerides.</li>
	<li>Import your own custom catalogs from within KStars.</li>
	<li>Access some of KStars' internal calculations with the
	Astro-Calculator.</li>
	<li>Supernova alerts: KStars can automatically alert you about new supernovae at startup!</list>
	<li>Click on technical terms / astronomy jargon to open up an
	explanation from the included AstroInfo project.</li>
	<li>Use your favorite map projection method to render the
	celestial sphere on a flat screen.</li>
	<li>A very powerful tool to find conjunctions and oppositions (somewhat experimental).</li>
	<li>Compute star-hopping routes (experimental; incomplete but
	functional).</li>
	<li>Auto-suggest sky objects for beginners (coming soon).</li>
	<li>More translations and localization than most other
	software.</li>
</ul>

<h3>Catalogs</h3>
<ul>
	<li>Default catalog consisting of stars to magnitude 8</li>
    	<li>Extra catalogs consisting of 100 million stars to magnitude 16</li>
	<li>Default deep-sky NGC / IC catalog (about 10000 objects)</li>
	<li>Download a more accurate revised NGC / IC catalog</li>
	<li>Downloadable catalogs including Messier Images, Abell Planetary Nebulae</li>
	<li>Add your own custom catalogs</li>
</ul>

<h3>Telescope / Equipment Control</h3>
<ul>
    <li><a href="indi/">Integration with INDI providing support for a wide range of astronomy instruments</a></li>
    <li>Full fledged astrophotography suite <a href="indi/ekos">Ekos</a> for a smooth imaging workflow</a></li>
</ul>

<h3>Other</h3>
<ul>
    <li>Corrections for precession, nutation and atmospheric refraction</li>
    <li>Tools for retrieval of data from Online Databases</li>
    <li>Scriptable actions using D-Bus</li>
    <li>Features for Educators and Students:
    <li>Adjustable simulation speed in order to view phenomena that happen over long timescales</li>
    <li>Astroinfo project to help facilitate learning with the aid of KStars</li>
    <li>What's up tonight tool to see what objects are visible tonight</li>
    <li>Altitude vs Time tool to plot the time-variation in the altitude of an object (good to decide observation times)</li>
    <li>Sky Calendar tool to see how the planets move across the sky over a long duration of time</li>
</ul>

<?php
  include "footer.inc";
?>
