<?php
  $site_root = "../";
  $page_title = 'KStars';
  
  include ( "header.inc" );

  $appinfo = new AppInfo( "KStars" );
  $appinfo->setVersion( "3.5.1" );
  $appinfo->setCopyright( "2001", "the KStars Team" );
  $appinfo->setLicense("gpl");


  $appinfo->addAuthor( "Jason Harris",	"kstars AT 30doradus DOT org", "Original Author");
  	
  $appinfo->addAuthor( "Jasem Mutlaq",
  	"mutlaqja AT ikarustech DOT com", "<strong>Maintainer</strong>, Major code contributions, INDI/Ekos" );
  
  $appinfo->addContributor( "Akarsh Simha", 
        "akarsh DOT simha AT kdemail DOT net", "Major Code contributions" );
    $appinfo->addContributor( "Hy Murveit",
        "hy AT murveit DOT com", "Major Code contributions" );
  $appinfo->addContributor( "Eric Dejouhanet",
        "eric DOT dejouhanet AT gmail DOT com", "Major Code contributions" );
  $appinfo->addContributor( "Wolfgang Reissenberger",
        "sterne-jaeger AT t-online DOT de", "Major Code contributions" );
  $appinfo->addContributor("Robert Lancaster", "Major Code contributions" );        
  $appinfo->addContributor( "James Bowlin",
  	"bowlin AT mindspring DOT com", "Major code contributions, reorganization of code, HTM" );
  $appinfo->addContributor( "Heiko Evermann",
  	"heiko AT evermann DOT de", "Major code contributions" );
  $appinfo->addContributor( "Thomas Kabelmann",
  	"tk78 AT gmx DOT de", "Major code contributions" );
  $appinfo->addContributor( "Pablo de Vicente",
  	"pvicentea AT wanadoo DOT es", "Major code contributions" );  
  $appinfo->addContributor( "Mark Hollomon",
  	"mhh AT mindspring DOT com", "Major code contributions" );  
  $appinfo->addContributor( "Carsten Niehaus",
        "cniehaus AT kde DOT org", "Major code contributions" );
  $appinfo->addContributor( "Luciano Montanaro",
  	"", "Additional Data / Catalogs, Patches" );
  $appinfo->addContributor( "Glenn Becker",
        "", "Additional Data / Catalogs" );
  $appinfo->addContributor( "Carl Knight",
   	"sleepless DOT knight AT paradise DOT net DOT nz", "Abell Planetary Catalog" );
  $appinfo->addContributor( "M&eacute;d&eacute;ric Boquien",
  	"mboquien AT free DOT fr", "Code contributions" );
  $appinfo->addContributor( "J&eacute;r&ocirc;me Sonrier",
        "jsid AT emor3j DOT fr DOT eu DOT org", "Code contributions" );  
  $appinfo->addContributor( "Alexey Khudyakov", 
        "alexey DOT skladnoy AT gmail DOT com", "Code contributions" );
  $appinfo->addContributor( "Prakash Mohan", 
        "prakash DOT mohan AT kdemail DOT net", "The Observation Planner and Execute feature" );
  $appinfo->addContributor( "Henry de Valence", 
        "hdevalence AT gmail DOT com", "OpenGL painting" );
  $appinfo->addContributor( "Victor Carbune", 
        "victor DOT carbune AT kdemail DOT net", "SQLite backend for
  	object data storage and retrieval, major contributions." );
  $appinfo->addContributor( "Samikshan Bairagya", 
        "samikshan AT gmail DOT com", "Supernovae in KStars" );
  $appinfo->addContributor( "Rafał Kułaga", 
        "rl DOT kulaga AT gmail DOT com", "<strong>Maintainer</strong> Better printing support for finder charts." );
  $appinfo->addContributor( "Łukasz Jaskiewicz", "", "Refactoring of OAL support." );  

  $appinfo->show();
?>

<h4>GSoC, SoCiS, SoK students</h4>
<p>
A listing of our Google Summer of Code, ESA SoCiS, and Season of KDE students:
</p>
<ul>
<li>Artem Fedoskin (GSoC 2016): KStars Lite</li>
<li>Cojocaru Raphael (GSoC 2016): KStars on Windows</li>
<li>Akarsh Simha (GSoC 2008): 100 million stars</li>
<li>Prakash Mohan (GSoC 2009): Observation planner</li>
<li>Victor Cărbune (GSoC 2010): SQLite Database for Objects</li>
<li>Henry de Valence (GSoC 2010): OpenGL rendering backend</li>
<li>Rafał Kułaga (GSoC 2011): Professional finder chart printing</li>
<li>Samikshan Bairagya (SoK 2011): Making KStars more suitable for a scientifically inclined user</li>
<li>Łukasz Jaśkiewicz (ESA SoCiS 2011): Making KStars more useful for beginners</li>
</ul>

<H4>A round of applause for our successful Google Code-in students!</H4>
<p>
These pre-university students contributed to KStars as a part of
Google Code-in 2010.
</p>

<ul>
<li>Cezar Mocan</li>
<li>Ana-Maria Constantin</li>
<li>Carl Gao</li>
<li>Valery Kharitonov</li>
<li>Diego Luca Candido</li>
<li>Kristian Ivanov</li>
</ul>

<H4>Thanks for the patches and testing!</H4>
<ul>
<li>Laurent Montel</li>
<li>Eckhart W&ouml;rner</li>
<li>Burkhard L&uuml;ck</li>
<li>Nico Dietrich</li>
<li>James Cameron</li>
<li>Ralf Habacker</li>
<li>Daniel Holler</li>
<li>Patrick Spendrin</li>
<li>Nuno Pinheiro</li>
<li>Andrew Buck</li>
<li>Patrice Levesque</li>
<li>Douglas Phillipson</li>
<li>Andrey Cherepanov</li>
<li>Vipul Kumar Singh</li>
<li>Jain Basil Aliyas</li>
<li>Jeamy Lee</li>
<li>Sivaramakrishnan S</li>
<li>Adhiraj Alai</li>
<li>Lukas Middendorf</li>
<li>Sruthi Devi</li>
<li>Keith Rusler</li>
<li>Frederik Gladhorn</li>
<li>Keerthi Kiran</li>
<li>Jure Repinc</li>
<li>Aditya Bhatt</li>
<li>Alessio Sangalli</li>
</ul>
<p>
And everyone else who helped us by filing those bug reports, popularizing our software...
</p>



<?php
   include('joinus.inc');
   include('footer.inc');
?>
