<h3>Bug reports to be resolved</h3>
<ul>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=69818">Empty sky on PPC</a>.
    Need more info, can't reproduce.</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=71434">date boxes are too 
    small in tools</a>.  Can't reproduce.</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=77564">Sun and other planets 
    overlay in dumped image</a>.  Can't reproduce.</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=81303">KStars crashes with 
    segfault after loading</a>.  Can't reproduce.</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=85322">Cannot start INDI 
    service</a>.  Can't reproduce.</li>
</ul>

<h3>Other Required Fixes</h3>
<ul>
<li>Improve opaque ground polygon</li>
<li>Fix custom catalogs, make it *much* easier to add custom objects</li>
<li>UI file for Location dialog (and others?)</li>
<li><b>OpsDialog</b>: child widgets should disable when parent deselected:
CNames, MW, Hide obj.</li>
<li><b>AltVsTime</b>: can add custom object more than once, because coords are 
re-precessed</li>
<li><b>Details</b>: "Edit Link" button should allow link text to be changed</li>
<li><b>Details</b>: Log window: get rid of Save button; text should silently autosave</li>
<li><b>ScriptBuilder</b>: Does not warn if Save As target already exists</li>
<li>Showing star mag label, but not name label, prepends "star" to label</li>
</ul>

<h3>Wishlist Items</h3>
<ul>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=51299">Add text labels 
    to celestial lines</a></li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=63369">Display multiple views</a> 
    (low priority)</li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=66177">KStars screensaver</a></li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=68529">Predict occultations 
    and eclipses</a></li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=77748">Show Magelanic Cloud 
    contours</a></li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=79671">Ability to add individual 
    custom objects or "pushpin" markers</a></li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=81617">Ability to save and reload 
    "view profiles" or "sky bookmarks"</a></li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=81760">Add "Goto" button to 
    Details window</a></li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=82946">Include Constellation in 
    object details</a></li>
<li><a href="http://bugs.kde.org/show_bug.cgi?id=83125">Add new cities in 
    Sweden</a></li>
</ul>
  

<h3>New Features:</h3>
<ul>
<li><b>Almanac Tool</b>: show dates of Sun events (equinoxes/soltices), 
    major moon phases, eclipses, etc.  Possible integration with KOrganizer?</li> 
<li><b>Improve Solar System Viewer</b>: include minor bodies</li>
<li><b>Improve Jupiter Moons Tool</b>: show dates rather than day offset; show 
    position of moons at t0</li>
<li>Contour lines for large nebulae</li>
<li>Alternative sky-projection schemes</li>
<li>Indicate binarity/variability of stars on map?</li>
<li>DCOP functions for drawing text and lines on the sky map</li>
<li>Better rendering of comets and asteroids</li>
</ul>


<h3>Large Projects</h3>  
<ul>
<li>OpenGL rendering</li>
<li>more planet satellites</li>
<li>Add Earth satellites (man-made)</li>
<li>Update moon equations to Meeus 1998</li>
</ul>

        
<h3><a href="http://indi.sf.net">INDI</a></h3>
<ul>
<li>Support for ACL-based telescopes.</li>
<li>LX200 AltAz and Polar Park feature.</li>
<li>Non-siderial tracking for Celestron.</li>
<li>INDI schedualer deamon with RTML-based observation requests for fully 
automated operations.</li>
</ul>
  

<h3>Ever-present needs:</h3>
<ul>
  <li>Improve documentation</li>
  <li>More AstroInfo articles</li>
  <li>More Internet links</li>
</ul>
