<?php
	$site_root = "../";
	$page_title = 'KStars - Screenshots';
	
	include ( "header.inc" );
	include( "../site_includes/class_edugallery.inc" );
  
	$gal = new EduGallery( "KStars Screenshots" );
				
	$gal->addImage( "screens_2.7.7/thumb_kstars_main.png",   "screens_2.7.7/kstars_main.png",     200, 113, "[Screenshot]", "KStars Primary Window", "" );
	$gal->addImage( "screens_2.7.7/thumb_kstars_billionsandbillions.png", "screens_2.7.7/kstars_billionsandbillions.png", 200, 113, "[Screenshot]", "Billions and Billions", "100 million stars using USNO Nomad Catalog" );
	$gal->addImage( "screens_2.7.7/thumb_kstars_popup.png", "screens_2.7.7/kstars_popup.png", 200, 113, "[Screenshot]", "Popup menu", "" );
	
	$gal->startNewRow();
	
	$gal->addImage( "screens_2.7.7/thumb_kstars_detaildialog.png", "screens_2.7.7/kstars_detaildialog.png", 200, 113, "[Screenshot]", "Detail dialog", "Examining Detailed info for M42" );		
	$gal->addImage( "screens_2.7.7/thumb_kstars_calculator.png",    "screens_2.7.7/kstars_calculator.png",    200, 113, "[Screenshot]", "Astrocalculator", ""  );
	$gal->addImage( "screens_2.7.7/thumb_kstars_calender.png",    "screens_2.7.7/kstars_calender.png",    200, 113, "[Screenshot]", "Sky Calender", "" );
	
	$gal->startNewRow();  
	
	$gal->addImage( "screens_2.7.7/thumb_kstars_ekos.png", "screens_2.7.7/kstars_ekos.png", 200, 113, "[Screenshot]", "Ekos Astrophotography Tool", "" );
	$gal->addImage( "screens_2.7.7/thumb_kstars_fov_editor.png", "screens_2.7.7/kstars_fov_editor.png", 200, 113, "[Screenshot]", "FOV Editor", "" );		
	$gal->addImage( "screens_2.7.7/thumb_kstars_observationplanner.png", "screens_2.7.7/kstars_observationplanner.png",     200, 113, "[Screenshot]", "Observation Planner", "With Altitude vs. Time tool" );
	
	$gal->startNewRow();  
	
	$gal->addImage( "screens_2.7.7/thumb_kstars_solarsystem.png", "screens_2.7.7/kstars_solarsystem.png", 200, 113, "[Screenshot]", "Solar System Tool", "With Jupiter moon tool" );
	$gal->addImage( "screens_2.7.7/thumb_kstars_ghns.png", "screens_2.7.7/kstars_ghns.png", 200, 113, "[Screenshot]", "Get More Data Online", "" );
	$gal->addImage( "screens_2.7.7/thumb_kstars_whatsuptonight.png", "screens_2.7.7/kstars_whatsuptonight.png", 200, 113, "[Screenshot]", "What is up tonight tool", "" );
	
	$gal->startNewRow();  
	
	$gal->addImage( "screens_2.7.7/thumb_kstars_mac_constellation.png", "screens_2.7.7/kstars_mac_constellation.png", 200, 113, "[Screenshot]", "Constellation Art", "" );
	$gal->addImage( "screens_2.7.7/thumb_kstars_mac_galaxy.png", "screens_2.7.7/kstars_mac_galaxy.png", 200, 113, "[Screenshot]", "Explore Galaxy with What is Interesting", "" );
	$gal->addImage( "screens_2.7.7/thumb_kstars_mac_wit.png", "screens_2.7.7/kstars_mac_wit.png", 200, 113, "[Screenshot]", "What is Interesting Tool", "" );

	$gal->show();

	include( "footer.inc" );
?>
