<?php
  $site_root = "../";
  $page_title = 'KStars 0.9 Release Announcement';
  
  include ( "header.inc" );
?>

<p>
2 May 2002: The KStars team announces the release of KStars 0.9, the latest
version of the graphical desktop planetarium for KDE.  This version
contains many improvements over the version that shipped with KDE 3.0,
which was functionally identical to our last independent release
(0.8.5).  Some of the highlights include:
</p>


<ul>
<li> Configurable user interface: you can choose to hide the toolbars
     and/or the information panel.

<li> Animated slewing: When pointing to a new position, KStars will show
     the sky scrolling by as it moves to the new position (you can also
     revert to the old "snap-to-focus" behavior).

<li> Planets and deep-sky objects are now drawn with realistic angular
     sizes and position angles.

<li> The NGC/IC catalog is now complete (13,000 objects compared to
     10,000 in older versions).  In addition, the coordinates of these
     objects are more precise.

<li> Coordinates of objects now corrected for aberration and atmospheric
     refraction.  Planet coordinates are corrected for light travel time
     delay.

<li> Added View Options toolbar, allowing one-click control over whether
     stars, planets, deep-sky objects, constellations, the Milky Way,
     the coordinate grid, and the horizon are drawn.

<li> Ability to add custom object catalogs.

<li> Manual focus function (enter exact coordinates to center on).

<li> KStars now properly accounts for Daylight Savings Time (a.k.a.
     "Summer Time"), using 25 different location-specific rules.

<li> Time can now run backwards, and a wider range of available time
     steps, up to +/- 100 years per frame!

<li> Added DCOP interface, which will form the foundation of our upcoming
     scripting features.

<li> Added AstroInfo articles for stars, parallax, and the magnitude
     scale.

<li> Several performance, efficiency and stability improvements.
</ul>


<h3>Download:</h3>
<hr width="25%" />

<p>
You can download source tarballs from our official repository.  There
are two archives, depending on your version of KDE:
</p>

<ul>
<li><a href="http://prdownloads.sourceforge.net/kstars/kstars-0.9-kde3.tar.gz">kstars-0.9-kde3.tar.gz</a> Tarball for KDE 3.0
<li><a href="http://prdownloads.sourceforge.net/kstars/kstars-0.9-kde2.tar.gz">kstars-0.9-kde2.tar.gz</a> tarball for KDE 2.x
</ul>

<p>
For installation instructions, see the INSTALL file.
</p>

<h3>Feedback:</h3>
<hr width="25%" />

<p>
Questions and comments can be sent to the lead developer
(<a href="mailto:kstars@30doradus.org">kstars@30doradus.org</a>) or the
developer mailing list
(<a href="mailto:kstars-devel@lists.sourceforge.net">kstars-devel@lists.sourceforge.net</a>).
</p>
<p>
Bug reports can be sent to either of the above addresses, but it's
better to send them to the KDE bug tracking system, using the "Report
Bug" item in the Help menu, or by sending email to
<a href="mailto:submit@bugs.kde.org">submit@bugs.kde.org</a>.
</p>
<p>
Would you like to get involved with KStars development?  If you want
to help with the code, subscribe to the developer mailing list and let
us know what you'd like to work on.  We could also use help with
documentation, AstroInfo articles, and gathering resources (i.e.,
obtaining more images and URL links to attach to objects).  Even bug
reports and comments are a big help.
</p>
<p>
We hope you enjoy KStars.
</p>

<p>
--The KStars team<br />
<a href="http://edu.kde.org/kstars">http://edu.kde.org/kstars</a>
</p>

<?php
  include "footer.inc";
?>
