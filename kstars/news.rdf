<?xml version="1.0"?>
<!DOCTYPE rss SYSTEM "http://my.netscape.com/publish/formats/rss-0.91.dtd">
<rss version="0.91">
<channel>
<title>edu.kde.org/KStars: KStars</title>
<link>http://edu.kde.org/kstars</link>
<description>Desktop Planetarium for KDE</description>
<language>en-us</language>
<webMaster>kstars@30doradus.org (Jason Harris)</webMaster>
<copyright>Copyright (c) 2007, Jason Harris</copyright>

<item>
  <title>OpenGL support!</title>
  <date>October 17, 2010</date>
  <fullstory>
    <p>
      KStars can now use OpenGL to render the sky map. The feature is
      still buggy, but we expect them to be fixed pretty soon. We
      expect a reasonably workable OpenGL version in the KDE 4.6 SC
      release.
    </p>
    <p>
      Google Summer of Code 2010 student and passionate math
      major <a href="http://hdevalence.wordpress.com">Harry de
	Valence</a> revamped the way drawing is done in KStars to
      achieve this. Owing to some really old code in KStars, this was
      quite an effort and a significant deal of the OpenGL "port" is
      now complete.
    </p>
    <p>
      One of the major decisions we took while doing this, was to
      retain the old QPainter way of painting as well. This means that
      if you don't have graphics acceleration on your hardware, you
      could just use the good old simple KStars, without losing speed.
    </p>
  </fullstory>
</item>

<item>
  <title>What's brewing in the future releases?</title>
  <date>July 5th, 2010</date>
  <fullstory>
    <p>
      A lot. We are in the proccess of building a moon phase almanac
      feature that quickly lets you see the moon phases this month, so
      that you can easily see when you can go observing. We are also
      going to build a feature that will make beginners' KStars
      experience better.
    </p>
    <p>
      In the next release, you can expect to see a better managed
      object database, thanks to Victor Carbune's Google Summer of
      Code work, which will put our data into a relational database
      and give you access to perform powerful searches.
    </p>
    <p>
      We are also in the process of getting KStars to use OpenGL for
      painting, which will make a lot of very aesthetic graphical
      effects possible! Harry de Valence, KStars' Google Summer of
      Code student from Canada is working on this project and has
      already obtained some success in painting using the OpenGL
      libraries.
    </p>
  </fullstory>
</item>

<item>
  <title>KStars at Akademy 2010</title>
  <date>July 4th, 2010</date>
  <fullstory>
    KStars contributor Akarsh Simha gave a short demo of KStars at
    Akademy 2010, the annual conference of the KDE community. Three
    KStars developers are at Akademy 2010, and they've been talking to
    a lot of people around to get ideas!
  </fullstory>
</item>

<item>
  <title>KStars is now an awesome observation planner!</title>
  <date>February 9th, 2010</date>
  <fullstory>
    <p>
      With the KDE 4.4 SC, Prakash Mohan's GSoC work has been released
      in KStars. KStars now has an awesome observation planner to plan
      your observation sessions. KStars can also execute your
      observation session by loading a session and hitting Ctrl + 2,
      but that feature is going to improve more in the future.
    </p>
    <p>
      Whenever you wish to see an object, you can add it to your
      observation wishlist. This can be done by right clicking on the
      object in the context menu. Once the object is added to your
      wishlist, you may transfer the objects visible on a particular
      night into the observation session planner, and KStars will
      automatically suggest observation times (looking at
      culmination). You can also download images for the objects from
      DSS / SDSS with a click of a button, and if you don't find an
      image appealing for comparison with your observations, you can
      always replace it by searching the internet for another
      image. Even if you don't have an internet connection at the
      location where you observe, you can always cache these images
      on your harddisk and take your laptop to your observing venue!
    </p>
    <p>
      The execute session option allows you to log information about
      objects as you observe them. Alternately, you may also write a
      short log in the session planner itself.
    </p>
    <p>
      We wish you a lot of fun planning your observations with KStars!
    </p>
  </fullstory>
</item>

<item>
	<title>KStars-1.5 released</title>
	<date>January 29th, 2009</date>
	<fullstory>
		<p>
		      KStars version 1.5.0 was released along with KDE-4.2 on January 27th, 2009.
		      Click <a href="http://edu.kde.org/kstars/kstars-4.2-features.php">here</a>
		      to learn what's new.
		</p>
		<br />
	</fullstory>
</item>
 
<item>
	<title>Updated Daylight Savings Time rules</title>
	<date>March 12th, 2007</date>
	<fullstory>
		<p>
			Daylight Savings Time rules have changed for locations in the United States.  
			Starting this year, Daylight Savings Time begins on the second Sunday in March, 
			and reverts on the first Sunday in November.  To update these rules in KStars, 
			all you need to do is open the "Get New Stuff" tool (<b>Ctrl+D</b>) and install 
			the "Updated Daylight Savings Time rules" item from the list.   
		</p>
		<br />
	</fullstory>
</item>

<item>
	<title>KStars-1.2 released</title>
	<date>March 13th, 2006</date>
	<fullstory>
		<p>
			I've finally put together a tarball release for 
			<a href="http://edu.kde.org/kstars/index.php#dl">KStars 1.2</a>, 
			the version corresponding to KDE-3.5.  Sorry it's so late; I guess 
			I thought I had already done it.  
		</p>
		<br />
	</fullstory>
</item>

<item>
	<title>Comets and Asteroids Updated</title>
	<date>March 6th, 2006</date>
	<fullstory>
		<p>
			I have just updated the ephemerides for comets and asteroids.
			To get the new data, just open the Get New Stuff tool in 
			KStars (Ctrl+D), and select "ephemerides" from the list.  The 
			number of comets has increased from 500 to 1300, including the 
			recently-discovered <a href="http://www.space.com/spacewatch/060224_night_sky.html">Comet Pojmanski (2006 A1)</a>.
		</p>
		<br />
	</fullstory>
</item>

<item>
	<title>KStars Community Forums</title>
	<date>March 2th, 2006</date>
	<fullstory>
		<p>
			We now have a web forum for the KStars Community:
			<a href="http://kstars.30doradus.org">http://kstars.30doradus.org</a>
			We're hoping to build a community out of our active user base, 
			so come on in and introduce yourself!
		</p>
		<br />
	</fullstory>
</item>

<item>
	<title>Introducing: SVN snapshot releases</title>
	<date>June 29th, 2005</date>
	<fullstory>
		<p>
			Thanks to Carsten Niehaus, KDE-Edu now has a ruby script that 
			makes it very easy to create release tarballs from the SVN tree.
			I have modified this script a bit, and will be using it to create 
			"SVN snapshot" releases periodically (about once per week).
			You can find the snapshot releases <a href="snapshot.php">here</a>.
		</p>
		<br />
	</fullstory>
</item>

<item>
	<title>Comets and Asteroids updated</title>
	<date>June 5th, 2005</date>
	<fullstory>
		<p>
			I have updated the orbital elements for Comets and Asteroids, which
			you can install directly from the "Get New Stuff" tool in KStars 
			("File | Download Data..." or Ctrl+D).  I have removed 
			<a href="http://en.wikipedia.org/wiki/Comet_Shoemaker-Levy_9">comet 
			Shoemaker-Levy 9</a> from the database, since it crashed into 
			Jupiter in 1994.
		</p>
		<br />
	</fullstory>
</item>

<item>
<title>KStars 1.1-p1: Important fix for KDE 3.3.x users!</title>
<date>March 29th, 2005</date>
<fullstory>
<p>
Our recent release of KStars-1.1 contained some new KDE-3.4 code 
that is incompatible with KDE-3.3.  I have now fixed the tarballs,
so KStars-1.1-p1 is now 
<a href="http://edu.kde.org/kstars/index.php#dl">available</a>, 
and is usable with kde-3.3.x.  Sorry about that.  
</p>
<p>
If you have already downloaded KStars-1.1, you can download 
<a href="http://edu.kde.org/kstars/kstars-1.1.patch">this patch</a>
to get the fix.  Simply place the patch file into your 
<tt>kstars-1.1</tt> directory, and enter the command 
<tt>patch -p0 &lt; kstars-1.1.patch</tt>.  
</p>
<br />
</fullstory>
</item>
    
<item>
<title>KStars 1.1 available for download</title>
<date>March 27th, 2005</date>
<fullstory>
<p>
I have packaged a tarball based on the KDE_3_4_BRANCH codebase, which
you can now <a href="http://edu.kde.org/kstars/index.php#dl">download</a>.
This code is essentially identical to the version of kstars included in 
kdeedu-3.4.0 (with a few minor additional bugfixes).  Again, these 
releases are intended for users who choose not to upgrade KDE to 3.4.0, 
but would still like to have the latest KStars code.  Enjoy!
</p>
<br />
</fullstory>
</item>
    
<item>
<title>New Features Tour for KDE 3.4 release</title>
<date>March 22th, 2005</date>
<fullstory>
<p>
KDE 3.4.0 was <a href="http://www.kde.org/announcements/announce-3.4.php">released</a> 
recently, so that must mean it's time for a 
<a href="kstars-3.4-features.php">New Features tour</a>!  
I will also put together a tarball corresponding to the 
3.4.0 codebase, hopefully soon.
</p>
<br />
</fullstory>
</item>
    
<item>
<title>Join the KStars Data Team!</title>
<date>March 10th, 2005</date>
<fullstory>
<p>
Are you an avid KStars user looking for a fun way to contribute to the project?
Afraid we are only looking for expert code hackers?  Well, fear no longer.
I'd like to announce the KStars Data Team.  The Data Team will be responsible 
for maintaining and improving all of the data related to KStars:
</p>
<ul>
<li>Fill in missing data in our object catalogs</li>
<li>Add names to unnamed stars and deep-sky objects</li>
<li>Expand the list of internet links available in the popup menu</li>
<li>Construct outline curves for large nebulae, to be displayed in the map</li>
<li>Keep the comet/asteroid ephemerides up-to-date</li>
<li>Improve the object images displayed on the sky</li>
<li>Collect default Thumbnail images for the new Details window (see last news item)</li>
</ul>
<p>
I've been thinking of trying something like this for a while, but the last item 
especially convinced me to go ahead.  The new Details window makes it pretty fun to search 
for thumbnail images, and having a default set for the Messier catalog, the major planets, 
and some other popular objects would be a very nice addition to the program.
</p>
<p>
So if you think you're interested in joining the Data Team, introduce yourself on our 
mailing list (kstars-devel AT kde DOT org), and let us know how you'd like to contribute.
Thanks!
</p>
<br />
</fullstory>
</item>

<item>
<title>New Details Window</title>
<date>March 10th, 2005</date>
<fullstory>
<p>
KStars has a new Details window:<br />
<a href="new_details.png"><img alt="new details window" src="new_details_small.png"/></a>
</p>
<p>
The layout is much cleaner, and all data text can now be copy/pasted with the mouse.
Best of all, the window now includes a Thumbnail image, which can be customized:<br />
<a href="thumb_chooser.png"><img alt="thumbnail image selector" src="thumb_chooser_small.png"/></a>
</p>
<p>
The list of images is populated from the object's internal list of Image URLs (as 
shown in the popup menu) and by performing a Google Image search on the object's name.
</p>
<p>
If you'd like to check it out, you have to <a href="svn.php">install the latest CVS 
code</a>.
</p>
</fullstory>
</item>

<item>
<title>KStars wins QtForum.org contest!</title>
<date>February 2nd, 2005</date>
<fullstory>
<p>
KStars has earned first prize in a 
<a href="http://www.qtforum.org/thread.php?postid=21864">contest</a> for the best 
Educational Software using Qt libraries!  <a href="http://edu.kde.org/kig">Kig</a> 
(another <a href="http://edu.kde.org">KDE-Edu</a> program) won second prize.  The 
contest judges had this to say about KStars:  "KStars gets first place 
because it is a stellar (no pun intended) ambassador of Linux, KDE, and 
Qt to the world, with a `wow factor' that conveys in a glance the power 
and relevance of this platform to modern tasks of any scope".
</p>
<p>
See the article at <a href="http://dot.kde.org/">the Dot</a> for more details, 
and congratulations to the whole KStars team!
</p>
</fullstory>
</item>

<item>
<title>Comets and Asteroids Updated!</title>
<date>January 4th, 2005</date>
<fullstory>
<p>
After numerous requests from eager hunters of 
<a href="http://encke.jpl.nasa.gov/">C/Machholz (2004 Q2)</a>, 
I have finally updated the orbital elements for comets and asteroids
in KStars!  If you have KDE 3.3, just start KStars and press Ctrl+D
to open the <b>Get New Stuff</b> tool.  Highlight the "ephemerides"
package, and click Install.  
</p>
<p>
If you have an older version of KStars, you can still manually 
download the <a href="downloads/ephemerides-2005.01.tar.bz2">new 
package</a>.  Just unpack it into your 
<tt>~/.kde/share/apps/kstars/</tt> directory.
</p>
<p>
I'd also like to make it known that 
<a href="http://webcvs.kde.org/kdeedu/kstars/README.ephemerides?rev=1.1">it 
is possible to update the orbital elements yourself</a>, if you 
find that I am taking too long to do it :)
</p>
</fullstory>
</item>

<item>
<title>KStars 3.3 Snapshot Release</title>
<date>October 31th, 2004</date>
<fullstory>
<p>
The KStars team is pleased to announce a snapshot release of 
KStars from the KDE-3.3 codebase.  This release is essentially 
identical to the code included in kdeedu-3.3.1, so if you have 
that module installed there is no need to download this release.  
The release is targeted at users who are unwilling or unable to 
upgrade to KDE-3.3 at this time, but would still like to use the 
latest version of KStars.
</p>
<p>
You can take a  
<a href="http://edu.kde.org/kstars/kstars-3.3-features.php">tour 
of the new features in 3.3</a>, or <a href="#dl">download</a> 
the 3.3 release tarball.
</p>
</fullstory>
</item>

<item>
<title>New KStars Review</title>
<date>October 12th, 2004</date>
<fullstory>
<p>
Marcel Gagn&#233; has written
<a href="http://www.linuxjournal.com/article.php?sid=7728">this
article</a> in <a href="http://www.linuxjournal.com">LinuxJournal</a>,
in which he reviews three astronomy-related apps:
<a href="http://lightspeed.sourceforge.net/">Light Speed!</a>,
<a href="http://www.shatters.net/celestia/">Celestia</a>, and our own
KStars.  Here's a sample:
</p>

<blockquote>
KStars is amazing fun but much more than a toy. With a database of
the planets, 130,000 stars, 13,000 deep-sky objects, the planets and
many asteroids, KStars is an astronomical treasure.
</blockquote>
<p>
Thanks, Marcel!  We've had some <a href="buzz.php">other reviews</a>
over the years, too.
</p>
</fullstory>
</item>

<item>
<title>KStars Scripts Archive</title>
<date>September 27th, 2004</date>
<fullstory>
<p>
I recently gave a short presentation about the night sky to about 50
grade school students and some parents, using
<a href="http://edu.kde.org/kstars">KStars</a> and
<a href="http://www.shatters.net/celestia/">Celestia</a> as demo engines.
It went very well, the kids were really fascinated, and asked many
questions.
</p>
<p>
To prepare the presentation, I used the
<a href="http://docs.kde.org/en/3.3/kdeedu/kstars/tool-scriptbuilder.html">scripting features</a>
of KStars to set up several scenarios, each designed to illustrate some
particular aspect of how the night sky works.  I have made these scripts
available for download in a new <a href="scripts/index.php">Scripts
Archive</a> section of the website.
</p>
<p>
I invite everyone to contribute any KStars scripts that they may have
written.  Just <a href="mailto:kstars AT 30doradus DOT org">email me</a>
the script along with a brief description of what it does, and I will
add it to the Scripts Archive.
</p>
</fullstory>
</item>

<item>
<title>New Features Tour</title>
<date>August 20th, 2004</date>
<fullstory>
<p>
<a href="http://www.kde.org/announcements/announce-3.3.php">KDE
3.3 is released</a>, and it includes a new version of KStars.
If you'd like to see what we've been up to since 3.2, I have put
together a brief <a href="kstars-3.3-features.php">tour of the
new features</a>.
</p>
</fullstory>
</item>

<item>
<title>Venus Transits the Sun Tomorrow</title>
<date>June 7th, 2004</date>
<fullstory>
<table>
<tr>
<td valign="top">
<p>
On 8 June 2004, Venus will transit (or pass in front of) the Sun.
You can use KStars to plan observations of this historic event.
At right is a screenshot showing the Venus transit in progress from
Greenwich, UK at 08:27 UT.
</p>
<p>
(WARNING!  <b>Never</b> look directly at the Sun!  Use a
<a href="http://sunearth1.gsfc.nasa.gov:80/sunearthday/2004/vt_viewing_pinhole_2004.htm">simple
pinhole-camera projector</a>, or some kind of
<a href="http://sunearth1.gsfc.nasa.gov:80/sunearthday/2004/vt_viewing_protection_2004.htm">solar
filter</a>.  Newton almost blinded himself by observing the Sun.
This is your opportunity to be smarter than Newton!)
</p>
</td>
<td valign="top"><img alt="Venus transits the sun in KStars" src="venus_transit.png"/></td>
</tr>
</table>
</fullstory>
</item>

<item>
<title>Spring Comets</title>
<date>April 21th, 2004</date>
<fullstory>
<p>
There are two comets that may be visible to the unaided eye this spring,
and both of them are included in KStars.  Both comets are on extremely
long-period orbits, so this is their first known trip to the inner solar
system.  That means there's no telling if they will become very bright
or not.
</p>
<p>
The first comet is named "LINEAR (2002 T7)".  It reaches perihelion
(closest approach to the Sun) on 23 April, and should continue to
brighten in the days following.  LINEAR (2002 T7) will be visible in
the pre-dawn hours of late April and early May.
</p>
<p>
The other comet is named "NEAT (2001 Q4)", and while it's still early
for firm predictions, many people seem to think it has a better chance
of putting on a good show for us Earthlings.  By mid-May, NEAT (2001
Q4) will be high in the sky after sunset.
</p>
<p>
<b>UPDATE:</b>  It looks like neither comet is going to be bright enough
to see without binoculars.  My friend Adam Block, who runs the
<a href="http://www.noao.edu/outreach/aop/">Advanced Observing Program</a>
at the <a href="http://www.noao.edu/kpno/">Kitt Peak National Observatory</a>,
took <a href="http://www.noao.edu/outreach/aop/observers/c-2002t7.html">these
images</a> of comet LINEAR (2002 T7).  I myself have been observing NEAT
(2001 Q4); it's an easy target with KStars and a pair of binoculars :).
</p>
<p>
Here are two screenshots from KStars showing each comet around the time
they are expected to peak:
</p>
<p>
<table>
<TR>
<TH>Comet LINEAR (2002 T7)<br />05:00 01 May 2004</TH>
<TH>Comet NEAT (2001 Q4)<br />21:00 15 May 2004</TH>
</TR>
<TR>
	<TD><a href="screens/com_linear.png"><img border="0" alt="comet LINEAR (2002 T7)" src="screens/com_linear_thumb.png"></a></TD>
	<TD><a href="screens/com_neat.png"><img border="0" alt="comet NEAT (2001 Q4)" src="screens/com_neat_thumb.png"></TD>
</TR>
</table>
</p>
<p>
<small>
Note: these images show the sky from Tucson, AZ, USA.  You should run KStars 
to find where these objects will be from your location
</small>
</p>
</fullstory>
</item>

<item>
<title>Add Sedna to KStars</title>
<date>April 14, 2004</date>
<fullstory>
<p>
You may have read about
<a href="http://www.space.com/scienceastronomy/new_object_040315.html">the
recent discovery of Sedna</a>, a minor planet that is nearly as big 
as Pluto, but three times further away from the Sun.  I have just 
added Sedna to KStars in CVS, but if you like, you can add it to your
own copy of KStars now.  Just add the following line to the installed 
asteroids.dat file (you may need root priveliges to do so):
</p>
<p>
<code>
Sedna             53200 531.657634 0.85743380  11.93041 311.82711 144.49288 357.8814700  1.70 0.00
</code>
</p>
</fullstory>
</item>

<item>
<title>Updated images_url.dat File</title>
<date>April 2nd, 2004</date>
<fullstory>
<p>
Recently, NASA reorganized the Hubble Space Telescope website,
which broke many of the URL links accessible from the
right-click popup menu.  We have fixed the links on CVS, and
the fixed links will be in KDE 3.2.2 (expected next week).
</p>
<p>
However, if you do not wish to upgrade all of KStars, you
can simply <a href="image_url.dat">download the fixed
image_url.dat file</a>.  You can install it
globally (as root) in your $KDEDIR/share/apps/kstars/
directory.  Or, you can place it in your local user's
$KDEHOME/share/apps/kstars/ directory ($KDEHOME is typically
"~/.kde" or "~/.kde3").
</p>
</fullstory>
</item>

</channel>
</rss>
