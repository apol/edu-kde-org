<?php
	$site_root = "../";
	$page_title = 'KTouch KDE 4 - Lecture files';

	include ( "header.inc" );
?>

<h3>Contributed lecture files for KTouch</h3>
<p>Below is a list of lecture files contributed by users. The lecture files are provided as they are without further checking. Please contact the respective authors for questions and/or comments directly.
</p>

<h3>How to create proper lecture files for KTouch</h3>
<p>Technically, creating lecture files is easy with KTouch. However, there are some considerations to keep in mind for creating <b>good</b> lecture files. Below is also a description about <i>special</i> training files (newspaper articles etc.).
<h4>General concepts</h4>
<p>
Training files come in two kinds. The 'old" ones were simple text files, the current lecture files are in XML format. Also, most of the old files were ASCII encoded, while all new files are UTF-8 encoded. In any case, you can edit the files directly in a text editor. The old format is still recognized in the lecture editor of KTouch (which can be used to convert the files).
</p>
<p>
KTouch expects a certain structure of a lecture file. A good touch-typing course will consist of several lessons with a good mixture of new keys to learn and some practice text for the previously learned keys. Basically, each lecture/course is structured in different levels/lessons. Each lessen typically introduces a few new keys on the keyboard (2 to 4 keys). The order in which keys are introduced depends a bit on the lecture, but follows roughly the following order:
<ol><li>first the keys where the fingers rest on (middle row)</li>
<li>then the center keys (middle row), and/or some easy-to-press keys in the top or bottom row</li>
<li>gradually add keys based on their usage in the respective language, for languages which rely on alternate keys introduce SHIFT keys early, also introduce . and , to allow for full sentence typing</li>
<li>last are the numbers and special characters</li>
</ol>
A lesson can have several lines (not too many, 8-10 max). The first few lines should just use the new keys and then in the following lines the new keys should be mixed with previously introduced keys. The complexity should increase and (if possible) words of the choosen language should be used. So at least after level 8..9 it should be possible to offer the user small (and preferably typical) phrases to practise.

<h4>Notes on the XML data in lecture files</h4>
The new XML format has a fairly simple structure. The new characters property is used to highlight the known keys on the keyboard. The program collects all characters in the new characters property of the current and <i>all</i> previous levels and highlights these keys on the keyboard. The description line is shown as level/lesson description. They can be the same, but mind that the new characters property has a special function!

<h4>The training file generators</h4>
Haavard wrote a number of small programs which can extract the content of an ASPELL dictionary and create lecture files from them. Take a look at the following files:<br>
<a href="lectures/generator_perl.tar.gz">PERL version of the generator</a><br>
<a href="lectures/generator_c.tar.gz">C version of the generator</a><br>
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
	include "footer.inc";
?>
