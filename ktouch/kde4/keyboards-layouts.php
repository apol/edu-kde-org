<?php
  $site_root = "../";
  $page_title = 'KTouch KDE 4 - Keyboard Layouts';
  
  include ( "header.inc" );
  ?>

 <h3>Keyboard layouts in KTouch</h3>
 KTouch comes with a number of pre-installed keyboard. You can simply select a keyboard from the list in the main menu that matches your own keyboard layout. If you can't find a matching keyboard layout, try one in the list below or <a href="#create">create your own keyboard layout</a>.

 <h3>Contributed keyboard layouts</h3>
 <p>
The keyboard layouts provided below were created by users of KTouch. We provide them without further checking or testing, so please direct all questions to the respective authors.
 <p>


 <h3><a name="create">&nbsp;</a>How to port an old .keyboard file to a new .keyboard.xml one</h3>
<ul>
  <li>File menu, Edit Keyboard Layout...</li>
  <li>Choose "Open a keyboard file:"<br/>
If you have kde4 sources, look in the <tt>kdeedu/ktouch/keyboards</tt> folder and get a .keyboard file. If not, get a .keyboard file from <a href="http://websvn.kde.org/trunk/KDE/kdeedu/ktouch/keyboards/">websvn</a>.<br />
The .keyboard.xml files are the ones already converted. See <a href="ktouch1.png">ktouch1.png</a></li>
  <li>The keyboard editor will display <a href="ktouch2.png">the layout</a>. First change the Keyboard title (top left) and write the name of the Language in the <a href="ktouch3.png">Keyboard Identification Data area</a></li>
  <li>Those keys are the fingers keys <a href="ktouch4.png">ktouch4.png</a>, all keys should be associated with one of these.</li>
<li>Presumably you will improve an old .keyboard file so click on each key and check if the information for it is right.</li>
  <li>Let's say we want to add the Euro symbol on the E key on the French Swiss layout. Click on the E key in the editor, it gets highlighted in blue. The <a href="ktouch6.png">Key Properties dialog</a> has the different symbols on the current key. The € symbol is bottom right so ket's add it there.</li>
  <li>Now you have to specify how the Euro symbol is accessed from the key (E key + Alt Gr). This is done in the Key connectors section, see <a href="ktouch7.png">ktouch7.png</a>. Type € in the character field then click on "Modifier Key", choose Alt-Gr in the displayed keyboard then click Add/update connector. The new connector appears in the connector table.</li>
  <li>Save your keyboard layout using the Save Keyboard As option, see <a href="ktouch5.png">ktouch5.png</a></li>
  <li>If you have commit access you can commit it. Otherwise please send the file to <a href="mailto:annma AT kde DOT org">me</a>.</li>
</ul>
<p>
 <a href="http://lists.kde.org/?l=kde-edu&amp;m=119085544623900&amp;w=2">More explanations</a> on this mail.
</p>
 <p>
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>
