<?php
  include_once("../site_includes/applicationpage.inc");

  $site_root = "../";
  $page_title = "KGeography";
  
  include("header.inc");
  
  $appinfo = new AppInfo( "KGeography" );
  $appinfo->setIcon( "../images/icons/kgeography_32.png", "32", "32" );
  $appinfo->setVersion( "0.7" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2004", "Albert Astals Cid" );
  $appinfo->addAuthor( "Albert Astals Cid", "aacid@kde.org" );
  $appinfo->show();

    // for now only use the auto-generated information
    $app = new EduAppData("kgeography");
    printPage($app);
?>

<br />
<p>To get detailed information about KGeography please visit <a href="http://userbase.kde.org/KGeography">KGeography on UserBase...</a></p>


<?php
  include("footer.inc");
?>

