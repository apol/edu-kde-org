<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

include("site_includes/class_handler404_edu.inc");

$handler = new Handler404_Edu();

$handler->add("/doc", "/development/document.php");
$handler->add("/development/debug.php", "http://techbase.kde.org/Development/Tools");
$handler->add("/development/port2kde4.php", "http://techbase.kde.org/Development/Tutorials/KDE4_Porting_Guide");
$handler->add("/development/ghns.php", "http://techbase.kde.org/Development/Tutorials/Introduction_to_Get_Hot_New_Stuff");
$handler->add("/development/knewstuff.php", "http://techbase.kde.org/Development/Tutorials/Introduction_to_Get_Hot_New_Stuff");

$handler->addDir("/parley/*", "/applications/all/parley");
$handler->addDir("/myprog", "/applications");

$handler->execute();
?>
