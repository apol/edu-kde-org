<?php
  $site_root = "../";
  $page_title = 'Contributed graphics';
  
  include ( "header.inc" );
?>

 <p><a href="mailto:e_achiam AT netvision DOT net DOT il">Elhay Achiam</a> has started to create some graphics that are freely available
 for anyone to use. They are:</p>

<p>
	  <b>Animals</b> <br />
	  <a href="graphics/cable/duck.jpg"><img src="graphics/cable/duck_thumb.jpg" alt="" border="0" /></a>
	  <a href="graphics/cable/fish.jpg"><img src="graphics/cable/fish_thumb.jpg" alt="" border="0" /></a>
	  <a href="graphics/cable/penguin.jpg"><img src="graphics/cable/penguin_thumb.jpg" alt="" border="0" /></a>
	  <a href="graphics/cable/frog.jpg"><img src="graphics/cable/frog_thumb.jpg" border="0" alt="" /></a>
	  <a href="graphics/cable/squirrel.jpg"><img src="graphics/cable/squirrel_thumb.jpg" alt="" border="0" /></a>
	  <a href="graphics/cable/cat.jpg"><img src="graphics/cable/cat_thumb.jpg" border="0" alt="" /></a>
	  <a href="graphics/cable/lion.png"><img src="graphics/cable/lion_thumb.png" border="0" alt="" /></a>
	  <a href="graphics/cable/stork.png"><img src="graphics/cable/stork_thumb.png" border="0" alt="" /></a>
</p>

<p>
	<b>Cartoons</b> <br />
	<a href="graphics/cable/jinji.jpg"><img src="graphics/cable/jinji_thumb.jpg" alt="" border="0" /></a>
	<a href="graphics/cable/host.gif"><img src="graphics/cable/host_thumb.gif" alt="" border="0" /></a>
</p>
<p>
	<b>Fantasy creatures</b> <br />
	<a href="graphics/cable/dragon.png"><img src="graphics/cable/dragon_thumb.png" alt="" border="0" /></a>
 </p>

<p>Please click on one of the images to show the full sized version.</p>
	<hr />

<p><a href="mailto:zerokode AT yahoo DOT com">Primoz</a> created some icons and sreens for the applications:</p>

<p>
	<b>Icons</b> <br />
	<img src="graphics/primoz/icons/menu_48x48_hi.png" alt="" border="0" />
	<img src="graphics/primoz/education.png" alt="" width="91" height="47" border="0" />
</p>

<p>
	<b>Screens</b> <br />
	<a href="graphics/primoz/edulogoprpl.jpg"><img src="graphics/primoz/edulogoprpl_thumb.jpg" alt="" border="0" /></a>
	<a href="graphics/primoz/splash_top.png"><img src="graphics/primoz/splash_top_thumb.png" alt="" border="0" /></a>
</p>

<p>
	<b>Animals</b> <br />
            <a href="graphics/primoz/funnywhale.jpg"><img src="graphics/primoz/funnywhale_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/primoz/dino.jpg"><img src="graphics/primoz/dino_thumb.jpg" alt="" border="0" /></a>
</p>

<p>Please click on one of the images to show the full sized version.</p>
        <hr />

<p><a href="mailto:kisukuma AT chez DOT com">Renaud Blanchard </a>created some
pictures using Blender and the Gimp. He is willing to draw icons, animated
pictures and 3D pictures so if you need some fancy drawing in your
application, just ask him! He also proposes to create music samples. Here
are samples of what he can do:</p>
<p>
	<b>Miscelleanous</b> <br />
            <a href="graphics/renaud/kumahabitdecor.jpg"><img src="graphics/renaud/kumahabitdecor_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/kumahabitdecor11.jpg"><img src="graphics/renaud/kumahabitdecor11_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/kumahabitdecor111.jpg"><img src="graphics/renaud/kumahabitdecor111_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/ping.jpg"><img src="graphics/renaud/ping_thumb.jpg" alt="" border="0" /></a>
</p>
<p>
	<b>Single object pictures</b> <br />
            <a href="graphics/renaud/ampoule.jpg"><img src="graphics/renaud/ampoule_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/argent.jpg"><img src="graphics/renaud/argent_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/bague.jpg"><img src="graphics/renaud/bague_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/baguettes.jpg"><img src="graphics/renaud/baguettes_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/bol.jpg"><img src="graphics/renaud/bol_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/cd.jpg"><img src="graphics/renaud/cd_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/crayon.jpg"><img src="graphics/renaud/crayon_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/disquette.jpg"><img src="graphics/renaud/disquette_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/fleche.jpg"><img src="graphics/renaud/fleche_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/fleches.jpg"><img src="graphics/renaud/fleches_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/livre.jpg"><img src="graphics/renaud/livre_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/montre.jpg"><img src="graphics/renaud/montre_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/mug.jpg"><img src="graphics/renaud/mug_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/poste.jpg"><img src="graphics/renaud/poste_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/table.jpg"><img src="graphics/renaud/table_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/telephone.jpg"><img src="graphics/renaud/telephone_thumb.jpg" alt="" border="0" /></a>
            <a href="graphics/renaud/tv.jpg"><img src="graphics/renaud/tv_thumb.jpg" alt="" border="0" /></a>
</p>


<?php
  include "footer.inc";
?>










