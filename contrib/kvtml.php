<?php
  $translation_file = "edu-kde-org";
  include_once( "functions.inc" );
  $site_root = "/applications/language/";
  $page_title = i18n_noop( "Contributed Learning Files" );

  include ( "header.inc" );
?>

<p><?php echo i18n_var( 'The learning files listed here are intended for use with %1 and %2 which are both part of the KDE Edutainment project. To some extent these files also work with %3 and %4.', '<a href="/applications/language/parley/">Parley</a>', '<a href="/applications/language/kwordquiz/">KWordQuiz</a>', '<a href="/applications/language/khangman/">KHangMan</a>', '<a href="/applications/language/kanagram/">Kanagram</a>' ); ?></p>

<h2><?php i18n( "Creating kvtml Files" ); ?></h2>

<ul>
  <li><?php echo i18n_var( "You can create a kvtml file using %1. You add the back and front for each cards and then you save your data. It will be saved using the kvtml extension.", '<a href="/applications/language/kwordquiz/">KWordQuiz</a>' ); ?></li>
  <li><?php echo i18n_var( "If you are on KDE4 already consider using %1. Parley offers a wealth of editing possibilities.", '<a href="/applications/language/parley/">Parley</a>' ); ?></li>
  <li><?php echo i18n_var( 'Please see <a href="http://files.kde.org/edu/kvtml/sample.txt">here</a> for an example of a kvtml file if you want to create one with an editor.' ); ?></li>
</ul>

<h2><?php i18n( "Share Your Files" ); ?></h2>
<p><?php echo i18n_var( "Please upload files you created to %1. They will appear on the page and also show up in the download dialogs of the applications. The files can be found here:", '<a href="https://store.kde.org/">https://store.kde.org/</a>' ); ?></p>

<ul>
  <li><a href="https://store.kde.org/browse/cat/216/"><?php i18n( "Parley files on store.kde.org" ); ?></a></li>
  <li><a href="https://store.kde.org/browse/cat/337/"><?php i18n( "KWordQuiz files on store.kde.org" ); ?></a></li>
  <li><a href="https://store.kde.org/browse/cat/218/"><?php i18n( "KHangMan files on store.kde.org" ); ?></a></li>
</ul>

<p><?php echo i18n_var( 'If you have questions or need help, ask the <a href="mailto:kde-edu@kde.org">the KDE-Edu mailing list</a>.' ); ?></p>

<h2><?php i18n( "Areas" ); ?>:</h2>
<ul>
  <li><a href="#vocab"><?php i18n( "Vocabulary / Languages" ); ?></a></li>
  <li><a href="#anatomy"><?php i18n( "Anatomy / Physiology" ); ?></a><br/>
    <ul>
      <li><a href="#joints"><?php i18n( "Joints" ); ?></a></li>
      <li><a href="#bones"><?php i18n( "Bones" ); ?></a></li>
      <li><a href="#muscles"><?php i18n( "Muscles" ); ?></a></li>
      <li><a href="#ligamenti"><?php i18n( "Ligamenti" ); ?></a></li>
      <li><a href="#anamisc"><?php i18n( "Miscellaneous" ); ?></a></li>
    </ul>
  </li>
  <li><a href="#music"><?php i18n( "Music" ); ?></a><br/>
    <ul>
      <li><a href="#guitar"><?php i18n( "Guitar" ); ?></a></li>
      <li><a href="#theory"><?php i18n( "Music Theory" ); ?></a></li>
    </ul>
  </li>
  <li><a href="#geography"><?php i18n( "Geography" ); ?></a></li>
  <li><a href="#history"><?php i18n( "History" ); ?></a></li>
 <li><a href="#math"><?php i18n( "Math" ); ?></a></li>
<li><a href="#chemistry"><?php i18n( "Chemistry" ); ?></a></li>
  <li><a href="#exam"><?php i18n( "Exam Preparation" ); ?></a></li>
</ul>


<a name="vocab"></a>
<h2><?php i18n( "Vocabulary / Languages" ); ?></h2>

 <!-- language_1+2   filename   description   advanced_features -->

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" />&nbsp;&nbsp;<img src="flags/latin.png" alt="latin" /></td>
   <td>Tarball of German to Latin vocabulary files</td>
   <td><a href="http://files.kde.org/edu/kvtml/dela.tar.bz2">dela.tar.bz2</a></td>
   <td>alphabetical order from German words</td>
   <td>2005-03-21</td>
  </tr>

   <tr>
   <td><img src="flags/latin.png" alt="latin" />&nbsp;&nbsp;<img src="flags/de.png" alt="de" /></td>
   <td>Tarball of Latin to German vocabulary files</td>
   <td><a href="http://files.kde.org/edu/kvtml/lade.tar.bz2">lade.tar.bz2</a></td>
   <td>alphabetical order from Latin words</td>
   <td>2005-03-21</td>
  </tr>

   <tr>
   <td><img src="flags/de.png" alt="de" />&nbsp;&nbsp;<img src="flags/gb.png" alt="gb" /></td>
   <td>Basic words English/German</td>
   <td><a href="http://files.kde.org/edu/kvtml/basic_vocabulary_en2de.kvtml">basic_vocabulary_en2de.kvtml</a></td>
   <td>1332 entries in 47 lessons</td>
   <td>2005-12-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" />&nbsp;&nbsp;<img src="flags/gb.png" alt="gb" /></td>
   <td>Edgar Allen Poe: The Raven</td>
   <td><a href="http://files.kde.org/edu/kvtml/the_raven.kvtml">the_raven.kvtml</a></td>
   <td>-</td>
   <td>-</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" />&nbsp;&nbsp;<img src="flags/gb.png" alt="gb" /></td>
   <td>Intended for german pupils: Vorbereitung Abitur Englisch</td>
   <td><a href="http://files.kde.org/edu/kvtml/En.AbiVorb.Vokabeln.kvtml">En.AbiVorb.Vokabeln.kvtml</a></td>
   <td>Some remarks, verbs and adjectives</td>
   <td>-</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" />&nbsp;&nbsp;<img src="flags/es.png" alt="es" /></td>
   <td>General words German/Spanish</td>
   <td><a href="http://files.kde.org/edu/kvtml/spanisch.kvtml">spanisch.kvtml</a></td>
   <td>Some conjugation</td>
   <td>-</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" />&nbsp;&nbsp;<img src="flags/es.png" alt="es" /></td>
   <td>A collection of German/Spanish vocabularies</td>
   <td><a href="http://files.kde.org/edu/kvtml/tests_deutsch_spanisch.tar.bz2">tests_deutsch_spanisch.tar.bz2</a></td>
   <td></td>
   <td>2008-02-07</td>
  </tr>

  <tr>
   <td><img src="flags/gb.png" alt="gb" />&nbsp;&nbsp;<img src="flags/cz.png" alt="cz" /></td>
   <td>Basic words English/Czech</td>
   <td><a href="http://files.kde.org/edu/kvtml/basic_english_czech.kvtml">basic_english_czech.kvtml</a></td>
   <td>-</td>
   <td>2002-05-26</td>
  </tr>

  <tr>
   <td><img src="flags/nl.png" alt="nl" />&nbsp;&nbsp;<img src="flags/gb.png" alt="gb" /></td>
   <td>Basic words English/Dutch</td>
   <td><a href="http://files.kde.org/edu/kvtml/nederlands.kvtml">nederlands.kvtml</a></td>
   <td>-</td>
   <td>2006-09-29</td>
  </tr>

  <tr>
   <td><img src="flags/gb.png" alt="gb" />&nbsp;&nbsp;<img src="flags/fr.png" alt="fr" /></td>
   <td>Irregular verbs, English/French</td>
   <td><a href="http://files.kde.org/edu/kvtml/eng-fra_irregular_verbs.kvtml">eng-fra_irregular_verbs.kvtml</a></td>
   <td>Includes conjugations</td>
   <td>2002-10-15</td>
  </tr>


  <tr>
   <td><img src="flags/gb.png" alt="gb" />&nbsp;&nbsp;<img src="flags/fr.png" alt="fr" /></td>
   <td>Words of daily use, English/French</td>
   <td><a href="http://files.kde.org/edu/kvtml/eng-fra_daily_words.kvtml">eng-fra_daily_words.kvtml</a></td>
   <td>Includes conjugations</td>
   <td>2002-10-24</td>
  </tr>


  <tr>
   <td><img src="flags/gb.png" alt="gb" />&nbsp;&nbsp;<img src="flags/jp.png" alt="jp" /></td>
   <td>Basic words English/Japanese</td>
   <td><a href="http://files.kde.org/edu/kvtml/japanese5-6.kvtml">japanese5-6.kvtml</a><br/>
       <a href="http://files.kde.org/edu/kvtml/japanese.kvtml">japanese.kvtml</a><br/>
       <a href="http://files.kde.org/edu/kvtml/japanese10.kvtml">japanese10.kvtml</a><br/>
       <a href="http://files.kde.org/edu/kvtml/japanese11.kvtml">japanese11.kvtml</a><br/>
       <a href="http://files.kde.org/edu/kvtml/japanese12.kvtml">japanese12.kvtml</a><br/>
       <a href="http://files.kde.org/edu/kvtml/japanese7.kvtml">japanese7.kvtml</a><br/>
       <a href="http://files.kde.org/edu/kvtml/japanese8.kvtml">japanese8.kvtml</a><br/>
       <a href="http://files.kde.org/edu/kvtml/japanese9.kvtml">japanese9.kvtml</a><br/>
       <a href="http://files.kde.org/edu/kvtml/jpn_club_a.kvtml">jpn_club_a.kvtml</a><br/>
       <a href="http://files.kde.org/edu/kvtml/jpn_expr_a.kvtml">jpn_expr_a.kvtml</a>
   </td>
   <td>Needs japanese fonts</td>
   <td>2002-12-05</td>
  </tr>

  <tr>
   <td><img src="flags/jp.png" alt="jp" /></td>
   <td>Help for learning Katakana alphabet</td>
   <td><a href="http://files.kde.org/edu/kvtml/katakana.kvtml">katakana.kvtml</a></td>
   <td>Needs japanese fonts</td>
   <td>2002-09-04</td>
  </tr>

  <tr>
   <td><img src="flags/jp.png" alt="jp" /></td>
   <td>Help for learning Hirigana alphabet</td>
   <td><a href="http://files.kde.org/edu/kvtml/hirigana.kvtml">hirigana.kvtml</a></td>
   <td>Needs japanese fonts</td>
   <td>2004-02-22</td>
  </tr>

  <tr>
   <td><img src="flags/gb.png" alt="gb" />&nbsp;&nbsp;<img src="flags/kr.png" alt="kr" /></td>
   <td>Basic English/Korean vocabulary</td>
   <td><a href="http://files.kde.org/edu/kvtml/korean_lesson1.kvtml">korean_lesson1.kvtml</a><br />
       <a href="http://files.kde.org/edu/kvtml/korean_lesson2.kvtml">korean_lesson2.kvtml</a>
   </td>
   <td>Needs Unicode font</td>
   <td>2006-08-19</td>
  </tr>

  <tr>
   <td><img src="flags/no.png" alt="no" /></td>
   <td>Strong verbs used in Norwegian Nynorsk for KVocTrain</td>
   <td><a href="http://files.kde.org/edu/kvtml/sterke_verb_(nynorsk).kvtml">sterke_verb_(nynorsk).kvtml</a></td>
   <td>Conjugation</td>
   <td>2005-08-17</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de"/>&nbsp;&nbsp;<img src="flags/eo.png" alt="eo" /></td>
   <td>German - Esperanto words</td>
   <td><a href="http://files.kde.org/edu/kvtml/vortaro-eo-de.kvtml">vortaro-eo-de.kvtml</a></td>
   <td>278 words</td>
   <td>2006-02-12</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de"/>&nbsp;&nbsp;<img src="flags/eo.png" alt="eo" /></td>
   <td>German - Esperanto words</td>
   <td><a href="http://files.kde.org/edu/kvtml/vortoj-de-eo.kvtml">vortoj-de-eo.kvtml</a></td>
   <td>163 words</td>
   <td>2008-09-19</td>
  </tr>

  <tr>
   <td><img src="flags/pl.png" alt="pl" />&nbsp;&nbsp;<img src="flags/de.png" alt="de" /></td>
   <td>Polish - German adjectives</td>
   <td><a href="http://files.kde.org/edu/kvtml/pl_de_adjective1.kvtml">pl_de_adjective1.kvtml</a></td>
   <td>-</td>
   <td>2006-03-24</td>
  </tr>

  <tr>
   <td><img src="flags/pl.png" alt="pl" />&nbsp;&nbsp;<img src="flags/de.png" alt="de" /></td>
   <td>Polish - German verbs. - to train Präteritum and Partizip II</td>
   <td><a href="http://files.kde.org/edu/kvtml/pl_de_verb1.kvtml">pl_de_verb1.kvtml</a></td>
   <td>-</td>
   <td>2006-03-24</td>
  </tr>
  <tr>
   <td><img src="flags/pl.png" alt="pl" />&nbsp;&nbsp;<img src="flags/fr.png" alt="fr" /></td>
   <td>Polish - French basic vocabulary</td>
   <td><a href="http://files.kde.org/edu/kvtml/pl_fr_basic1.kvtml">pl_fr_basic1.kvtml</a></td>
   <td>-</td>
   <td>2006-03-24</td>
  </tr>
  <tr>
  <td><img src="flags/us.png" alt="us" />&nbsp;&nbsp;<img
src="flags/lojban.png" alt="lojban" /></td>
  <td>English - Lojban vocabulary</td>
  <td><a href="http://files.kde.org/edu/kvtml/en2lojban.kvtml">
en2lojban.kvtml</a></td>
 <td>-</td>
  <td>2006-09-18</td>
  </tr>
  <tr>
  <td><img src="flags/es.png" alt="es" />&nbsp;&nbsp;<img
src="flags/ca.png" alt="ca" /></td>
  <td>Spanish - Catalan vocabulary</td>
  <td><a href="http://files.kde.org/edu/kvtml/escat0.2.kvtml">
escat0.2.kvtml</a></td>
 <td>-</td>
  <td>2006-11-12</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" />&nbsp;&nbsp;<img src="flags/ar.png" alt="ar" /></td>
   <td>Some basic words/phrases for german / arabic</td>
   <td><a href="http://files.kde.org/edu/kvtml/de-ar-1.kvtml">de-ar-1.kvtml</a></td>
   <td>Includes arabic without vocal signs, and arabic with.</td>
   <td>2007-07-18</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" />&nbsp;&nbsp;<img src="flags/ar.png" alt="ar" /></td>
   <td>Some basic words/phrases for german / arabic level 2</td>
   <td><a href="http://files.kde.org/edu/kvtml/de-ar-2.kvtml">de-ar-2.kvtml</a></td>
   <td>Includes arabic without vocal signs, and arabic with.</td>
   <td>2007-07-18</td>
  </tr>

  <tr>
   <td><img src="flags/cn.png" alt="cn" /></td>
   <td>A collection of chinese files will be included with kde4 chinese language pack(s)</td>
   <td><a href="http://files.kde.org/edu/kvtml/chinese-kvtml-1.0.tar.gz">chinese-kvtml-1.0.tar.gz</a></td>
   <td>Includes: HSK test sets A-D all words now, and most frequent 2000 characters grouped into 500 character files.</td>
   <td>2007-10-12</td>
  </tr>

  <tr>
   <td><img src="flags/tr.png" alt="tr" />&nbsp;&nbsp;<img src="flags/de.png" alt="de" /></td>
   <td>Turkish and german - words, sentence chunks (constructions) and sentences (from a grammar,
mostly)</td>
   <td><a href="http://files.kde.org/edu/kvtml/tr-de.kvtml">tr-de.kvtml</a></td>
   <td>792 entries.</td>
   <td>2007-10-21</td>
  </tr>

  <tr>
   <td><img src="flags/br.png" alt="br_PT" />&nbsp;&nbsp;<img src="flags/de.png" alt="de" /></td>
   <td>Brasilian Portugese - German 1.0</td>
   <td><a href="http://files.kde.org/edu/kvtml/pt_BR-de.kvtml">pt_BR-de</a></td>
   <td>Including Portugese conjugations and many example sentences.</td>
   <td>2007-11-22</td>
  </tr>

  <tr>
   <td><img src="flags/ar.png" alt="ar" />&nbsp;&nbsp;<img src="flags/nl.png" alt="nl" />&nbsp;&nbsp;<img src="flags/gb.png" alt="en" /></td>
   <td>Arabic-Dutch-English</td>
   <td><a href="http://files.kde.org/edu/kvtml/ar-nl-en.kvtml">ar-nl-en</a></td>
   <td>Still growing, the first 400 words.</td>
   <td>2007-12-12</td>
  </tr>

 </table>

<a name="anatomy"></a>
<h2><?php i18n( "Anatomy / Physiology" ); ?></h2>

<a name="joints"></a>
<h3><?php i18n( "Joints" ); ?></h3>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>general information on joints (Gelenke)</td>
   <td><a href="http://files.kde.org/edu/kvtml/a_gelenke.kvtml">a_gelenke.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the knee</td>
   <td><a href="http://files.kde.org/edu/kvtml/a_knie.kvtml">a_knie.kvtml</a></td>
   <td>example uses the right knee.</td>
   <td>2003-06-01</td>
  </tr>

</table>

<a name="bones"></a>
<h3><?php i18n( "Bones" ); ?></h3>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>general information on bones</td>
   <td><a href="http://files.kde.org/edu/kvtml/o_allgemein.kvtml">o_allgemein.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the hip</td>
   <td><a href="http://files.kde.org/edu/kvtml/o_becken.kvtml">o_becken.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the chest</td>
   <td><a href="http://files.kde.org/edu/kvtml/o_brustkorb.kvtml">o_brustkorb.kvtml</a></td>
   <td>-</td>
   <td>2003-06-16</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the chest, theoretical aspects</td>
   <td><a href="http://files.kde.org/edu/kvtml/o_brustkorb_th.kvtml">o_brustkorb_th.kvtml</a></td>
   <td>-</td>
   <td>2003-06-16</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>os humeralis (arm)</td>
   <td><a href="http://files.kde.org/edu/kvtml/o_oberarm.kvtml">o_oberarm.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>os humeralis (arm), theoretical aspects</td>
   <td><a href="http://files.kde.org/edu/kvtml/o_oberarm_th.kvtml">o_oberarm_th.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the shoulder</td>
   <td><a href="http://files.kde.org/edu/kvtml/o_schulter.kvtml">o_schulter.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the spine</td>
   <td><a href="http://files.kde.org/edu/kvtml/o_wirbelsaeule.kvtml">o_wirbelsaeule.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

</table>

<a name="muscles"></a>
<h3><?php i18n( "Muscles" ); ?></h3>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>general information on muscles</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_allgemein.kvtml">m_allgemein.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

 <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>types of contraction</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_kontrakt.kvtml">m_kontrakt.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

 <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>muscle types</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_typen.kvtml">m_typen.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the stomach</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_bauch.kvtml">m_bauch.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the stomach, theoretical aspects</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_bauch_th.kvtml">m_bauch_th.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the breast</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_brust.kvtml">m_brust.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the breast, theoretical aspects</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_brust_th.kvtml">m_brust_th.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>


  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the chest</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_brustkorb.kvtml">m_brustkorb.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the chest, theoretical aspects</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_brustkorb_th.kvtml">m_brustkorb_th.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>erector spinae</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_erectorspinae.kvtml">m_erectorspinae.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>erector spinae, theoretical aspects</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_erectorspinae_th.kvtml">m_erectorspinae_th.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>


 <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>shoulder belt</td>
   <td><a href="http://files.kde.org/edu/kvtml/m_schulterguertel.kvtml">m_schulterguertel.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>
</table>

<a name="ligamenti"></a>
<h3><?php i18n( "Ligamenti" ); ?></h3>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>general information on the ligamenti</td>
   <td><a href="http://files.kde.org/edu/kvtml/lig_allg.kvtml">lig_allg.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>ligamenti humerale (arm)</td>
   <td><a href="http://files.kde.org/edu/kvtml/lig_oberarm.kvtml">lig_oberarm.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the spine's ligamenti</td>
   <td><a href="http://files.kde.org/edu/kvtml/lig_wirbelsaeule.kvtml">lig_wirbelsaeule.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

</table>

<a name="anamisc"></a>
<h3><?php i18n( "Miscellaneous" ); ?></h3>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>Latin expressions</td>
   <td><a href="http://files.kde.org/edu/kvtml/mi_begriffe.kvtml">mi_begriffe.kvtml</a></td>
   <td>eases learning if you know what the latin expressions mean</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>the shock</td>
   <td><a href="http://files.kde.org/edu/kvtml/mi_schock.kvtml">mi_schock.kvtml</a></td>
   <td>-</td>
   <td>2003-06-14</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>Injuries and their names</td>
   <td><a href="http://files.kde.org/edu/kvtml/mi_verletzungen.kvtml">mi_verletzungen.kvtml</a></td>
   <td>-</td>
   <td>2003-06-01</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>brain injuries</td>
   <td><a href="http://files.kde.org/edu/kvtml/mi_verletzungen.kvtml">mi_verletzungen hirn.kvtml</a></td>
   <td>-</td>
   <td>2003-06-03</td>
  </tr>

 </table>

<p style="font-size:small">

   Key: a: articulare (joints) ; m: musculi (muscles) ; o: os (bones) ; mi: misc. ; _th: theory
</p>


<a name="music"></a>
<h2><?php i18n( "Music" ); ?></h2>
<br/>

<a name="guitar"></a>
<h3><?php i18n( "Guitar" ); ?></h3>

 <table class="kvtml">
  <colgroup>
    <col style="width:40%;" />
    <col style="width:30%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td>guitar tuning standard fretboard</td>
   <td><a href="http://files.kde.org/edu/kvtml/guitar-fretboard-standard-tuning.kvtml">guitar-fretboard-standard-tuning.kvtml</a></td>
   <td>Learn which six notes appear on a particular fret for a guitar in
standard tuning.  Helps to increase sight-reading speed.</td>
   <td>2003-07-21</td>
  </tr>

</table>

<a name="theory"></a>
<h3><?php i18n( "Music Theory" ); ?></h3>

 <table class="kvtml">
  <colgroup>
    <col style="width:40%;" />
    <col style="width:30%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td>all keys signatures</td>
   <td><a href="http://files.kde.org/edu/kvtml/key-signatures-all-keys.kvtml">key-signatures-all-keys.kvtml</a></td>
   <td>Learn to recognise which key a piece of music is written in by examining
how many sharps or flats appear in the key signature.</td>
   <td>2003-07-21</td>
  </tr>

    <tr>
   <td>keys signatures: major keys</td>
   <td><a href="http://files.kde.org/edu/kvtml/key-signatures-major-keys.kvtml">key-signatures-major-keys.kvtml</a></td>
   <td>A subset of key-signatures-all-keys.kvtml - just the major keys.</td>
   <td>2003-07-21</td>
  </tr>

   <tr>
   <td>keys signatures: major keys flat</td>
   <td><a href="http://files.kde.org/edu/kvtml/key-signatures-major-keys-flats.kvtml">key-signatures-major-keys-flats.kvtml</a></td>
   <td>A subset of key-signatures-all-keys.kvtml - just the major keys with
flats in the key signature.</td>
   <td>2003-07-21</td>
  </tr>

    <tr>
   <td>keys signatures: major keys sharp</td>
   <td><a href="http://files.kde.org/edu/kvtml/key-signatures-major-keys-sharps.kvtml">key-signatures-major-keys-sharps.kvtml</a></td>
   <td>A subset of key-signatures-all-keys.kvtml - just the major keys with
sharps in the signature.</td>
   <td>2003-07-21</td>
  </tr>

    <tr>
   <td>keys signatures: minor keys</td>
   <td><a href="http://files.kde.org/edu/kvtml/key-signatures-minor-keys.kvtml">key-signatures-minor-keys.kvtml</a></td>
   <td>A subset of key-signatures-all-keys.kvtml - just the minor keys</td>
   <td>2003-07-21</td>
  </tr>

   <tr>
   <td>keys signatures: minor keys flat</td>
   <td><a href="http://files.kde.org/edu/kvtml/key-signatures-minor-keys-flats.kvtml">key-signatures-minor-keys-flats.kvtml</a></td>
   <td>A subset of key-signatures-all-keys.kvtml - just the minor keys with
flats in the key signature.</td>
   <td>2003-07-21</td>
  </tr>

    <tr>
   <td>keys signatures: minor keys sharp</td>
   <td><a href="http://files.kde.org/edu/kvtml/key-signatures-minor-keys-sharps.kvtml">key-signatures-minor-keys-sharps.kvtml</a></td>
   <td>A subset of key-signatures-all-keys.kvtml - just the minor keys with
sharps in the key signature.</td>
   <td>2003-07-21</td>
  </tr>

    <tr>
   <td>keys signatures: relative major and-minor keys</td>
   <td><a href="http://files.kde.org/edu/kvtml/relative-major-and-minor-keys.kvtml">relative-major-and-minor-keys.kvtml</a></td>
   <td>Each major key shares the same notes as one particular minor key.  Only
the the starting note diffs.  Each major key is said to have a "relative
minor".  In reverse, each minor key is said to have a "relative major".</td>
   <td>2003-07-21</td>
  </tr>

</table>

<p style="font-size:small">
   Note: the key-signatures-all-keys file has been broken into -major and -minor, then
those halves have been separated into -sharps and -flats since it is easier to
learn those smaller chunks than to try to get everything in one go
</p>

<a name="history"></a>
<h2><?php i18n( "History" ); ?></h2>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>List of the roman emperors from Augustus till the constitution of
the tetrachy in 284 a.c.</td>
   <td><a href="http://files.kde.org/edu/kvtml/Kaiser.kvtml">Kaiser.kvtml</a></td>
   <td>-</td>
   <td>2007-01-18</td>
  </tr>

  <tr>
   <td><img src="flags/us.png" alt="en" /></td>
   <td>List of the different capitals of USA states from the beginning to now</td>
   <td><a href="http://files.kde.org/edu/kvtml/uS Historical Capitals (advanced).kvtml">uS Historical Capitals (advanced).kvtml</a></td>
   <td>-</td>
   <td>2007-03-22</td>
  </tr>

  <tr>
   <td><img src="flags/us.png" alt="en" /></td>
   <td>Inventors and their inventions - English</td>
   <td><a href="http://files.kde.org/edu/kvtml/inventors.kvtml">inventors.kvtml</a></td>
   <td>-</td>
   <td>2007-03-28</td>
  </tr>

 </table>

<a name="geography"></a>
<h2><?php i18n( "Geography" ); ?></h2>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/gb.png" alt="gb" /></td>
   <td>World capitals - English</td>
   <td><a href="http://files.kde.org/edu/kvtml/world_capitals.kvtml">world_capitals.kvtml</a></td>
   <td>-</td>
   <td>2002-12-05</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>World capitals - German</td>
   <td><a href="http://files.kde.org/edu/kvtml/world_capitals-de.kvtml">world_capitals-de.kvtml</a></td>
   <td>-</td>
   <td>2007-02-03</td>
  </tr>

  <tr>
   <td><img src="flags/nl.png" alt="nl" /></td>
   <td>World capitals - Dutch</td>
   <td><a href="http://files.kde.org/edu/kvtml/world_capitals-nl.kvtml">world_capitals-nl.kvtml</a></td>
   <td>-</td>
   <td>2007-02-19</td>
  </tr>

  <tr>
   <td><img src="flags/it.png" alt="it" /></td>
   <td>World capitals - Italian</td>
   <td><a href="http://files.kde.org/edu/kvtml/capitali_del_mondo.kvtml">capitali_del_mondo.kvtml</a></td>
   <td>-</td>
   <td>2004-02-24</td>
  </tr>

  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>European capitals in German</td>
   <td><a href="http://files.kde.org/edu/kvtml/european_capitals.kvtml">european_capitals.kvtml</a></td>
   <td>-</td>
   <td>2005-12-13</td>
  </tr>

  <tr>
   <td><img src="flags/fr.png" alt="fr" /></td>
   <td>French departments</td>
   <td><a href="http://files.kde.org/edu/kvtml/departements.kvtml">departements.kvtml</a></td>
   <td>Learn the French departments - Apprenez les d&eacute;partements fran&ccedil;ais</td>
   <td>2003-08-12</td>
  </tr>

 </table>

<a name="math"></a>
<h2><?php i18n( "Math" ); ?></h2>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/us.png" alt="en" /></td>
   <td>Learn to name the numbers in English (easy)</td>
   <td><a href="http://files.kde.org/edu/kvtml/numbers (easy).kvtml">numbers (easy).kvtml</a></td>
   <td>-</td>
   <td>2007-03-22</td>
  </tr>

  <tr>
   <td><img src="flags/us.png" alt="en" /></td>
   <td>Learn to name the numbers in English (hard)</td>
   <td><a href="http://files.kde.org/edu/kvtml/numbers (advanced).kvtml">numbers (advanced).kvtml</a></td>
   <td>-</td>
   <td>2007-03-22</td>
  </tr>

 </table>

<a name="chemistry"></a>
<h2><?php i18n( "Chemistry" ); ?></h2>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/us.png" alt="en" /></td>
   <td>Chemical elements of the periodic table in English</td>
   <td><a href="http://files.kde.org/edu/kvtml/periodic table.kvtml">periodic table.kvtml</a></td>
   <td>-</td>
   <td>2007-03-22</td>
  </tr>

  <tr>
   <td><img src="flags/us.png" alt="en" /></td>
   <td>Amino acids in English</td>
   <td><a href="http://files.kde.org/edu/kvtml/aminos.kvtml">aminos.kvtml</a></td>
   <td>2 lessons: Amino Acid One letter Abbreviations and General R Group</td>
   <td>2007-04-30</td>
  </tr>

 </table>

<a name="exam"></a>
<h2><?php i18n( "Exam Preparation" ); ?></h2>

 <table class="kvtml">
  <colgroup>
    <col style="width:15%;" />
    <col style="width:30%;" />
    <col style="width:25%;" />
    <col style="width:20%;" />
    <col style="width:10%;" />
  </colgroup>

  <tr>
   <th>Languages</th>
   <th>Description</th>
   <th>Filename</th>
   <th>Notes</th>
   <th>Last Update</th>
  </tr>

  <tr>
   <td><img src="flags/us.png" alt="us" /></td>
   <td>GRE preparation</td>
   <td><a href="http://files.kde.org/edu/kvtml/gretools.kvtml">gretools.kvtml</a></td>
   <td>Published by <a href="http://theory.cs.iitm.ernet.in/~arvindn/gretools/">GRETools</a>.</td>
   <td>2005-07-19</td>
  </tr>
  <tr>
   <td><img src="flags/us.png" alt="us" /></td>
   <td>SAT preparation</td>
   <td><a href="http://files.kde.org/edu/kvtml/SAT.kvtml">SAT.kvtml</a></td>
   <td>Published by <a href="http://www.freevocabulary.com/">http://www.freevocabulary.com/</a>.</td>
   <td>2005-07-19</td>
  </tr>
  <tr>
   <td><img src="flags/de.png" alt="de" /></td>
   <td>SRC preparation</td>
   <td><a href="http://files.kde.org/edu/kvtml/src_exam.kvtml">src_exam.kvtml</a></td>
   <td>Exam questions for German Short Range Certificate, see <a href="http://www.bmvbw.de">http://www.bmvbw.de</a></td>
   <td>2005-12-20</td>
  </tr>
  <tr>
   <td><img src="flags/us.png" alt="us" /><img src="flags/es.png" alt="es" /></td>
   <td>ASE (Nationaly Institute of Automotive Service Excellence) exam topics</td>
   <td><a href="http://files.kde.org/edu/kvtml/A1_Engine_Repair.kvtml">A1_Engine_Repair.kvtml</a><br />
   <a href="http://files.kde.org/edu/kvtml/A4_Steering_and_Suspension.kvtml">A4_Steering_and_Suspension.kvtml</a><br />
   <a href="http://files.kde.org/edu/kvtml/A5_Brakes.kvtml">A5_Brakes.kvtml</a><br />
   <a href="http://files.kde.org/edu/kvtml/A6_Electrical_Electronic_Systems.kvtml">A6_Electrical_Electronic_Systems.kvtml</a></td>
   <td>Words from: <a href="http://www.ase.com/Content/NavigationMenu/Service_Professionals1/Espa%F1ol/Glosarios_de_T%E9rminos/Glosarios_de_ASE_Ingl%E9s_Espa%F1ol.htm">http://www.ase.com/</a></td>
   <td>2007-08-17</td>
  </tr>
 </table>

 <p><?php i18n( "All the files here are distributed under free licenses. If not otherwise stated this is the <b>GNU General Public License</b>. You are encouraged to download and use the files according to their license.<br /><b>And you are even more encouraged to contribute your own files or improve the existing ones!</b>" ); ?></p>

 <p><?php
   echo i18n_var( "There is another thing you should keep in mind when using data from books or similar: copyright violation. As a rule of thumb you might say that individual word translations can be used from books but not entire sections (The data isn't copyrighted but the collection of data is).")." ".
   i18n_var( 'Also books out of copyright can be used for data (in the US this means mainly pre-1923 books). Have a look at project Gutenberg at <a href="http://gutenberg.spiegel.de/">Projekt Gutenberg-DE</a> (German site), or <a href="http://www.gutenberg.org/">www.gutenberg.org</a> (English site), for more information about finding out if a book is out of copyright.' ); ?></p>

 <p><?php i18n( "Despite the fact that all files can be loaded and used with both KWordQuiz and Parley you should keep in mind that KWordQuiz has a simpler design and might ignore some features from the files. If you work with KWordQuiz and save afterwards you might lose some properties that are available in Parley. For that reason the table above lists advanced features that are only available in Parley. Minor features like the names of lessons are not mentioned though they get lost with KWordQuiz. It is expected that this limitation will be removed in the next major version of KDE." ); ?></p>

 <p><?php i18n( '<b>Removing Grades:</b> Using Parley you should be able to remove grades from the files by using Edit -> Remove Grades.' ); ?></p>

<h2><?php i18n( "Contributors" ); ?></h2>
<ul>
<li><a href="mailto:predator1710 AT gmx DOT de">Juergen Appel</a> Anatomy - Misc</li>
<li><a href="mailto:pkeenan AT blueyonder DOT co DOT uk">Paul J. Keenan</a> Music files</li>
<li><a href="http://amor.rz.hu-berlin.de/~golcherf/">Felix Golcher</a> Turkish - German</li>
<li><a href="mailto:grossard AT kde DOT org">Ludovic Grossard</a> French Departments file</li>
<li><a href="mailto:davh AT davh.dk">Dennis Haney</a> Japanese Hirigana file</li>
<li><a href="mailto:simontol AT inwind DOT it">Simone Tolotti</a> World Capitals in Italian</li>
<li><a href="mailto:Markus DOT Buechele AT web DOT de">Markus B&uuml;chele</a> Basic vocabulary English/German</li>
<li><a href="http://wernersindex.de/lateindeutsch.htm">Werner Eichelberg</a> Latin
&lt;--&gt; German KVTML files - Thanks to <a href="mailto:lueck AT hube-lueck
DOT de">Burkhard L&uuml;ck</a> for getting the data in KVTML format.</li>
<li><a href="mailto:karl AT huftis DOT org">Karl Ove Hufthammer</a> Strong verbs
used in Norwegian Nynorsk for KVocTrain</li>
<li><a href="mailto:xelzifesch AT web DOT de">Felix Zesch</a> German to
Esperanto (vortaro-eo-de)</li>
<li><a href="mailto:skkd.h4k1n9 AT googlemail DOT com">S. Karl Kochs D.</a> German to
Esperanto (vortoj-de-eo)</li>
<li><a href="mailto:sagasu AT gmail DOT com">Mateusz Kopij</a> Polish to German verbs and adjectives, Polish to French basic vocabulary</li>
<li><a href="mailto:spyb4573 AT gmail DOT com">Bart Spyra</a> English/Korean vocabulary</li>
<li><a href="mailto:rene.merou AT kdemail DOT net">Ren&eacute; M&eacute;rou</a> Spanish/Catalan common mistakes vocabulary</li>
<li><a href="mailto:highwaykind AT gmail DOT net">Corien Bennink</a> English/Dutch vocabulary</li>
<li><a href="mailto:andre.hager AT gmx DOT net">Andre Hager</a> List of the roman emperors from Augustus till the constitution of
the tetrachy in 284 a.c.</li>
<li><a href="mailto:sonium AT gmail DOT com">Alexander Hupfer</a> World Capitals in German</li>
<li><a href="mailto:ml AT winfix DOT it">Jo</a> World Capitals in Dutch</li>
<li><a href="mailto:keith.penguin AT gmail DOT com">Keith Worrell</a> US Historical Capitals, numbers (easy and hard), periodic table, inventors</li>
<li><a href="mailto:aikawarazuni AT gmail DOT com">Alejandro Wainzinger</a> Amino acids in English</li>
<li><a href="mailto:ThomasBergmannn AT gmx DOT de">Thomas Bergmann</a> Brasilian Portugese - German</li>
<li><a href="mailto:henk AT laaksoft DOT nl">Henk van der Laak</a> Arabic-Dutch-English</li>
<li><a href="mailto:david DOT moreno AT gmail DOT com>">David Moreno</a> German-Spanish Collection</li>
</ul>

<?php
  include "footer.inc";
?>
