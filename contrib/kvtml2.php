<?php
  $translation_file = "edu-kde-org";
  include_once( "functions.inc" );
  $site_root = "../";
  $page_title = i18n_noop( "Contributed Learning Files" );

  include ( "header.inc" );
?>

<p><?php echo i18n_var( "The learning files listed here are intended for use with %1, %2, and %3, which are all part of the KDE Educational Project.", '<a href="../parley/" title="Parley">Parley</a>', '<a href="../kwordquiz/" title="KWordQuiz">KWordQuiz</a>, <a href="../kanagram/" title="Kanagram">Kanagram</a>', '<a href="../khangman/" title="KHangman">KHangman</a>' ); ?></p>

<p><?php echo i18n_var( 'Please send your contributed files to <a href="mailto:kde-edu@kde.org">the KDE-Edu mailing list</a> so they can be uploaded on this page.' ); ?></p>
<p><?php i18n( "All the files here are distributed under free licenses. If not otherwise stated this is the <b>GNU General Public License</b>. You are encouraged to download and use the files according to their license.<br /><b>And you are even more encouraged to contribute your own files or improve the existing ones!</b>" ); ?></p>

<h2><?php i18n( "Areas:" ); ?></h2>

<ul>
  <li><a href="#vocab"><?php i18n( "Vocabulary / Languages" ); ?></a></li>
</ul>

<a name="vocab"></a>
<h3><?php i18n( "Vocabulary / Languages" ); ?></h3><br />

<!-- language_1+2   filename   description   last_update -->

<table width="100%" border="1">

  <tr align="center">
   <td width="11%"><b>Languages</b></td>
   <td width="25%"><b>Filename</b></td>
   <td width="35%"><b>Description</b></td>
   <td width="30%"><b>Author</b></td>
   <td width="9%"><b>Last Update</b></td>
  </tr>

  <tr>
   <td align="center">Latin<br/>German</td>
   <td><a href="http://files.kde.org/edu/kvtml2/latinum.kvtml.bz2">latinum.kvtml.bz2</a></td>
   <td>Learning files for german/latin - latin/german</td>
   <td>"Frank C. Eckert" <f.c.e@web.de></td>
   <td>2007-10-18</td>
  </tr>

  <tr>
   <td align="center">Greek<br/>German</td>
   <td><a href="http://files.kde.org/edu/kvtml2/graecum.kvtml.bz2">graecum.kvtml.bz2</a></td>
   <td>Learning files for german/greek - greek/german</td>
   <td>"Frank C. Eckert" <f.c.e@web.de></td>
   <td>2007-10-18</td>
  </tr>

  <tr>
   <td align="center">Arabic<br/>English<br/>German<br/>Spanish</td>
   <td><a href="http://www.floyd-online.com/Download/de-uk-es-ar2.kvtml">de-uk-es-ar<br/>(no images)</a>
	<br/><br/>
	<a href="http://www.floyd-online.com/Download/en_de_es_ar.tar.bz2">de-uk-es-ar<br/>(including images)</a></td>
   <td>The start of an extensive collection in four languages. Please note that it's not finished and contact Florian in case you find mistakes.</td>
   <td>Florian Schmid <br/><a href="mailto:mail AT floyd-online DOT com">email</a></td>
   <td>2007-12-11</td>
  </tr>

  <tr>
   <td align="center">Catalan<br/>Spanish<br/>German</td>
   <td><a href="http://files.kde.org/edu/kvtml2/language/cat-es-ger.kvtml">cat-es-ger.kvtml</a></td>
   <td>Basic words in Spanish, Catalan and German</td>
   <td>matthias <br/><a href="mailto:mensch0815 AT googlemail DOT com">email</a></td>
   <td>2009-04-05</td>
  </tr>

</table>

 <h3><?php i18n( "More vocabulary files" ); ?></h3>
  <p><?php i18n( 'For KDE 3 many files exist that work perfectly fine with the new versions of the programs. Have a look at <a href="../contrib/kvtml.php">Vocabulary files for KDE 3</a>.' ); ?></p>

<br/>


<hr width="30%" align="center" />
<p>
<?php echo i18n_var( 'Last update: %1', date ("Y-m-d", filemtime( __FILE__ ) ) ); ?>
</p>

<?php
  include "footer.inc";
?>

