<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );
  $page_title = i18n_noop( "Kig Macros" );
  include( "header.inc" );
?>

  <p><?php i18n( "Since Kig can import and use third-party macros as built-in objects, it can have new objects,
  being more and more powerful." ); ?></p>
  
  <p id="kig-macro-install-paragraph" style="font-weight:bold;"></p>
  <div id="kig-macro-install">

  <p><?php i18n( "Once you have a macro file, you can import it into Kig using one of these instructions (tailored for a typical Linux system):" ); ?></p>
  
  <dl>
    <dt><?php i18n( "Local installation" ); ?></dt>
    <dd><?php i18n( "Install a macro only for the user which installed it: in Kig, select <strong>Types&nbsp;->&nbsp;Manage Types</strong>, click on the <strong>Import</strong> button, select the macro file(s) you want to import, and you're done." ); ?></dd>
    
    <dt><?php i18n( 'Global installation' ); ?></dt>
    <dd><?php i18n( 'This requires the root privilegies! Install a macro for every Kig users on that machine: place it in <span style="font-family:monospace;">&#36;KDEDIR/share/apps/kig/builtin-macros/</span>. The new macro(s) will be available in Kig the next time you start Kig.' ); ?></dd>
  </dl>
  </div>
  
  <script type="text/javascript">
  /* <![CDATA[ */
      $(document).ready(function(){
          $('#kig-macro-install').toggle();
          $('#kig-macro-install-paragraph').append('<a href="#" id="kig-macro-install-toggle">'+'<?php i18n("How can I import a macro file?"); ?>'+'</a>');
          //Add toggle effect
          $('#kig-macro-install-toggle').click(function(){
             $('#kig-macro-install').slideToggle('slow');
             return false;
          });
      });
  /* ]]> */
  </script>
  
  <br />

  <table summary="Kig Macros" class="kvtml">
    <thead>
      <tr>
        <th># of<br />
        mac.</th>

        <th>Name</th>

        <th>Description</th>

        <th>Author</th>

        <th>License</th>

        <th>File</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>

        <td>cirkel</td>

        <td>A circle BTP constructed using the circle BCP</td>

        <td>Dominique Devriese</td>

        <td>GPL</td>

        <td><a href="macros/circleBTP_by_circleBCP.kigt" title=
        "circleBTP_by_circleBCP.kigt"><img border="0" src="../../images/icons/kig_22.png" alt=
        "circleBTP_by_circleBCP.kigt" /></a></td>
      </tr>

      <tr>
        <td>1</td>

        <td>ConicByLocus</td>

        <td>A conic constructed as a locus</td>

        <td>Dominique Devriese</td>

        <td>GPL</td>

        <td><a href="macros/conic_by_locus.kigt" title="conic_by_locus.kigt"><img border=
        "0" src="../../images/icons/kig_22.png" alt="conic_by_locus.kigt" /></a></td>
      </tr>

      <tr>
        <td>1</td>

        <td>ConicCenter</td>

        <td>The center of a conic</td>

        <td>Dominique Devriese</td>

        <td>GPL</td>

        <td><a href="macros/ConicCenter.kigt" title="ConicCenter.kigt"><img border="0"
        src="../../images/icons/kig_22.png" alt="ConicCenter.kigt" /></a></td>
      </tr>

      <tr>
        <td>1</td>

        <td>LineConicMainAxis</td>

        <td>The main axis of a conic</td>

        <td>Dominique Devriese</td>

        <td>GPL</td>

        <td><a href="macros/LineConicMainAxis.kigt" title=
        "LineConicMainAxis.kigt"><img border="0" src="../../images/icons/kig_22.png" alt=
        "LineConicMainAxis.kigt" /></a></td>
      </tr>

      <tr>
        <td>1</td>

        <td>LineConicSecondAxis</td>

        <td>The secondary axis of a conic</td>

        <td>Dominique Devriese</td>

        <td>GPL</td>

        <td><a href="macros/LineConicSecondAxis.kigt" title=
        "LineConicSecondAxis.kigt"><img border="0" src="../../images/icons/kig_22.png" alt=
        "LineConicSecondAxis.kigt" /></a></td>
      </tr>

      <tr>
        <td>1</td>

        <td>Translatie</td>

        <td>Mirror a point using the translation</td>

        <td>Dominique Devriese</td>

        <td>GPL</td>

        <td><a href="macros/mirrorpoint_by_translationpoint.kigt" title=
        "mirrorpoint_by_translationpoint.kigt"><img border="0" src=
        "../../images/icons/kig_22.png" alt="mirrorpoint_by_translationpoint.kigt" /></a></td>
      </tr>

      <tr>
        <td>6</td>

        <td>
          <ol type="1">
            <li>Baricenter</li>

            <li>Circumcenter</li>

            <li>Gauss Segment</li>

            <li>Incenter</li>

            <li>Inscribed circle</li>

            <li>Ortocenter</li>
          </ol>
        </td>

        <td>
          <ol type="1">
            <li>Baricenter of a triangle, given the vertices</li>

            <li>Circumcenter of a triangle, given the vertices</li>

            <li>Gauss Segment of a triangle, given the vertices</li>

            <li>Incenter of a triangle, given the vertices</li>

            <li>Inscribed circle on a triangle, given the vertices</li>

            <li>Ortocenter of a triangle, given the vertices</li>
          </ol>
        </td>

        <td>Noel Torres</td>

        <td>Public Domain</td>

        <td><a href="macros/triangle_centers.kigt" title=
        "triangle_centers.kigt"><img border="0" src="../../images/icons/kig_22.png" alt=
        "triangle_centers.kigt" /></a></td>
      </tr>

      <tr>
        <td>2</td>

        <td>star5 - star5b</td>

        <td>Two different ways to construct a star.</td>

        <td>Maurizio Paolini</td>

        <td>GPL</td>

        <td><a href="macros/starswith5points.kigt" title=
        "starswith5points.kigt"><img border="0" src="../../images/icons/kig_22.png" alt=
        "starswith5points.kigt" /></a></td>
      </tr>

      <tr>
        <td>4</td>

        <td>
          <ol type="1">
            <li>TropicalLine1pt</li>

            <li>TropicalLine2pt</li>

            <li>TropicalLineLineIntersection</li>

            <li>TropicalConic</li>
          </ol>
        </td>

        <td>
          <ol type="1">
            <li>a tropical line constructed using its centre point</li>

            <li>The stable tropical line passing through two points</li>

            <li>The stable intersection of two tropical lines, to select a tropical line, select
            its centre point</li>

            <li>Stable Conic Passing through 5 points</li>
          </ol><img border="0" src="../../images/icons/python_22.png" alt="Python script" style="float:left;margin-right:10px;" /> These macros require a Kig version with the Python script.
        </td>

        <td>Luis Felipe Tabera<br />
        &lt;<a href="mailto:lftabera%20AT%20yahoo%20DOT%20es">lftabera AT yahoo DOT es</a>&gt;</td>

        <td>GPL</td>

        <td><a href="macros/tropical_geometry.kigt" title=
        "tropical_geometry.kigt"><img border="0" src="../../images/icons/kig_22.png" alt=
        "tropical_geometry.kigt" /></a></td>
      </tr>
    </tbody>
  </table>
  
<?php include( "footer.inc" ); ?>
