<?php
  $page_title = 'Contributed Learning Files';
  
  include ( "header.inc" );
?>
  <p>The learning files listed here are intended for use with <a href=
  "/applications/language/parley/">Parley</a> and <a href=
  "/applications/language/kwordquiz/">KWordQuiz</a> which are both part of the KDE Education
  project. The files can also be used with <a href=
  "/applications/language/flashkard/">FlashKard</a>, a program that was included in earlier
  versions of KDE. To some extent these files also work with <a href=
  "/applications/language/khangman/">KHangMan</a> and <a href=
  "/applications/language/kanagram/">Kanagram</a>.</p>

  <h2>Subjects</h2>

  <ul>
    <li><a href="#vocab">Vocabulary / Languages</a></li>

    <li><a href="#exam">Exam Preparation</a></li>
  </ul><a name="vocab" id="vocab"></a>

  <h2>Vocabulary / Languages</h2><!-- language_1+2   filename   description   last_update -->

  <table class="kvtml">
    <tr>
      <th>Language Vocabulary</th>

      <th>Description</th>

      <th>Author</th>

      <th>Last Update</th>

      <th>Dowload</th>
    </tr><?php
        include "table_language.inc"
      ?>
  </table><a name="exam" id="exam"></a>

  <h2>Exam Preparation</h2>

  <table class="kvtml">
    <tr>
      <th>Test Preparation</th>

      <th>Description</th>

      <th>Author</th>

      <th>Last Update</th>

      <th>Dowload</th>
    </tr><?php
        include "table_exam.inc"
      ?>
  </table>

  <h2>More vocabulary files</h2>

  <p>For KDE&nbsp;3 many files exist that work perfectly fine with the new versions of the programs.
  Have a look at <a href="../kvtml/">Vocabulary files for KDE&nbsp;3</a>.</p>
  
<?php include "footer.inc"; ?>
