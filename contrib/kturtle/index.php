<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );
  $page_title = i18n_noop( "KTurtle Logo Scripts" );
  include( "header.inc" );
?>
<p><?php i18n( "You can download these Logo files and freely use them with KTurtle." ); ?></p>

<?php
$entries = scandir( "logo_scripts" );
foreach( $entries as $entry ){
  if( substr( $entry, -7 ) == ".turtle" ) {
    $basename = substr( $entry, 0, -7 );
    $screenshot = "images/$basename.png"; 
    $thumb = "images/thumbs/$basename.png";
?>
<div style="text-align:center;width:300px;float:left;">
<a href="<?php echo $screenshot; ?>"><img src="<?php echo $thumb; ?>" /></a><br />
<a href="logo_scripts/<?php echo $entry; ?>"><?php echo $entry; ?></a>
</div>
<?php
  }
}
?>

<p style="clear:left;"><?php echo i18n_var( "Feel free to share your KTurtle examples. Please upload files you created to %1. They will appear on the page. Already available files can be found <a href='http://kde-files.org/index.php?xcontentmode=686'>here</a>.", '<a href="http://kde-files.org/content/add.php">kde-files.org</a>' ); ?></p>

<p><?php i18n( "If you have questions or need help, don't hesitate to <a href=\"/contact_us/\">contact us</a>." ); ?></p>

<?php
  include "footer.inc";
?>


