<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );
  $site_root = "../";
  $page_title = i18n_noop( "Get Involved..." );
  
  include ( "header.inc" );
?>

<h3><?php i18n( "With KDE" ); ?></h3>

<img src="../images/icons/kde_64.png" style="width:64px;height:64px;float:right;" alt="KDE logo" />
<p><?php i18n( "Possible tasks include testing, bugreporting and triaging incoming bugreports; writing code, developing artwork, documentation or translations; supporting and maintaining our infrastructure; or spreading the word and helping out with promotion and marketing both on line and at tradeshows and conferences." ); ?></p>

<p><?php i18n( "For more information go to the <a href=\"http://kde.org/community/getinvolved/\">main KDE page</a>." ); ?></p>

<h3 style="clear:right;"><?php i18n( "With KDE Education" ); ?></h3>

<img src="../images/icons/new-KDEdu-logo.png" style="float:right;" alt="<?php i18n( "KDE-Edu logo" ); ?>" />
<p><?php i18n( "The KDE Education team is very open to new people.
Join us and have a chat with us on IRC (server: irc.freenode.net, channel: #kde-edu)
or contact us via the <a href=\"https://mail.kde.org/mailman/listinfo/kde-edu\">kde-edu mailing-list</a>.
This is a great opportunity to get involved with an open source project!" ); ?></p>

<p><?php i18n( "If you want to help within the KDE Education team you probably find a suitable task below. Your help is welcome!" ); ?></p>

<h3 style="clear:right;"><img src="../images/icons/feedback_32.png" class="header-image" width="32" height="32" alt="refresh" /><?php i18n( "Feedback" ); ?></h3>

<p><?php i18n( "The easiest way to get involved is to use the applications and give us some feedback about:" ); ?></p>

<ul>
  <li><?php i18n( "The usability of the user interface." ); ?></li>
  <li><?php i18n( "The quality of the content (vocabularies, precision of the output etc.)" ); ?></li>
  <li><?php i18n( "Possible targets to use the application for." ); ?></li>
  <li><?php i18n( "Missing features of the application." ); ?></li>
  <li><?php i18n( "Bugs araised." ); ?></li>
  <li>...</li>
</ul>

<p><?php i18n( "If you would like to use any of the contributed content in the <a href=\"../contrib/\">Contributions section</a>, it would be nice if you contacted the contributors and let them know where it is being used." ); ?></p>

<h3><img src="../images/icons/write_32.png" class="header-image" width="32" height="32" alt="pencil" /><?php i18n( "Writing Content Files" ); ?></h3>

<p><?php i18n( "You can contribute various content files. Read more about:" ); ?></p>

<ul>
  <li><?php i18n( "Creating <a href=\"kvtml.php\">kvtml vocabulary files</a>" ); ?></li>
  <li><?php i18n( "Writing <a href=\"kturtle.php\">KTurtle Logo scripts</a>" ); ?></li>
  <li><?php i18n( "Writing <a href=\"kig.php\">Kig macros</a>" ); ?></li>
  <li><?php i18n( "Creating <a href=\"parley.php\">themes for Parley</a>" ); ?></li>
</ul>

<p><?php i18n( "Please upload files you created to <a href=\"http://www.kde-files.org/\">kde-files.org</a> using this <a href=\"http://kde-files.org/content/add.php\">upload page</a>." ); ?><br />
<?php i18n( "They will appear on the page and also show up in the download dialogs of the applications.
Already available files can be found here:" ); ?></p>

<a href="http://www.kde-files.org"><img src="../images/icons/kde-files.org.jpg" style="width:295px;height:118px;float:right;" alt="kde-files.org logo" title="go to kde-files.org" /></a>
<ul>
  <li><a href="http://kde-files.org/index.php?xcontentmode=643"><?php i18n( "KStars Data files" ); ?></a></li>
  <li><a href="http://kde-files.org/index.php?xcontentmode=686"><?php i18n( "KTurtle Scripts" ); ?></a></li>
  <li><a href="http://kde-files.org/index.php?xcontentmode=687"><?php i18n( "Parley files (kvtml)" ); ?></a></li>
  <li><a href="http://kde-files.org/index.php?xcontentmode=694"><?php i18n( "KWordQuiz files (kvtml)" ); ?></a></li>
  <li><a href="http://kde-files.org/index.php?xcontentmode=693"><?php i18n( "KHangMan (kvtml)" ); ?></a></li>
  <li><a href="http://kde-files.org/index.php?xcontentmode=695x696x697x698"><?php i18n( "Cantor files" ); ?></a></li>
  <li><a href="http://kde-files.org/index.php?xcontentmode=699"><?php i18n( "Step files" ); ?></a></li>
</ul>

<p><?php i18n( "If you have questions or need help don't hesitate to <a href=\"../contact_us/\">contact us</a>." ); ?></p>

<?php
  include "footer.inc";
?>
