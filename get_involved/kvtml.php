<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );
  $page_title = i18n_noop( "Creating Vocabulary Files" ); 
  include "header.inc"; 
?>

  <p>
<?php i18n( '<a href="/applications/language/parley/">Parley</a> and 
<a href="/applications/language/kwordquiz/">KWordQuiz</a> use the same XML data file format called 
<strong>kvtml</strong>. To some extent these files also work with 
<a href="/applications/language/khangman/">KHangMan</a> and
<a href="/applications/language/kanagram/">Kanagram</a>.' ); ?>
</p>

  <h3><?php i18n( 'Programs exporting kvtml Files' ); ?></h3>

  <ul>
    <li><?php i18n( 'You can create a kvtml file using 
<a href="/applications/language/kwordquiz/">KWordQuiz</a>. You add the back and front for each cards 
and then you save your data. It will be saved using the kvtml extension.' ); ?></li>

<!--
    <li>KVocTrain is similiar in that aspect but adds some complexity. Create and edit kvtml file
    using <a href="/applications/language/kvoctrain/">KVocTrain</a>.</li>
-->

    <li><?php i18n( 'We suggest to use 
<a href="/applications/language/parley/">Parley</a>. Parley offers a wealth of editing possibilities
without being too difficult.' ); ?></li>

    <li><?php i18n( 'Please see <a href="sample.kvtml">here</a> for an example of a kvtml file if you want 
to create one with an editor.' ); ?></li>
  </ul>
  
  <h3><?php i18n( 'More Resources' ); ?></h3>
  
  <p><?php i18n( 'For a closer look at the filesharing features of KDE Education applications and the kvtml file format please visit following techbase wiki pages:' ); ?></p>
  
  <ul>
    <li><a href="http://techbase.kde.org/Projects/KNS2"><?php i18n( 'KNewStuff Project' ); ?></a></li>
    <li><a href="http://techbase.kde.org/Development/Tutorials/Collaboration/HotNewStuff"><?php i18n( 'Tutorial for developers how to add KNewStuff feature' ); ?></a></li>
    <li><a href="http://techbase.kde.org/Projects/Edu/kvtml2"><?php i18n( 'kvtml2 DTD' ); ?></a></li>
  </ul>

  <h3><img src="../images/icons/copyright_32.png" class="header-image" style="width:32px;height:32px;" alt="copyright symbol" /><?php i18n( 'Copyright Issues' ); ?></h3>

  <p><?php
   echo i18n_var( "There is another thing you should keep in mind when using data from books or similar: copyright violation. As a rule of thumb you might say that individual word translations can be used from books but not entire sections (The data isn't copyrighted but the collection of data is).")." ".
   i18n_var( 'Also books out of copyright can be used for data (in the US this means mainly pre-1923 books). Have a look at project Gutenberg at <a href="http://gutenberg.spiegel.de/">Projekt Gutenberg-DE</a> (German site), or <a href="http://www.gutenberg.org/">www.gutenberg.org</a> (English site), for more information about finding out if a book is out of copyright.' ); ?></p>
  
<?php include "footer.inc"; ?>
