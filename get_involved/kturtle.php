<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );
  $page_title = i18n_noop( "Writing KTurtle Scripts" ); 
  include "header.inc"; 
?>

<p><?php i18n( 'As every developer knows: the best way to learn a programming language is to study example code. You can find some Logo scripts <a href="../contrib/kturtle/">here</a>. Additional examples are welcome!' ); ?></p>

<?php 
  include "footer.inc"; 
?>
