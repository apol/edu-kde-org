<?php
  $site_root = "../";
  $page_title = "Download Kig";
  

  include( "header.inc" );
?>

<br />
<h3>Download Kig</h3>
<p>Since Kig has been included in the <code>kdeedu</code> module since KDE 3.2,
  it is no longer released separately.<br />
  You should download the <code>kdeedu</code> package from a KDE mirror.
  Moreover, because Kig is part of KDE, packages will most likely be available
  from your distribution.</p>

<p>Franco Pasquarelli made available an unofficial RPM of the most recent
  version of Kig, which you can find
  <a href="http://kig.dmf.unicatt.it/">here</a>. Thanks Franco :)</p>

<h3>Compiling Kig from SVN</h3>
<p>First of all, you should be aware of the current SVN version is the place
  where development occurs, and sometimes, it may crash, or mess up data like
  macros you have created, or files you try to open or save to. Generally, this
  shouldn&#039;t happen, but I can&#039;t guarantee it won&#039;t.</p>
<p>In order to compile Kig form SVN, you should do the following steps:</p>

<?php
  show_obtain_instructions( "Kig", "kdeedu", "false" );
?>

<h3>Post-KDE 3.5 version</h3>
<p>The development of Kig hasn&#039;t stopped after KDE 3.5. For more info
  you can take a look at the <a href="development.php">development page</a> of
  Kig to see what happened between KDE 3.5 and 4.0.</p>

<?php include( "footer.inc" ); ?>
