<?php
  $site_root = "../";
  $page_title = "Kig development";
  

  include( "header.inc" );
?>

<br />
<h3>The development version of Kig</h3>
<p>As we can&#039;t add new features in the KDE 3.5 version of Kig, and KDE 4
  won&#039;t happen in a few months, we&#039;ve created a branch with some new
  features already introduced in the version you will find in KDE-Edu for KDE 4.</p>

<p>Here there are all the new features already introduced in the post-kde-3.5
  branch and in the KDE 4 version. Note also that the KDE 4 version contains
  other features that can&#039;t be backported in this branch.</p>
<ul type="disc">
<li>Improved property system that now support cases like the dynamical change
  of object (eg: when using a Python script) maintaining the same property</li>
<li>New object: Numeric Label. This is a particular label that can hold only a
  number<br />
  <ul type="disc">
  <li>Adapted many costructions to get the numeric value also from a numeric
    label</li>
  <li>New object: Point by Numeric Labels (as coordinates)</li>
  <li>Extended the Python interface to return a Numeric Label in case of a
    numeric value calculated by a script</li>
  </ul>
</li>
<li>Various improvements in the input filters and in the exporters:<br />
  <ul type="disc">
  <li>Now Kig is able to read (partially) the files of Cabri v1.2, and the
    compatibility with Cabri v1.0 is slightly improved too</li>
  <li>Thanks to the Numeric Label, now the Dr.Geo and KSeg input filters can
    import a bit more objects</li>
  <li>The XFig exporter can export polygons</li>
  </ul>
</li>
<li>The user can freely choose the font of text and numeric labels</li>
<li>New object: Conic arc by Center and Three Points</li>
<li>New object: Conic arc by Five Points</li>
<li>New object: Vertical Cubic by Four Points</li>
<li>Basic support for polygons in the Python interface</li>
<li>New test: object existance</li>
</ul>

<p>Here you have, instead, all the new features you will find only in KDE 4
  version.</p>
<ul type="disc">
<li>Add the possibility to use a different metrical unit when specifying the
  size of an exported image</li>
<li>Better look for polygons, that allows to see through them
  (<a href="http://bugs.kde.org/show_bug.cgi?id=130603">bug 130603</a>)</li>
</ul>

<h3>Getting and compiling the post-KDE 3.5 version</h3>
<p>To download and compile the post-kde-3.5 branch, just follow these
  instructions:</p>
<pre>
$ svn co -N svn://anonsvn.kde.org/home/kde/branches/KDE/3.5/kdeedu
$ cd kdeedu
$ svn co svn://anonsvn.kde.org/home/kde/branches/kig/post-kde-3.5/kig
$ svn co svn://anonsvn.kde.org/home/kde/branches/KDE/3.5/kde-common/admin
$ make -f Makefile.cvs
$ ./configure
$ make
$ su -c &quot;make install&quot;
</pre>

<p><b>Be careful</b> that a <tt>.kig</tt> file saved with this version <i>may
  be NOT</i> compatible with the official Kig shipped with KDE 3.x. It&#039;s
  an almost-experimental version of Kig, so such kind of issues can happen.</p>

<?php include( "footer.inc" ); ?>
