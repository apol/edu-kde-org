<?php
  $site_root = "../";
  $page_title = 'Kig';
//  $page_title = 'Kig - KDE Interactive Geometry';

  include( "header.inc" );

  $appinfo = new AppInfo( "Kig" );
  $appinfo->setIcon( "../images/icons/kig_32.png", "32", "32" );
  $appinfo->setVersion( "1.0" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2002", "The Kig Developers" );
  $appinfo->addMaintainer( "Pino Toscano", "pino AT kde DOT org" );
  $appinfo->addAuthor( "Dominique Devriese", "devriese AT kde DOT org" );
  $appinfo->addAuthor( "Maurizio Paolini", "paolini AT dmf DOT bs DOT unicatt DOT it");
  $appinfo->addAuthor( "Franco Pasquarelli", "pasqui AT dmf DOT bs DOT unicatt DOT it" );
  $appinfo->show();
?>

<br />

<h3><a name="description">Description</a></h3>

<p><b>Kig is a program for exploring geometric constructions.</b></p>

<p>It is meant as a better replacement for such free programs as
  <a href="http://kgeo.sourceforge.net">KGeo</a>,
  <a href="http://www.mit.edu/~ibaran/kseg.html">KSeg</a> and
  <a href="http://www.ofset.org/articles/15">Dr. Geo</a> and commercial programs
  like <a href="http://www-cabri.imag.fr/">Cabri</a>.</p>

<p>Kig is <a href="http://www.gnu.org/philosophy/philosophy.html#AboutFreeSoftware">free
  software</a>, as in &quot;free speech&quot;. This basically means that you do
  not have to ask my permission to use it, to improve it, to distribute or even
  to sell it. The only thing that you are required to do is to pass on these
  rights if you distribute Kig. Of course, this section is not meant as a legal
  text, and you should read the
  <a href="http://www.gnu.org/licenses/gpl.html">GPL license</a> if you want to
  know what your rights are.</p>

<p>Kig is meant as a useful and powerful utility for high school students and
  teachers, and as a fun project for me and others. I have learned a lot by
  working on it, and it&#039;s fun to work together with other people on it.
  I&#039;ve written much of the Kig code myself, but I&#039;ve also had a lot of
  help from other people. Please see the about box for more information.</p>

<p>Anyway, since this is probably what you all came here for, here&#039;s a link
  to the <a href="screenshots.php">screenshots</a> page.</p>

<p>If you have Kig with Python scripting, you can find the documentation of Kig
  Pyhon scripting API <a href="manual/scripting-api/index.html">here</a>.</p>

<?php
  include("footer.inc" );
?>
