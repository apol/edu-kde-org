<h3>New Features</h3>
<ul>
<li>Printing: different paper sizes</li>
<li>Logarithmic axes division</li>
<li>In the  tool-menu:<br/>
	<ul>
	<li>get the slope in a x-point</li>
	</ul>
</li>
<li>#52887, need to calculate with complex numbers</li>
<li>Import parameter values from different file format (.csv, .txt, KSpread, OpenOffice)</li>
</ul>

<h3>Bugs Fixes</h3>
<ul>
	<li>Address all the bugs in the bugs database and deal with them</li>
</ul>
