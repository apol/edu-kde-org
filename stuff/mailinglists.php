<?php
  $site_root = "../";
  $page_title = 'Contact Information';
  
  include( "header.inc" );
 ?>

 <h3><a name="lists">Mailing List</a></h3>
 <p>There is one mailing list concerning the KDE-Edu Project</p>
<dl>
<dt><b>kde-edu:</b></dt>
<dd>In this list people discuss Free Educational Software issues more or less related to the KDE-Edu project.
To subscribe to the list, go <a href="http://mail.kde.org/mailman/listinfo/kde-edu">here</a>.
You can also browse through the <a href="http://mail.kde.org/pipermail/kde-edu/">online archive</a> of this list.</dd>
</dl>
<br />

<h3><a name="irc">IRC channel</a></h3>
<p>On the Freenode network you can reach some of us on the #kde-edu channel.</p>

<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

