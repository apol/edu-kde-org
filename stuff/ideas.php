<?php
  $site_root = "../";
  $page_title = 'Ideas for new Educational Applications';
  
  include ( "header.inc" );
?>

<h2>Languages</h2>
<ul>
<li><b>A spelling program</b>
<br />Please see <a href="http://lists.kde.org/?l=kde-edu&amp;m=119921332204229&amp;w=2">http://lists.kde.org/?l=kde-edu&amp;m=119921332204229&amp;w=2</a><br />
and <a href="http://lists.kde.org/?l=kde-edu&amp;m=119923706526028&amp;w=2">http://lists.kde.org/?l=kde-edu&amp;m=119923706526028&amp;w=2</a></li>
</ul>

<h2>Mathematics</h2>
<ul>
<li><b>A program to teach how to manipulate basic algebraic expressions</b>
<br />
For example, the student could see an equation, and could drag with the mouse a term of it and drop it in the other 
side of the equation, with opposite sign.<br />
This could have several configurable levels, for instance, in a level they 
could not drag and drop, but only select and open menu to "substract this 
term from both sides", and then there could be an animation that shows how 
the term is substracted, and canceled. There could also be menus for things 
like "reduce to common denominator", "take the common factor", 
"distribute"...
<br />
Submitted by  <a href="mailto:mmarco@unizar.es">Miguel Marco</a>
<br />
<a href="http://lists.kde.org/?l=kde-edu-devel&m=115971446917258&w=2">Link to archived email</a></li>
</ul>
 <p>
Ideas are also listed on <a href="http://wiki.skolelinux.de/LernSoftware/Desiderata#English">the SkoleLinux Desiderata page</a>.</p>

<br />
<br />
<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
