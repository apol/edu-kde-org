<?php
  $site_root = "../";
  $page_title = "Getting Involved";
  
  include( "header.inc" );
?>

 <div id="quicklinks"> [
  <a href="#getinvolved">Get involved</a> |
  <a href="#programs">New programs ideas</a> |
  <a href="#join">How to join?</a>
  ]
</div>
<h3><a name="getinvolved">Who can take part in the project?</a></h3>

<p>Many different people are needed to make the project a success. These
include:</p>
<ul>
<li><b>Artists</b>: We are in great need of Oxygen-style actions icons and Oxygen-style application icons for Blinken, KTurtle, KAlgebra, Step and KVerbos. Please contact the <a href="https://mail.kde.org/mailman/listinfo/kde-edu">kde-edu mailing-list</a> for more information.</li>
<li><b>Designers</b>: To design the overall interface of an
application, to give consistency in the project so the child could switch from
math to languages and find the same sort of interface.</li>
<li><b>Web Designers</b>: Our website could do with some improvements!</li>
<li><b>Developers</b>: Both beginners and more experienced developers are
welcomed -- everyone should be able to find a suitable project.</li>
<li><b>Musicians</b>: For sound and music in some applications as well as
recorded voice sounds. Please contact the <a href="https://mail.kde.org/mailman/listinfo/kde-edu">kde-edu mailing-list</a> for more information.</li>
<li><b>Parents and teachers</b>: Feedback is needed
to to tell us what is needed, how could it be done, to test alpha and beta
versions and to help with the content, etc.</li>
<li><b>Children</b>:
Children, will, of course be required to help test the software. They can also
tell us their ideas.</li>
</ul>

 <h3><a name="programs">New programs for new developers</a></h3>
<p>You can find <a href="ideas.php">a list of ideas</a> that need to be developed. Please send a mail to <a href="mailto:kde-edu AT kde DOT org">the kde-edu mailing list</a> saying you are going to work on a program to avoid duplicated work.</p>


 <h3><a name="join">How do I join?</a></h3>
 <ul>
 <li>Subscribe to the to the <a href="https://mail.kde.org/mailman/listinfo/kde-edu">kde-edu mailing-list</a></li>
 <li>Join us on IRC (server: irc.freenode.net, channel: #kde-edu)</li>
 <li>Send a project proposal, or see if you like an existing project</li>
 <li>After projects are started, they will be listed at the projects status page. You will then be able to contact people directly to join a project.</li>
 </ul>

<br />
<hr width="30%" align="center" />
<p>
Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
