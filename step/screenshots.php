<?php
  $site_root = "../";
  $page_title = 'Step - Screenshots - KDE4';
  
  include ( "header.inc" );
?>
<br />

<?php
  $gallery = new EduGallery("Step - Screenshots1");
  $gallery->addImage("pics/solar1_sm.png", "pics/solar1.png", 160, 130,  "[Screenshot]", "", "Solar System model");
  $gallery->addImage("pics/springs1_sm.png", "pics/springs1.png", 160, 130,  "[Screenshot]", "", "Particles and springs");
  $gallery->startNewRow();
  $gallery->addImage("pics/polygons1_sm.png", "pics/polygons1.png", 160, 130,  "[Screenshot]", "", "Rigid polygons");
  $gallery->addImage("pics/solvers1_sm.png", "pics/solvers1.png", 160, 130,  "[Screenshot]", "", "Selecting solver type");
  $gallery->startNewRow();
  $gallery->addImage("pics/graph1_sm.png", "pics/graph1.png", 160, 128,  "[Screenshot]", "", "Graphs");
  $gallery->addImage("pics/lissajous1_sm.png", "pics/lissajous1.png", 160, 128,  "[Screenshot]", "", "Lissajous curves");
  $gallery->startNewRow();
  $gallery->addImage("pics/gas1_sm.png", "pics/gas1.png", 159, 128,  "[Screenshot]", "", "Gas");
  $gallery->addImage("pics/liquid_sm.png", "pics/liquid.png", 159, 128,  "[Screenshot]", "", "Liquid");
  /*$gallery->addImage("pics/oldqt4_sm.jpg", "pics/oldqt4.jpg", 160, 115,  "[Screenshot]", "", "Old QT4-only version");*/
  $gallery->startNewRow();
  $gallery->addImage("pics/note2_sm.png", "pics/note2.png", 159, 128,  "[Screenshot]", "", "Notes");
  $gallery->show();
?>

 <br />
 <hr width="30%" align="center" />
 <p>
 Author: Anne-Marie Mahfouf<br />
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>














