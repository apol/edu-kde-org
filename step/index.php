<?php
  $site_root = "../";
  $page_title = "Step";
  
  include( "header.inc" );
?>

<table width="100%">
<tr>
<td>

<?php
  $appinfo = new AppInfo( "Step" );
  $appinfo->setIcon( "../images/icons/step_32.png", "32", "32" );
  $appinfo->setVersion( "0.1.0" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2007", "Vladimir Kuznetsov" );
  $appinfo->addAuthor( "Vladimir Kuznetsov", "ks.vladimir AT gmail DOT com" );
  $appinfo->addContributor( "Carsten Niehaus", "cniehaus AT kde DOT org" );
  $appinfo->addContributor( "Aliona Kuznetsova", "aliona.kuz AT gmail DOT com" );

  $appinfo->show();
?>

</td>
<td align="right" valign="top">
<a href="pics/gas1.png">
<img alt="[Screenshot]" src="pics/step_sm.png" width="268" height="215" />
</a>
</td>
</tr>
</table>

<br />

<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#features">Features</a> |
  <a href="#stepcore">StepCore Library</a> |
  <a href="#join">Join us!</a> |
  <a href="#news">News</a>
]
</div>

<h3><a name="description">Description</a></h3>
<p>Step is an interactive physics simulator. It works like this: you place some bodies on the scene, add some forces such as gravity or springs, then click "Simulate" and Step shows you how your scene will evolve according to the laws of physics. You can change every property of bodies/forces in your experiment (even during simulation) and see how this will change the outcome of the experiment. With Step you can not only learn but feel how physics works !</p>

<h3><a name="features">Features</a></h3>
<p><ul>
    <li>Classical mechanical simulation in two dimensions</li>
    <li>Particles, springs with damping, gravitational and coulomb forces</li>
    <li>Rigid bodies</li>
    <li>Collision detection (currently only discrete) and handling</li>
    <li>Soft (deformable) bodies simulated as user-editable particles-springs system, sound waves</li>
    <li>Molecular dynamics (currently using <a href="http://en.wikipedia.org/wiki/Lennard-Jones_potential">Lennard-Jones potential</a>): gas and liquid, condensation and evaporation, calculation of macroscopic quantities and their variances</li>
    <li>Units conversion and expression calculation: you can enter something like "(2 days + 3 hours) * 80 km/h" and it will be accepted as distance value (requires <a href="http://qalculate.sourceforge.net/">libqalculate</a>)</li>
    <li>Errors calculation and propagation: you can enter values like "1.3 ± 0.2" for any property and errors for all dependent properties will be calculated using statistical formulas</li>
    <li>Solver error estimation: errors introduced by the solver is calculated and added to user-entered errors</li>
    <li>Several different solvers: up to 8th order, explicit and implicit, with or without adaptive timestep (most of the solvers require <a href="http://www.gnu.org/software/gsl/">GSL library</a>)</li>
    <li>Controller tool to easily control properties during simulation (even with custom keyboard shortcuts)</li>
    <li>Tools to visualize results: graph, meter, tracer</li>
    <li>Context information for all objects, integrated wikipedia browser</li>
    <li>Collection of example experiments, more can be downloaded with KNewStuff2</li>
</ul>
</p>

<!--p>Currently the realm of Step is the classical mechanics on two dimension. Two dimensions was chosen instead of three because it is quite easier to imagine, edit and visualize on computer screen so user can concentrate on physics itself (still not sure - than just compare complexity of Inkscape and Blender).
</p-->

<h3><a name="stepcore">StepCore Library</a></h3>
<p>StepCore is the physical simulation library on which Step is based. It can be used without Step for complex simulations which require coding or in other software which require physical simulation functionality. It is designed in order to be extensible, tunable and to provide accurate simulation.
</p>

<h3><a name="join">Join Us!</a></h3>
<p>Do you like Step and want to help developing it ? If you are programmer you can help coding Step. If you have some knowledge in physics you can help by writing context information. You can also help by writing documentation or by translating Step to non-english language. Even bug reports and feature requests are a big help, so keep them coming!</p>
<p>In order to start just subscribe to <a href="https://mail.kde.org/mailman/listinfo/kde-edu">kde-edu mailing list</a> or join #kde-edu channel on freenode.</p>

<?php
  kde_general_news("./news.rdf", 10, true);
?>

<br />
<hr width="30%" align="center" />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
<?php
  include("footer.inc");
?>

