<?php
  $page_title = "How to obtain Step";
  $site_root = "../";
  

  include( "header.inc" );
?>

<p>
In order to compile Step you need a recent KDE 4 libraries (Qt 4.5.rc1 and kdelibs trunk) and the Eigen2 library from kdesupport trunk.</p>
<p>
Step can optionally use 
<ul>
<li>the GSL library (<a href="http://www.gnu.org/software/gsl">http://www.gnu.org/software/gsl</a> or from your distribution) for solvers</li>
<li>the libqalculate library (<a href="http://qalculate.sourceforge.net/">http://qalculate.sourceforge.net/</a> or from your distribution) for unit conversion.</li>
</ul>
Please set the KDEDIRS environment variable to point to where your KDE 4 is installed.
</p>


<?php
  show_obtain_instructions( "Step", "trunk", true );
?>


<p></p>
<br />
<hr width="30%" align="center" />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>

<?php
  include "footer.inc";
?>


