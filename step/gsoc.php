<?php
  $site_root = "../";
  $page_title = "Step GSoC project";
  

  include( "header.inc" );
?>

<br />
<p>This section is dedicated for tracking the state of the <a href="http://code.google.com/soc/kde/appinfo.html?csaid=D999CC3F6794C985">Step GSoC project</a>. The project consists of implementing the following features:</p>

<ol>
<li>Simulation (StepCore library)
    <ol>
    <li><span style="color: green">[done]</span> Global error estimation
        <ul>
        <li>allows to calculate total error for each dynamic variable for
            the whole simulation time</li>
        <li>allows to set initial conditions with errors and see how error
            will propagate</li>
        </ul>
    </li>
    <li><span style="color: green">[done]</span> Molecular dynamics
        <ul>
        <li>2d fluid and gas</li>
        <li>evaporation, Brownian motion</li>
        <li>allows to demonstrate relations between macro- and microscopic
            quantities in gas and fluid</li>
        </ul>
    </li>
    <li><span style="color: green">[done]</span> Soft bodies and massive springs
        <ul>
        <li>deformable bodies</li>
        <li>sound waves in bodies</li>
        </ul>
    </li>
    </ol>
</li>

<li>GUI (Step itself)
    <ol>
    <li><span style="color: green">[done]</span> Threading
        <ul>
        <li>keep GUI responsible while performing long calculations</li>
        <li>allows to abort long calculations</li>
        </ul>
    </li>
    <li><span style="color: green">[done]</span> Units for all quantities, unit conversions
        <ul>
        <li>users will know in what units quantities are displayed</li>
        <li>allows to enter values in various units</li>
        <li>underlying conversion library will be shared in libkdeedu and
            available for use in other parts of the module</li>
        </ul>
    </li>
    <li><span style="color: green">[done]</span> Custom graphics for various quantities
        <ul>
        <li>allows to create various useful graphics, for example phase
            diagram for oscillator</li>
        <li>graphics are very important for education since they can
            visually demonstrate complex relations between quantities</li>
        </ul>
    </li>
    <li><span style="color: green">[done]</span> Custom controllers to control body (force, solver) parameters
        during simulation
        <ul>
        <li>various properties will be easily controllable when running
            simulation</li>
        </ul>
    </li>
    <li><span style="color: green">[done]</span> User-editable notes in experiment
        <ul>
        <li>allows users to annotate experiment</li>
        <li>teachers can add explanations for students</li>
        </ul>
    </li>
    <li><span style="color: green">[done]</span> Sidebar with nice descriptions and articles from wikipedia for
        all body (force, solver) types
        <ul>
        <li>users can easily find common formulas about objects</li>
        <li>users can easily access information they require to learn more
            about objects</li>
        </ul>
    </li>
    <li><span style="color: green">[done]</span> Knewstuff2 for sharing experiments
        <ul>
        <li>users can easily download new experiments and share their own</li>
        </ul>
    </li>
    </ol>
</li>

<li>Polishing
    <ol>
    <li><span style="color: #C0C000">[70% done]</span> Step will be prepared for inclusion in the kdeedu just after SoC</li>
    </ol>
</li>

<li>I will also implement the following features before SoC coding period begins (so technically they are not parts of SoC):
    <ol>
    <li><span style="color: #C0C000">[60% done]</span> Collision handling with friction</li>
    <li><span style="color: #C0C000">[10% done]</span> Movement constraints (joints)</li>
    <li><span style="color: red">[not started]</span> Motors</li>
    </ol>
</li>
</ol>

<?php
  include( "footer.inc" );
?>
