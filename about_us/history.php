<?php
  $translation_file = "edu-kde-org";
  include_once( "functions.inc" );
  $page_title = i18n_noop( "History of the KDE-Edu Project" );
  include ( "header.inc" );
?>

<h3><?php i18n( "How did it start?" ); ?></h3>

<p><?php i18n( "The KDE-Edu project was started in July 2001. 
The goal of the project is to develop Free Educational Software (GPL license) within the KDE environment. 
This software is mainly aimed to schools, 
to students and to parents at home as well as to adults willing to extend their knowledge." ); ?></p>

<p><?php i18n( "A new KDE module was created, the kdeedu module. 
The applications in that module are fully KDE compatible (use of KMainWindow, XML GUI, ...) 
and get a proper documentation handbook in docbook format. 
Thus, all applications interfaces and documentation are translated in more than 65 languages, 
via the l10n KDE teams." ); ?></p>

<p><?php i18n( "The kdeedu module joined the KDE 3.0 official release." ); ?></p>
 
<?php
  include "footer.inc";
?>
