<?php
  $translation_file = "edu-kde-org";
  include_once( "functions.inc" );
  $site_root = "./";
  $page_title = i18n_noop( "KDE Edutainment News" );
  include("header.inc");
?>

<?php
	kde_general_news("./news.rdf", 10, false);
?>

<?php
  include("footer.inc");
?>
