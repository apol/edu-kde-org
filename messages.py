#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2011 Matthias Meßmer <messmer@kde.org> and
#                  Luca Beltrame <einar@heavensinferno.net>
#
#   Many many thanks to Luca Beltrame who gave me the starting point and
#   lots of help during my long try and error process!
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License, under
#   version 2 of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""This small program aims at converting JSON files used for KDE's web
pages in a form understandable by xgettext and hence translatable.

Invoke it like:

    messages.py [>messages.inc]

"""


import glob
import json
import types
import sys

template = "\ti18n_noop(\"%s\");\n"
comment = "\n\t// %s\n"


def get_app_files():

    """Collect the paths of json files into a list and return it.

    """

    files = glob.glob("applications/apps/*.json")

    apps = [item for item in files if not item.endswith("_generated.json")]
    apps_generated = [item for item in files if item.endswith("_generated.json")]

    return (apps, apps_generated)


def output_messages(messages):

    """Output messages to stdout.

    :param messages: a list of messages to be translated

    """

    if messages:
        for message in messages:
            # No empty messages, please
            if message:
                # preserve escaped quotes
                msg = message.replace("\"", "\\\"")
                msg = template % msg
                sys.stdout.write(msg)


def parse_app(inputfile):

    """Parse json file for an application.

    :param inputfile: path to json file

    """

    messages = []

    with open(inputfile) as handle:
        source = json.load(handle)

    # only "description" and "features" contain translatable messages

    # Get the value for "description" and "feature" keys,
    # or return None if not found
    description_content = source.get("description")
    features_content = source.get("features")

    if (description_content is not None
        and isinstance(description_content, str)):
        messages.append(description_content)

    if features_content is not None and isinstance(features_content,
                                                   list):
        for feature in features_content:
            if isinstance(feature, str):
                messages.append(feature)
            elif isinstance(feature, list):
                for subfeature in feature:
                    messages.append(subfeature)

    output_messages(messages)


def parse_app_generated(inputfile):

    """Parse auto generated json file for an application.

    :param inputfile: path to json file

    """

    messages = []

    with open(inputfile) as handle:
        source = json.load(handle)

    # "authors" and "credits" contain translatable messages
    # only in the second list entry
    # Get elements from the keys, or None if not found. Only proecss
    # non None elements

    author_content = source.get("authors")
    credit_content = source.get("credits")

    if author_content is not None:
        for author in author_content:
            if isinstance(author[1], str):
                messages.append(author[1])

    if credit_content is not None:
        for author in credit_content:
            if isinstance(author[1], str):
                messages.append(author[1])

    messages.append(source["generic name"])

    output_messages(messages)


def main():

    # usage = "%prog[>messages.inc]"

    sys.stdout.write("<?php")

    apps, generated_apps = get_app_files()

    for source in apps:
        sys.stdout.write(comment % source)
        parse_app(source)

    for source in generated_apps:
        sys.stdout.write(comment % source)
        parse_app_generated(source)

    sys.stdout.write("?>")

if __name__ == "__main__":
    main()
