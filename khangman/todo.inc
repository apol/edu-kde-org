<ul>
<li>Rewrite the code for more clarity</li>
<li>Solve the font problem, try to find a way to ensure the correct font is used to display special characters</li>
<li>Integrate Danny's designs. Please see <a href="http://edu.kde.org/khangman/future.php">http://edu.kde.org/khangman/future.php</a></li>
<li>Add an area to display pics as hints for younger kids: integrate pics in kvtml files</li>
</ul>
<ul>
<li>svg support</li>
<li>the languages in the Languages menu should be in alphabetical order after KNewStuff has added a new one</li>
<li>the Level combobox should be refreshed to accomodate a longer file name</li>
<li>bug in the Departements French file when there are 2 '-'</li>
<li>the KNewStuff dialog displays the languages reverse alphabetically</li>
</ul>

