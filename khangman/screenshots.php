<?php
  $site_root = "../";
  $page_title = 'KHangMan - Screenshots KDE 4.0';
  
  include ( "header.inc" );
?>
<br />

<?php
  $gallery = new EduGallery("KHangMan - KDE 4.0");
  $gallery->addImage("pics/khangman_4.0-1_sm.png", "pics/khangman_4.0-1.png", 160, 128,  "[Screenshot]", "", "KHangMan in French");
  $gallery->addImage("pics/khangman_4.0-2_sm.png", "pics/khangman_4.0-2.png", 160, 97,  "[Screenshot]", "", "The Get Hot New Stuff dialog allows you to download data in other languages");
  $gallery->startNewRow();
  $gallery->addImage("pics/khangman_4.0-3_sm.png", "pics/khangman_4.0-3.png", 160, 118,  "[Screenshot]", "", "In Catalan, with the Special Characters toolbar shown");
  $gallery->addImage("pics/khangman_4.0-4_sm.png", "pics/khangman_4.0-4.png", 160, 86,  "[Screenshot]", "", "The Configure Dialog");
  $gallery->show();
?>

 <p>
 <b>Note</b>: These screenshots are from KHangMan current stable KDE 4.0.0.
 </p>
 <br />
 <hr width="30%" align="center" />
 <p>
 Author: Anne-Marie Mahfouf<br />
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>














