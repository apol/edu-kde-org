<?php
  $site_root = "../";
  $page_title = 'KHangMan - Languages';
  
  include ( "header.inc" );

  $appinfo = new AppInfo( "KHangMan" );
  $appinfo->setVersion( "1.6" );
  $appinfo->setCopyright( "2001", "Anne-Marie Mahfouf" );
  $appinfo->setLicense("gpl");
  $appinfo->addContributor( "Stefan Asserh&#228;ll", "stefan DOT asserhall AT telia DOT com", "Swedish data files" );
  $appinfo->addContributor( "eXParTaKus", "expartakus AT expartakus DOT com", "Spanish data files" );
  $appinfo->addContributor( "Rafael Beccar", "rafael DOT beccar AT kdemail DOT net", "Spanish data tips" );
  $appinfo->addContributor( "Erik Kj&aelig;r Pedersen", "erik AT mpim-bonn DOT mpg DOT de", "Danish data files" );
  $appinfo->addContributor( "Niko Lewman", "niko DOT lewman AT edu DOT hel DOT fi", "Finnish data files" );
  $appinfo->addContributor( "Jo&atilde;o Sebasti&atilde;o de Oliveira Bueno", "gwidion AT mpc DOT com DOT br", "Brazilian Portuguese data files" );
  $appinfo->addContributor( "Antoni Bella", "bella5 AT teleline DOT es", "Catalan data files" );
  $appinfo->addContributor( "Giovanni Venturi", "jumpyj AT tiscali DOT it", "Italian data files" );
  $appinfo->addContributor( "Rinse", "Rinse AT kde DOT nl", "Dutch data files" );
  $appinfo->addContributor( "Pedro Morais", "Morais AT kde DOT org", "Portuguese data files" );
  $appinfo->addContributor( "Chusslove Illich", "chaslav AT sezampro DOT yu", "Serbian (Cyrillic and Latin) data files" );
  $appinfo->addContributor( "Jure Repinc", "jlp AT holodeck1 DOT com", "Slovenian data files" );
  $appinfo->addContributor( "Luk&aacute;&scaron; Tinkl", "lukas AT kde DOT org", "Czech data files" );
  $appinfo->addContributor( "Roger Kovacs", "rkovacs AT khujand DOT org", "Tajik data files" );
  $appinfo->addContributor( "Torger &#197;ge Sinnes", "torg-a-s AT online DOT no", "Norwegian (Bokm&#229;l) data files" );
  $appinfo->addContributor( "Tamas Szanto", "tszanto AT mol DOT hu", "Hungarian data files" );
  $appinfo->addContributor( "Gaute Hvoslef Kvalnes", "gaute AT verdsveven DOT com", "Norwegian (Nynorsk) data files" );
  $appinfo->addContributor( "Mehmet &Ouml;zel", "mehmet_ozel2003 AT hotmail DOT com", "Turkish data files");
 $appinfo->addContributor( "Radostin Radnev", "radnev AT yahoo DOT com", "Bulgarian data files");
 $appinfo->addContributor( "Kevin Patrick Scannell", "scannell AT slu DOT edu", "Irish (Gaelic) data files");
 $appinfo->addContributor( "Rastislav Stanik", "rastos AT rastos DOT org", "Slovak data files");
 $appinfo->addContributor( "Jan Saganowski", "jan DOT saganowski AT gmail DOT com", "Polish data files");
  $appinfo->addContributor( "Anne-Marie Mahfouf", "annma AT kde DOT org", "English, German and French data files" );
  $appinfo->show();
  ?>

 <h3>Description</h3>
 The next KHangMan version, shipped with KDE 3.4.1, will have data in 24 languages: Czech, Brazilian Portuguese, Bulgarian, Catalan, Danish, Dutch, English, Finnish, French, German, Hungarian, Irish (Gaelic), Italian, Norwegian (Bokm&#229;l), Norwegian (Nynorsk), Polish, Portuguese, Spanish , Slovenian, Serbian Latin, Serbian Cyrillic, Slovak, Swedish, Tajik and Turkish. On install, you will get English plus your KDE language if it's one in the list (it'll come with your i18n module). You will be able to easily get any other language with the new Get New Stuff menu item in the File menu, this will allow you to install data in a new language in a few clicks (if you have an internet connection).You can already have a preview of this in cvs HEAD. Furthermore, in six languages, Bulgarian, French , English, Polish, Slovak and Italian, hints are available to help the user guess the word.<br/>

 <h3>How to change the language for KHangMan in KDE 3.0 or 3.1</h3>
 <p>
 In the KHangMan version shipped with KDE 3.0 and KDE 3.1, only words in English are available. If you want to replace the english words by one of the available other language (Catalan, Danish, Dutch, Finnish, French, German, Hungarian, Italian, Portuguese, Brazilian Portuguese, Serbian-Cyrillic, Serbian-Latin, Slovenian, Spanish, Swedish or Tajik), please follow the instructions below. Note that you can have ONLY ONE language at a time with KDE-3.0 or 3.1. Please set your global font to Arial or URW Bookman (in KDE Control Center) for special characters in your language if necessary.
 </p>
 <ol>
 <li>Download here the language package that you want.
 <ul>
  <li><a href="catalan-data.tar.gz">catalan-data.tar.gz</a></li>
  <li><a href="czech-data.tar.bz2">czech-data.tar.bz2</a></li>
 <li><a href="danish-data.tar.gz">danish-data.tar.gz</a></li>
 <li><a href="dutch-data.tar.bz2">dutch-data.tar.bz2</a></li>
 <li><a href="english-data.tar.bz2">english-data.tar.bz2</a></li>
 <li><a href="finnish-data.tar.gz">finnish-data.tar.gz</a></li>
 <li><a href="french-data.tar.gz">french-data.tar.gz</a></li>
 <li><a href="german-data.tar.bz2">german-data.tar.bz2</a></li>
 <li><a href="downloads/hungarian-data.tar.gz">hungarian-data.tar.gz</a></li>
 <li><a href="italian-data.tar.bz2">italian-data.tar.bz2</a></li>
 <li><a href="downloads/nb-data.tar.gz">norwegian-bokm&#229;l-data.tar.gz</a></li>
 <li><a href="portuguese-data.tar.bz2">portuguese-data.tar.bz2</a></li>
  <li><a href="pt_BR-data.tar.gz">portuguese_BR-data.tar.gz</a></li>
 <li><a href="serbian-cyr-data.tar.bz2">serbian-cyr-data.tar.bz2</a></li>
 <li><a href="serbian-latin-data.tar.bz2">serbian-latin-data.tar.bz2</a></li>
 <li><a href="slovenian-data.tar.gz">slovenian-data.tar.gz</a></li>
  <li><a href="spanish-data.tar.gz">spanish-data.tar.gz</a></li>
  <li><a href="swedish-data.tar.gz">swedish-data.tar.gz</a></li>
  <li><a href="downloads/tajik-data.tar.gz">tajik-data.tar.gz</a></li>
 </ul>
 <br />
 </li>

 <li>
 mv <i>language</i>-data.tar.gz $KDEDIR/share/apps/khangman/data/<br />
 where $KDEDIR is the directory where your KDE is installed (/usr or /opt/kde3 for example)
 <br />
 You might need to be root to do so.<br /><br />
 </li>

 <li>
 cd $KDEDIR/share/apps/khangman/data/
 <br /><br />
 </li>

  <li>tar zxvf <i>language</i>-data.tar.gz<br />
  or<br />
  tar jxvf <i>language</i>-data.tar.bz2<br />
 where <i>language</i> is one of the 19 languages.
 <br />
 </li>

 </ol>
 The four files easy.txt, medium.txt, hard.txt and animals.txt will overwrite the existing ones.<br />
 Start KHangMan to play in the new language.<br />

 <h3>How to add data for a new language for KDE 4</h3>
 If you would like to contribute to <strong>data in your own language for KDE 4</strong>, you can do so very easily. Please see the <a href="add_language/add_language_kde4.php">Add new language page</a> for more details..

 <p>
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>
