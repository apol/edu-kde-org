<table border="0" cellpadding="5" cellspacing="10">

<tr>
<th colspan="2" class="contentheader">Miscellaneous</th>
<th class="contentheader" style="text-align:center">Since KDE</th>
</tr>

<tr>
<td valign="top"><a href="../blinken/index.php"><img alt="Blinken" align="top" src="../images/icons/blinken_32.png"  width="32" height="32" /></a></td>
<td valign="top"><a style="font-weight:bold" href="../blinken/index.php">Blinken</a> is the KDE version of the well-known game Simon Says.</td>
<td style="text-align:center">3.5</td>
</tr>

<tr>
<td valign="top"><a href="../kgeography/index.php"><img alt="KGeography" align="top" src="../images/icons/kgeography_32.png" /></a></td>
<td valign="top"><a style="font-weight:bold" href="../kgeography/index.php">KGeography</a> is a geography learning tool.</td>
<td style="text-align:center">3.5</td>
</tr>

<tr>
<td valign="top"><a href="../ktouch/index.php"><img alt="KTouch" align="top" src="../images/icons/ktouch_32.png"  width="32" height="32" /></a></td>
<td valign="top"><a style="font-weight:bold" href="../ktouch/index.php">KTouch</a> is a program for learning touch typing.</td>
<td style="text-align:center">3.0</td>
</tr>

<tr>
<td valign="top"><a href="../kturtle/index.php"><img alt="KTurtle" align="top" src="../images/icons/kturtle_32.png"  width="32" height="32" /></a></td>
<td valign="top"><a style="font-weight:bold" href="../kturtle/index.php">KTurtle</a>  is an educational programming environment using the Logo programming language.</td>
<td style="text-align:center">3.3</td>
</tr>

<tr>
<td valign="top"><a href="../pairs/index.php"><img alt="Pairs" align="top" src="../images/icons/pairs_32.png"  width="32" height="32" /></a></td>
<td valign="top"><a style="font-weight:bold" href="../pairs/index.php">Pairs</a> is a game that will help train your memory by remembering different shapes, sounds and text.</td>
<td style="text-align:center">3.3</td>
</tr>

</table>
